unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Gorilla.Viewport, FMX.Types3D, System.Math.Vectors, FMX.Controls3D,
  Gorilla.Light, Gorilla.Control, Gorilla.Transform, Gorilla.Mesh, Gorilla.Model,
  Gorilla.DefTypes, FMX.Objects3D, Gorilla.Camera, FMX.MaterialSources;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaLight1: TGorillaLight;
    GorillaModel1: TGorillaModel;
    TriangleRenderer: TDummy;
    procedure FormCreate(Sender: TObject);
    procedure TriangleRendererRender(Sender: TObject; Context: TContext3D);
    procedure GorillaModel1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Single; RayPos, RayDir: TVector3D);
    procedure FormDestroy(Sender: TObject);
    procedure GorillaModel1MouseLeave(Sender: TObject);
  private
    FSelectedTriangle : TTriangleRayCastResult;
    FSelectedMesh     : TGorillaMesh;

    // For rendering the selected triangle we setup some buffer to render fast
    FTriangleVBuf : TVertexBuffer;
    FTriangleIBuf : TIndexBuffer;
    FTriangleMat : TColorMaterialSource;
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.RTLConsts,
  Gorilla.Context, Gorilla.Context.Types, Gorilla.FBX.Loader;

procedure TForm1.FormCreate(Sender: TObject);

  procedure LinkPickingMethod(AMesh : TGorillaMesh);
  var I : Integer;
  begin
    if not Assigned(AMesh) then
      Exit;

    AMesh.OnMouseMove := GorillaModel1MouseMove;
    AMesh.OnMouseLeave := GorillaModel1MouseLeave;

    for I := 0 to AMesh.Meshes.Count - 1 do
      LinkPickingMethod(AMesh.Meshes[I]);
  end;

begin
  FTriangleVBuf := TVertexBuffer.Create([TVertexFormat.Vertex], 3);
  FTriangleIBuf := TIndexBuffer.Create(6, TIndexFormat.UInt32);
  FTriangleIBuf.Indices[0] := 0;
  FTriangleIBuf.Indices[1] := 1;
  FTriangleIBuf.Indices[2] := 1;
  FTriangleIBuf.Indices[3] := 2;
  FTriangleIBuf.Indices[4] := 2;
  FTriangleIBuf.Indices[5] := 0;

  FTriangleMat := TColorMaterialSource.Create(GorillaViewport1);
  FTriangleMat.Color := TAlphaColorRec.Yellow;

  // Attach light to camera
  GorillaLight1.Parent := GorillaViewport1.GetDesignCamera();

  // Load model from file
  GorillaModel1.LoadFromFile(nil, '.\willoughsby-espresso.fbx', []);

  // Allow picking by enabling the hittest
  GorillaModel1.SetHitTestValue(true);

  // Make model a bit transparent
  GorillaModel1.SetOpacityValue(0.75);

  // Then we need to link the mouse-move event to all sub-meshes
  LinkPickingMethod(GorillaModel1);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FTriangleIBuf);
  FreeAndNil(FTriangleVBuf);
end;

procedure TForm1.GorillaModel1MouseLeave(Sender: TObject);
begin
  // Deselect mesh
  if Assigned(FSelectedMesh) then
    FSelectedMesh.SetOpacityValue(0.75);

  FSelectedMesh := nil;
  FillChar(FSelectedTriangle, SizeOf(FSelectedTriangle), 0);
end;

procedure TForm1.GorillaModel1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Single; RayPos, RayDir: TVector3D);
var LAbsRayPos, LAbsRayDir : TVector3D;
begin
  var LMesh := Sender as TGorillaMesh;

  // Convert ray info to absolute transformation
  LAbsRayPos := LMesh.LocalToAbsoluteVector(RayPos);
  LAbsRayDir := LMesh.LocalToAbsoluteDirection(RayDir);

  // Preform a pick triangle test
  if LMesh.PickTriangle(LAbsRayPos, LAbsRayDir, FSelectedTriangle) then
    ;
end;

procedure TForm1.TriangleRendererRender(Sender: TObject; Context: TContext3D);
var LCtx : TGorillaContext;
begin
  // Deselect mesh
  if Assigned(FSelectedMesh) then
    FSelectedMesh.SetOpacityValue(0.75);

  if not FSelectedTriangle.Hit then
    Exit;

  LCtx := Context as TGorillaContext;
  LCtx.BeginLineRendering(3, TGorillaLineType.Solid, true);
  try
    // Render the selected object as bounding box
    if (FSelectedTriangle.Instance <> nil) and (FSelectedTriangle.Instance is TGorillaMesh) then
    begin
      FSelectedMesh := (FSelectedTriangle.Instance as TGorillaMesh);
      FSelectedMesh.SetOpacityValue(1.01);
    end;

    // Update our triangle buffer and render a triangle to visualize the selection
    FTriangleVBuf.Vertices[0] := FSelectedTriangle.Triangle[0];
    FTriangleVBuf.Vertices[1] := FSelectedTriangle.Triangle[1];
    FTriangleVBuf.Vertices[2] := FSelectedTriangle.Triangle[2];

    LCtx.DrawLines(FTriangleVBuf, FTriangleIBuf, FTriangleMat.Material, 1);
  finally
    LCtx.EndLineRendering();
  end;
end;

end.
