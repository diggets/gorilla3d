unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Gorilla.Viewport, System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.DefTypes,
  Gorilla.Light, Gorilla.Camera, FMX.Objects3D, Gorilla.Control, Gorilla.Mesh,
  Gorilla.Plane, FMX.MaterialSources, Gorilla.Material.Default,
  Gorilla.Material.Phong, Gorilla.Cube, Gorilla.Material.Lambert;

type
  TForm2 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    Dummy1: TDummy;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaPlane1: TGorillaPlane;
    GorillaPhongMaterialSource1: TGorillaPhongMaterialSource;
    OpaqueCube1: TGorillaCube;
    OpaqueMaterial: TGorillaLambertMaterialSource;
    OpaqueCube2: TGorillaCube;
    OpaqueCube3: TGorillaCube;
    OpaqueCube4: TGorillaCube;
    TranslucentCube1: TGorillaCube;
    TranslucentMaterial: TGorillaLambertMaterialSource;
    TranslucentCube2: TGorillaCube;
    TranslucentCube3: TGorillaCube;
    TranslucentCube4: TGorillaCube;
    TranslucentCube5: TGorillaCube;
    TranslucentCube6: TGorillaCube;
    TranslucentCube7: TGorillaCube;
    TranslucentCube8: TGorillaCube;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

procedure TForm2.FormCreate(Sender: TObject);
begin
  TranslucentCube1.Opaque := false;
  TranslucentCube2.Opaque := false;
  TranslucentCube3.Opaque := false;
  TranslucentCube4.Opaque := false;
  TranslucentCube5.Opaque := false;
  TranslucentCube6.Opaque := false;
  TranslucentCube7.Opaque := false;
  TranslucentCube8.Opaque := false;
end;

procedure TForm2.Timer1Timer(Sender: TObject);
begin
  Dummy1.RotationAngle.Y := Dummy1.RotationAngle.Y + 1;
  GorillaViewport1.Invalidate;
end;

end.
