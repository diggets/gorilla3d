unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Gorilla.Viewport, System.Math.Vectors, FMX.Types3D, FMX.Controls3D,
  FMX.Objects3D, Gorilla.Control, Gorilla.Mesh, Gorilla.Sphere, Gorilla.Cube,
  Gorilla.Tube, Gorilla.Cone, Gorilla.Torus, Gorilla.DefTypes,
  Gorilla.Material.BumpMap, Gorilla.Material.NormalMap, Gorilla.Material.Blinn,
  Gorilla.Material.Phong, FMX.MaterialSources, Gorilla.Material.Default,
  Gorilla.Material.Lambert, Gorilla.Material.Layered, FMX.ListBox,
  Gorilla.Material.SplatMap, Gorilla.Cylinder, Gorilla.Material.RimLighting,
  Gorilla.Transform, Gorilla.Material.POM, Gorilla.Material.PBR;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaSphere1: TGorillaSphere;
    Dummy1: TDummy;
    Camera1: TCamera;
    Light1: TLight;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    GorillaPhongMaterialSource1: TGorillaPhongMaterialSource;
    GorillaBlinnMaterialSource1: TGorillaBlinnMaterialSource;
    GorillaNormalMapMaterialSource1: TGorillaNormalMapMaterialSource;
    GorillaBumpMapMaterialSource1: TGorillaBumpMapMaterialSource;
    GorillaSplatMapMaterialSource1: TGorillaSplatMapMaterialSource;
    GorillaLayeredMaterialSource1: TGorillaLayeredMaterialSource;
    LightMaterialSource1: TLightMaterialSource;
    LightMaterialSource2: TLightMaterialSource;
    GorillaBumpMapMaterialSource2: TGorillaBumpMapMaterialSource;
    ComboBox1: TComboBox;
    Timer1: TTimer;
    ComboBox2: TComboBox;
    GorillaCube1: TGorillaCube;
    GorillaCylinder1: TGorillaCylinder;
    GorillaTorus1: TGorillaTorus;
    GorillaCone1: TGorillaCone;
    GorillaTube1: TGorillaTube;
    LightMaterialSource3: TLightMaterialSource;
    ColorMaterialSource1: TColorMaterialSource;
    TextureMaterialSource1: TTextureMaterialSource;
    GorillaRimLightingMaterialSource1: TGorillaRimLightingMaterialSource;
    GorillaBlinnMaterialSource2: TGorillaBlinnMaterialSource;
    GorillaPBRMaterialSource1: TGorillaPBRMaterialSource;
    GorillaPOMMaterialSource1: TGorillaPOMMaterialSource;

    procedure FormCreate(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
  private
    FActive : TGorillaMesh;
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.IOUtils;

procedure TForm1.ComboBox1Change(Sender: TObject);
begin
  case ComboBox1.ItemIndex of
    // fmx materials
    0 : FActive.MaterialSource := LightMaterialSource3;
    1 : FActive.MaterialSource := ColorMaterialSource1;
    2 : FActive.MaterialSource := TextureMaterialSource1;

    // gorilla materials
    3 : FActive.MaterialSource := GorillaLambertMaterialSource1;
    4 : FActive.MaterialSource := GorillaPhongMaterialSource1;
    5 : FActive.MaterialSource := GorillaBlinnMaterialSource1;
    6 : FActive.MaterialSource := GorillaNormalMapMaterialSource1;
    7 : FActive.MaterialSource := GorillaBumpMapMaterialSource1;
    8 : FActive.MaterialSource := GorillaSplatMapMaterialSource1;
    9 : FActive.MaterialSource := GorillaLayeredMaterialSource1;
    10 : FActive.MaterialSource := GorillaBumpMapMaterialSource2;
    11 : FActive.MaterialSource := GorillaRimLightingMaterialSource1;
    12 : FActive.MaterialSource := GorillaPOMMaterialSource1;
    13 : FActive.MaterialSource := GorillaPBRMaterialSource1;
    14 : FActive.MaterialSource := GorillaBlinnMaterialSource2;

    else ShowMessage('no valid material index');
  end;

  GorillaViewport1.Invalidate();
end;

procedure TForm1.ComboBox2Change(Sender: TObject);
begin
  case ComboBox2.ItemIndex of
    1 : begin
          // activate cube
          GorillaSphere1.Visible := false;
          GorillaCube1.Visible := true;
          GorillaCylinder1.Visible := false;
          GorillaCone1.Visible := false;
          GorillaTube1.Visible := false;
          GorillaTorus1.Visible := false;
          FActive := GorillaCube1;
        end;

    2 : begin
          // activate cylinder
          GorillaSphere1.Visible := false;
          GorillaCube1.Visible := false;
          GorillaCylinder1.Visible := true;
          GorillaCone1.Visible := false;
          GorillaTube1.Visible := false;
          GorillaTorus1.Visible := false;
          FActive := GorillaCylinder1;
        end;

    3 : begin
          // activate cone
          GorillaSphere1.Visible := false;
          GorillaCube1.Visible := false;
          GorillaCylinder1.Visible := false;
          GorillaCone1.Visible := true;
          GorillaTube1.Visible := false;
          GorillaTorus1.Visible := false;
          FActive := GorillaCone1;
        end;

    4 : begin
          // activate tube
          GorillaSphere1.Visible := false;
          GorillaCube1.Visible := false;
          GorillaCylinder1.Visible := false;
          GorillaCone1.Visible := false;
          GorillaTube1.Visible := true;
          GorillaTorus1.Visible := false;
          FActive := GorillaTube1;
        end;

    5 : begin
          // activate torus
          GorillaSphere1.Visible := false;
          GorillaCube1.Visible := false;
          GorillaCylinder1.Visible := false;
          GorillaCone1.Visible := false;
          GorillaTube1.Visible := false;
          GorillaTorus1.Visible := true;
          FActive := GorillaTorus1;
        end;

    else
        begin
          // default: sphere
          GorillaSphere1.Visible := true;
          GorillaCube1.Visible := false;
          GorillaCylinder1.Visible := false;
          GorillaCone1.Visible := false;
          GorillaTube1.Visible := false;
          GorillaTorus1.Visible := false;
          FActive := GorillaSphere1;
        end;
  end;

  ComboBox1Change(ComboBox1);
end;

procedure TForm1.FormCreate(Sender: TObject);
var LTexPath : String;
begin
  LTexPath := IncludeTrailingPathDelimiter('textures');
{$IFDEF ANDROID}
  LTexPath := IncludeTrailingPathDelimiter(TPath.GetHomePath()) + LTexPath;
{$ENDIF}

  // default gouraud material by fmx
  TextureMaterialSource1.Texture.LoadFromFile(LTexPath + 'gift-med.jpg');

  // default gouraud material by fmx
  LightMaterialSource3.Texture.LoadFromFile(LTexPath + 'gift-med.jpg');

  // load lambert material
  GorillaLambertMaterialSource1.Texture.LoadFromFile(LTexPath + 'gift-med.jpg');

  // load phong material
  GorillaPhongMaterialSource1.Texture.LoadFromFile(LTexPath + 'gift-med.jpg');

  // load blinn phong material
  GorillaBlinnMaterialSource1.Texture.LoadFromFile(LTexPath + 'gift-med.jpg');

  // load normal map material
  GorillaNormalMapMaterialSource1.Texture.LoadFromFile(LTexPath + 'layingrock-c.jpg');
  GorillaNormalMapMaterialSource1.NormalMap.LoadFromFile(LTexPath + 'layingrock-n.jpg');

  // load bump map material
  GorillaBumpMapMaterialSource1.Texture.LoadFromFile(LTexPath + 'Rocks01_col.jpg');
  GorillaBumpMapMaterialSource1.NormalMap.LoadFromFile(LTexPath + 'Rocks01_nrm.jpg');
  GorillaBumpMapMaterialSource1.SpecularMap.LoadFromFile(LTexPath + 'Rocks01_AO.jpg');

  // load splatmap material
  GorillaSplatMapMaterialSource1.Texture.LoadFromFile(LTexPath + 'multi-splatmap.png');
  GorillaSplatMapMaterialSource1.SplatTexture0.LoadFromFile(LTexPath + 'floor-wood.jpg');
  GorillaSplatMapMaterialSource1.SplatTexture1.LoadFromFile(LTexPath + 'ground2.jpg');
  GorillaSplatMapMaterialSource1.SplatTexture2.LoadFromFile(LTexPath + 'layingrock-c.jpg');
  GorillaSplatMapMaterialSource1.SplatTexture3.LoadFromFile(LTexPath + 'grass.jpg');
  GorillaSplatMapMaterialSource1.UseTexturing := true;
  GorillaSplatMapMaterialSource1.UseLighting := true;

  // load layered material
  // LightMaterialSource1 and LightMaterialSource2 are already linked in design-mode
  // by Materials property of layered material source.
  // here we only load the textures for each layer
  LightMaterialSource1.Texture.LoadFromFile(LTexPath + 'multi-splatmap.png');
  LightMaterialSource2.Texture.LoadFromFile(LTexPath + 'multi-splatmap2.png');

  // load bumpmap material with displacement map
  GorillaBumpMapMaterialSource2.Texture.LoadFromFile(LTexPath + 'harshbricks-albedo.png');
  GorillaBumpMapMaterialSource2.NormalMap.LoadFromFile(LTexPath + 'harshbricks-normal.png');
  GorillaBumpMapMaterialSource2.SpecularMap.LoadFromFile(LTexPath + 'harshbricks-ao2.png');
  GorillaBumpMapMaterialSource2.DisplacementMap.LoadFromFile(LTexPath + 'harshbricks-height5-16.png');

  // load blinn phong material
  GorillaRimLightingMaterialSource1.Texture.LoadFromFile(LTexPath + 'gift-med.jpg');

  // load parallax occlusion mapping material
  GorillaPOMMaterialSource1.Texture.LoadFromFile(LTexPath + 'pom_wood.png');
  GorillaPOMMaterialSource1.NormalMap.LoadFromFile(LTexPath + 'pom_normal.png');
  GorillaPOMMaterialSource1.ParallaxOcclusionMap.LoadFromFile(LTexPath + 'pom_disp.png');
  GorillaPOMMaterialSource1.NormalIntensity := 0.2;
  GorillaPOMMaterialSource1.ParallaxLevels := 32;

  // load pbr material
  GorillaPBRMaterialSource1.Texture.LoadFromFile(LTexPath + 'PavingStones16_col.jpg');
  GorillaPBRMaterialSource1.NormalMap.LoadFromFile(LTexPath + 'PavingStones16_nrm.jpg');
  GorillaPBRMaterialSource1.AmbientOcclusionTexture.LoadFromFile(LTexPath + 'PavingStones16_AO.jpg');
  GorillaPBRMaterialSource1.MetalnessTexture.LoadFromFile(LTexPath + 'PavingStones16_rgh.jpg');
  GorillaPBRMaterialSource1.RoughnessTexture.LoadFromFile(LTexPath + 'PavingStones16_rgh.jpg');
  GorillaPBRMaterialSource1.DisplacementMap.LoadFromFile(LTexPath + 'PavingStones16_disp.jpg');
  GorillaPBRMaterialSource1.UseParallaxOcclusion := false;
  GorillaPBRMaterialSource1.NormalIntensity := 1;
  GorillaPBRMaterialSource1.MetallicBias := 1;

  // select sphere as default control
  FActive := GorillaSphere1;
  ComboBox1.ItemIndex := 5;
  ComboBox2.ItemIndex := 0;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  FActive.RotationAngle.Y := FActive.RotationAngle.Y + 1;
end;

end.
