unit Unit2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  System.Math,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Types3D,
  System.Math.Vectors, Gorilla.Control, Gorilla.Transform, Gorilla.Mesh,
  Gorilla.Cube, Gorilla.SkyBox, FMX.Controls3D, Gorilla.Light, Gorilla.Viewport,
  Gorilla.Model, Gorilla.Audio.FMOD, FMX.Layers3D, Gorilla.Layers, Gorilla.Plane,
  FMX.MaterialSources, Gorilla.Material.Default, Gorilla.Material.Lambert,
  Gorilla.Camera, Gorilla.Controller, Gorilla.Controller.Passes.Environment,
  FMX.StdCtrls, FMX.Controls.Presentation, Gorilla.Gizmo, FMX.Objects3D,
  Gorilla.Material.Behaviour, Gorilla.Material.Behaviour.Wind, FMX.Ani;

type
  TFret = record
    FromX : TPoint3D;
    ToX   : TPoint3D;
  end;

  TNote = record
    &String : Byte;
    Fret : Byte;
  end;

  TNoteSheet = record
    Notes : TArray<TNote>;
  end;

  TForm4 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaLight1: TGorillaLight;
    GorillaSkyBox1: TGorillaSkyBox;
    GorillaFMODAudioManager1: TGorillaFMODAudioManager;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    GorillaRenderPassEnvironment1: TGorillaRenderPassEnvironment;
    Timer1: TTimer;
    Button1: TButton;
    Dummy1: TDummy;
    GorillaMaterialWindBehaviour1: TGorillaMaterialWindBehaviour;
    StrokeCube1: TStrokeCube;
    Timer2: TTimer;
    GorillaModel1: TGorillaModel;
    procedure FormCreate(Sender: TObject);

    procedure StringEMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single; RayPos, RayDir: TVector3D);
    procedure StringAMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single; RayPos, RayDir: TVector3D);
    procedure StringDMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single; RayPos, RayDir: TVector3D);
    procedure StringGMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single; RayPos, RayDir: TVector3D);
    procedure StringBMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single; RayPos, RayDir: TVector3D);
    procedure StringE2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single; RayPos, RayDir: TVector3D);
    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: WideChar;
      Shift: TShiftState);
    procedure Timer2Timer(Sender: TObject);
  private
    FStrings : Array[0..5] of TGorillaMesh;
    FFrets   : Array[0..19] of TFret;

    FSheet : TNoteSheet;
    FCurrentNote : Integer;
    FIsCleaning : Integer;

    procedure CleanupBehaviours();

  public
    procedure PlayNoteSheet();
    procedure SetupFrets();
    function DetectFret(AString : TGorillaMesh; ARayPos, ARayDir: TVector3D) : Integer;
    procedure PlayString(AString : TGorillaMesh; AIndex : Integer;
      RayPos, RayDir: TVector3D);
  end;

var
  Form4: TForm4;

implementation

{$R *.fmx}

uses
  Gorilla.Utils.Geometry;

procedure TForm4.FormCreate(Sender: TObject);
begin
  StrokeCube1.Visible := false;

  // Attach dummy to camera controller for automatic 3D position detection
  Dummy1.Parent := GorillaViewport1.GetDesignCameraController;

  // Enable clicking by the enabled hittest property
  GorillaModel1.SetHitTestValue(true);

  // String E
  FStrings[0] := GorillaModel1.Meshes[1];
  FStrings[0].Cursor  := crHandPoint;
  FStrings[0].OnMouseUp := StringEMouseUp;

  // String A
  FStrings[1] := GorillaModel1.Meshes[2];
  FStrings[1].Cursor  := crHandPoint;
  FStrings[1].OnMouseUp := StringAMouseUp;

  // String D
  FStrings[2] := GorillaModel1.Meshes[3];
  FStrings[2].Cursor  := crHandPoint;
  FStrings[2].OnMouseUp := StringDMouseUp;

  // String G
  FStrings[3] := GorillaModel1.Meshes[4];
  FStrings[3].Cursor  := crHandPoint;
  FStrings[3].OnMouseUp := StringGMouseUp;

  // String B
  FStrings[4] := GorillaModel1.Meshes[5];
  FStrings[4].Cursor  := crHandPoint;
  FStrings[4].OnMouseUp := StringBMouseUp;

  // String E2
  FStrings[5] := GorillaModel1.Meshes[6];
  FStrings[5].Cursor  := crHandPoint;
  FStrings[5].OnMouseUp := StringE2MouseUp;

  SetupFrets();
end;

procedure TForm4.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: WideChar; Shift: TShiftState);
begin
  case KeyChar of
    'y' : PlayString(FStrings[0], 0, TVector3D.Zero, Vector3D(0, 0, 1, 0));
    'x' : PlayString(FStrings[1], 1, TVector3D.Zero, Vector3D(0, 0, 1, 0));
    'c' : PlayString(FStrings[2], 2, TVector3D.Zero, Vector3D(0, 0, 1, 0));
    'v' : PlayString(FStrings[3], 3, TVector3D.Zero, Vector3D(0, 0, 1, 0));
    'b' : PlayString(FStrings[4], 4, TVector3D.Zero, Vector3D(0, 0, 1, 0));
    'n' : PlayString(FStrings[5], 5, TVector3D.Zero, Vector3D(0, 0, 1, 0));
  end;
end;

procedure TForm4.SetupFrets();
begin
  // CAUTION: Values based on a default x-offset of -2.0!
  FFrets[0].FromX := Point3D(2.35, 0, 0);
  FFrets[0].ToX := Point3D(2.18, 0, 0);

  FFrets[1].FromX := Point3D(2.18, 0, 0);
  FFrets[1].ToX := Point3D(2.01, 0, 0);

  FFrets[2].FromX := Point3D(2.01, 0, 0);
  FFrets[2].ToX := Point3D(1.84, 0, 0);

  FFrets[3].FromX := Point3D(1.84, 0, 0);
  FFrets[3].ToX := Point3D(1.69, 0, 0);

  FFrets[4].FromX := Point3D(1.69, 0, 0);
  FFrets[4].ToX := Point3D(1.55, 0, 0);

  FFrets[5].FromX := Point3D(1.55, 0, 0);
  FFrets[5].ToX := Point3D(1.40, 0, 0);

  FFrets[6].FromX := Point3D(1.40, 0, 0);
  FFrets[6].ToX := Point3D(1.28, 0, 0);

  FFrets[7].FromX := Point3D(1.28, 0, 0);
  FFrets[7].ToX := Point3D(1.16, 0, 0);

  FFrets[8].FromX := Point3D(1.16, 0, 0);
  FFrets[8].ToX := Point3D(1.04, 0, 0);

  FFrets[9].FromX := Point3D(1.04, 0, 0);
  FFrets[9].ToX := Point3D(0.94, 0, 0);

  FFrets[10].FromX := Point3D(0.94, 0, 0);
  FFrets[10].ToX := Point3D(0.84, 0, 0);

  FFrets[11].FromX := Point3D(0.84, 0, 0);
  FFrets[11].ToX := Point3D(0.73, 0, 0);

  FFrets[12].FromX := Point3D(0.73, 0, 0);
  FFrets[12].ToX := Point3D(0.64, 0, 0);

  FFrets[13].FromX := Point3D(0.64, 0, 0);
  FFrets[13].ToX := Point3D(0.55, 0, 0);

  FFrets[14].FromX := Point3D(0.55, 0, 0);
  FFrets[14].ToX := Point3D(0.48, 0, 0);

  FFrets[15].FromX := Point3D(0.48, 0, 0);
  FFrets[15].ToX := Point3D(0.40, 0, 0);

  FFrets[16].FromX := Point3D(0.40, 0, 0);
  FFrets[16].ToX := Point3D(0.33, 0, 0);

  FFrets[17].FromX := Point3D(0.33, 0, 0);
  FFrets[17].ToX := Point3D(0.25, 0, 0);

  FFrets[18].FromX := Point3D(0.25, 0, 0);
  FFrets[18].ToX := Point3D(0.19, 0, 0);

  FFrets[19].FromX := Point3D(0.19, 0, 0);
  FFrets[19].ToX := Point3D(0.12, 0, 0);
end;

procedure TForm4.CleanupBehaviours();
var I : Integer;
    LString : TGorillaMesh;
    LMatSrc : TGorillaDefaultMaterialSource;
begin
  if FIsCleaning > 0 then
    Exit;

  AtomicIncrement(FIsCleaning);
  try
    for I := Low(FStrings) to High(FStrings) do
    begin
      LString := FStrings[I];
      LMatSrc := (LString.MaterialSource as TGorillaDefaultMaterialSource);

      if (LMatSrc.Tag <> 0) then
      begin
        // check if expired
        var LTime := TThread.GetTickCount;
        if (LTime - LMatSrc.Tag) > 2000 then
        begin
          LMatSrc.DetachMaterialBehaviour(GorillaMaterialWindBehaviour1);
          LMatSrc.Tag := 0;

          Log.d('%d.behaviours.detached', [I]);
        end;
      end;
    end;
  finally
    AtomicDecrement(FIsCleaning);
  end;
end;

function TForm4.DetectFret(AString : TGorillaMesh; ARayPos, ARayDir: TVector3D) : Integer;
var LAbsRayPos, LAbsRayDir : TVector3D;
    LIntersect, LNormal : TPoint3D;
    I: Integer;
begin
  Result := 0;

  if not Assigned(AString) then
    Exit;

  LAbsRayPos := AString.LocalToAbsoluteVector(ARayPos);
  LAbsRayDir := AString.LocalToAbsoluteDirection(ARayDir);

  if AString.RayCastIntersect(LAbsRayPos, LAbsRayDir, LIntersect, LNormal) then
  begin
    for I := Low(FFrets) to High(FFrets) do
    begin
      if (LIntersect.X <= FFrets[I].FromX.X) and (LIntersect.X > FFrets[I].ToX.X) then
      begin
        Result := I;
        Exit;
      end;
    end;
  end;
end;

function GetPitchValue(AFret: Integer): Single;
var I : Integer;
begin
  if AFret < 0 then
    raise Exception.Create('Fret number cannot be negative.');

  Result := Power(2, AFret / 12);
end;

procedure TForm4.PlayString(AString : TGorillaMesh; AIndex : Integer;
  RayPos, RayDir: TVector3D);
var LMatSrc : TGorillaDefaultMaterialSource;
begin
  // Detect which fret was clicked
  var LFret := DetectFret(AString, RayPos, RayDir);
  if LFret < 0 then
    Exit;

  // Mark the played string with a bounding box
  var LBBox := AString.GetBoundingBox().Transform(TControl3D(AString.Parent).AbsoluteMatrix);
  var LSize := LBBox.GetSize();
  StrokeCube1.SetSize(LSize.X, LSize.Y, LSize.Z);
  StrokeCube1.Position.Point := LBBox.CenterPoint;

  // Play the specific string sound with correct pitched value
  var LItem := GorillaFMODAudioManager1.Sounds.Items[AIndex] as TGorillaFMODSoundItem;
  LItem.Pitch := GetPitchValue(LFret);
  LItem.Play;

  // Attach wind effect for swinging strings
  Log.d('%d.play', [AIndex]);
  LMatSrc := (AString.MaterialSource as TGorillaDefaultMaterialSource);
  LMatSrc.AttachMaterialBehaviour(GorillaMaterialWindBehaviour1);
  LMatSrc.Tag := TThread.GetTickCount;
  Log.d('%d.behaviours.attached', [AIndex]);
end;

procedure TForm4.StringEMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single; RayPos, RayDir: TVector3D);
begin
  // E - Pitched by 20 frets
  PlayString(Sender as TGorillaMesh, 0, RayPos, RayDir);
end;

procedure TForm4.StringAMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single; RayPos, RayDir: TVector3D);
begin
  // A - Pitched by 20 frets
  PlayString(Sender as TGorillaMesh, 1, RayPos, RayDir);
end;

procedure TForm4.StringDMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single; RayPos, RayDir: TVector3D);
begin
  // D - Pitched by 20 frets
  PlayString(Sender as TGorillaMesh, 2, RayPos, RayDir);
end;

procedure TForm4.StringGMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single; RayPos, RayDir: TVector3D);
begin
  // G - Pitched by 20 frets
  PlayString(Sender as TGorillaMesh, 3, RayPos, RayDir);
end;

procedure TForm4.StringBMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single; RayPos, RayDir: TVector3D);
begin
  // H / B - Pitched by 20 frets
  PlayString(Sender as TGorillaMesh, 4, RayPos, RayDir);
end;

procedure TForm4.StringE2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single; RayPos, RayDir: TVector3D);
begin
  // E (low) - Pitched by 20 frets
  PlayString(Sender as TGorillaMesh, 5, RayPos, RayDir);
end;

procedure TForm4.PlayNoteSheet;
begin
  System.SetLength(FSheet.Notes, 16);

  /// Intro
//  FSheet.Notes[0].&String := 0;
//  FSheet.Notes[0].Fret := 4;
//  FSheet.Notes[1].&String := 4;
//  FSheet.Notes[1].Fret := 6;
//  FSheet.Notes[2].&String := 3;
//  FSheet.Notes[2].Fret := 4;
//  FSheet.Notes[3].&String := 2;
//  FSheet.Notes[3].Fret := 6;
//  FSheet.Notes[4].&String := 3;
//  FSheet.Notes[4].Fret := 4;
//  FSheet.Notes[5].&String := 2;
//  FSheet.Notes[5].Fret := 6;
//  FSheet.Notes[6].&String := 1;
//  FSheet.Notes[6].Fret := 4;
//  FSheet.Notes[7].&String := 0;
//  FSheet.Notes[7].Fret := 6;
//  FSheet.Notes[8].&String := 4;
//  FSheet.Notes[8].Fret := 6;

  // Melodie
  // E=0, A=1  D=2, G=3, B=4, E2=5

  // E 5  B	7   G	5   D	7
  FSheet.Notes[0].&String := 0;
  FSheet.Notes[0].Fret := 4;
  FSheet.Notes[1].&String := 4;
  FSheet.Notes[1].Fret := 6;
  FSheet.Notes[2].&String := 3;
  FSheet.Notes[2].Fret := 4;
  FSheet.Notes[3].&String := 2;
  FSheet.Notes[3].Fret := 6;

  // D 7  G	5   D	7   E	5
  FSheet.Notes[4].&String := 2;
  FSheet.Notes[4].Fret := 6;
  FSheet.Notes[5].&String := 3;
  FSheet.Notes[5].Fret := 4;
  FSheet.Notes[6].&String := 2;
  FSheet.Notes[6].Fret := 6;
  FSheet.Notes[7].&String := 0;
  FSheet.Notes[7].Fret := 4;

  // B 7  A	5   E	7   B	7
  FSheet.Notes[8].&String := 4;
  FSheet.Notes[8].Fret := 6;
  FSheet.Notes[9].&String := 1;
  FSheet.Notes[9].Fret := 4;
  FSheet.Notes[10].&String := 0;
  FSheet.Notes[10].Fret := 6;
  FSheet.Notes[11].&String := 4;
  FSheet.Notes[11].Fret := 6;

  // G 5  D	7   G	5   E	5
  FSheet.Notes[12].&String := 3;
  FSheet.Notes[12].Fret := 4;
  FSheet.Notes[13].&String := 2;
  FSheet.Notes[13].Fret := 6;
  FSheet.Notes[14].&String := 3;
  FSheet.Notes[14].Fret := 4;
  FSheet.Notes[15].&String := 0;
  FSheet.Notes[15].Fret := 4;

  FCurrentNote := 0;
  Timer1.Enabled := true;
end;

procedure TForm4.Timer1Timer(Sender: TObject);
begin
  if (FCurrentNote >= Length(FSheet.Notes)) then
  begin
    FCurrentNote := 0;
    Exit;
  end;

  var LString := FSheet.Notes[FCurrentNote].&String;
  var LFret := FSheet.Notes[FCurrentNote].Fret;

  var LItem := GorillaFMODAudioManager1.Sounds.Items[LString] as TGorillaFMODSoundItem;
  LItem.Pitch := GetPitchValue(LFret);
  LItem.Play;

  Inc(FCurrentNote);
end;

procedure TForm4.Timer2Timer(Sender: TObject);
begin
  CleanupBehaviours();
end;

procedure TForm4.Button1Click(Sender: TObject);
begin
  PlayNoteSheet;
end;


end.
