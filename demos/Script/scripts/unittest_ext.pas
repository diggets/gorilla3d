program unittest_ext;

uses
	System.SysUtils, FMX.StdCtrls;

	procedure Main();
	var LBtn : TButton;
	begin
		System.WriteLn('--- UNITTEST - EXTENSIONS --------------------------------');
		LBtn := TButton.Create(nil);
		System.WriteLn('Button created: ' + LBtn.ToString());
	end;

begin
	Main();
end.