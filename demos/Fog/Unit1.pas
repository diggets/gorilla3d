unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light, Gorilla.DefTypes,
  FMX.Objects3D, Gorilla.Camera, Gorilla.Viewport, FMX.MaterialSources,
  Gorilla.Material.Default, Gorilla.Material.Lambert, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Plane, FMX.StdCtrls, FMX.Controls.Presentation,
  FMX.ListBox, FMX.Colors, Gorilla.Transform;

type
  TForm2 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaCamera1: TGorillaCamera;
    Dummy1: TDummy;
    GorillaLight1: TGorillaLight;
    GorillaPlane1: TGorillaPlane;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    TrackBar1: TTrackBar;
    GroupBox1: TGroupBox;
    TrackBar2: TTrackBar;
    CheckBox1: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    TrackBar3: TTrackBar;
    Label3: TLabel;
    ComboBox1: TComboBox;
    ColorPicker1: TColorPicker;
    ColorQuad1: TColorQuad;
    ColorBox1: TColorBox;
    Timer1: TTimer;
    procedure ColorQuad1Change(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure TrackBar2Change(Sender: TObject);
    procedure TrackBar3Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    procedure ReadFogSettingsFromViewport();
    procedure ApplyFogSettingsToViewport();
  public
    { Public-Deklarationen }
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

uses
  Gorilla.Context.Rendering;

procedure TForm2.FormCreate(Sender: TObject);
begin
  ReadFogSettingsFromViewport();
end;

procedure TForm2.CheckBox1Change(Sender: TObject);
begin
  ApplyFogSettingsToViewport();
end;

procedure TForm2.ColorQuad1Change(Sender: TObject);
begin
  ApplyFogSettingsToViewport();
end;

procedure TForm2.ComboBox1Change(Sender: TObject);
begin
  ApplyFogSettingsToViewport();
end;

procedure TForm2.ReadFogSettingsFromViewport();
begin
  CheckBox1.IsChecked := GorillaViewport1.Fog;

  case GorillaViewport1.FogMode of
    TGorillaFogMode.fmExp  : ComboBox1.ItemIndex := 1;
    TGorillaFogMode.fmExp2 : ComboBox1.ItemIndex := 2;
    else ComboBox1.ItemIndex := 0;
  end;

  TrackBar1.Value := GorillaViewport1.FogStart;
  TrackBar2.Value := GorillaViewport1.FogEnd;
  TrackBar3.Value := GorillaViewport1.FogDensity;

  ColorPicker1.Color := GorillaViewport1.FogColor;

  // preset fog distance
  TrackBar1.Value := 5;
  TrackBar2.Value := 200;
end;

procedure TForm2.Timer1Timer(Sender: TObject);
begin
  Dummy1.RotationAngle.Y := Dummy1.RotationAngle.Y + 1;
end;

procedure TForm2.TrackBar1Change(Sender: TObject);
begin
  ApplyFogSettingsToViewport();
end;

procedure TForm2.TrackBar2Change(Sender: TObject);
begin
  ApplyFogSettingsToViewport();
end;

procedure TForm2.TrackBar3Change(Sender: TObject);
begin
  ApplyFogSettingsToViewport();
end;

procedure TForm2.ApplyFogSettingsToViewport();
var LClr : TAlphaColorF;
begin
  GorillaViewport1.BeginUpdate();
  try
    GorillaViewport1.Fog := CheckBox1.IsChecked;
    GorillaViewport1.FogStart := TrackBar1.Value;
    GorillaViewport1.FogEnd := TrackBar2.Value;
    GorillaViewport1.FogDensity := TrackBar3.Value;

    case ComboBox1.ItemIndex of
      1 : GorillaViewport1.FogMode := TGorillaFogMode.fmExp;
      2 : GorillaViewport1.FogMode := TGorillaFogMode.fmExp2;
      else GorillaViewport1.FogMode := TGorillaFogMode.fmLinear;
    end;

    LClr := TAlphaColorF.Create(ColorBox1.Color);
    GorillaViewport1.FogColorF := LClr;
  finally
    GorillaViewport1.EndUpdate();
  end;
end;

end.
