unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light, Gorilla.DefTypes,
  Gorilla.Camera, Gorilla.Viewport, Gorilla.Control, Gorilla.Mesh, Gorilla.Plane,
  FMX.MaterialSources, Gorilla.Material.Default, Gorilla.Material.Lambert,
  Gorilla.Controller, Gorilla.Controller.Input, FMX.Objects3D,
  Gorilla.Controller.Input.Character, Gorilla.Controller.Input.FirstPerson,
  FMX.Controls.Presentation, FMX.StdCtrls, Gorilla.Cone, Gorilla.Controller.Input.Types,
  Gorilla.Controller.Input.TriggerPoint, FMX.Layers3D, Gorilla.Layers,
  Gorilla.Transform;

type
  TForm2 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaPlane1: TGorillaPlane;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    GorillaInputController1: TGorillaInputController;
    GorillaFirstPersonController1: TGorillaFirstPersonController;
    Label1: TLabel;
    GorillaCone1: TGorillaCone;
    GorillaTriggerPointManager1: TGorillaTriggerPointManager;
    GorillaTextLayer3D1: TGorillaTextLayer3D;
    Timer1: TTimer;
    Label2: TLabel;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure GorillaFirstPersonController1CustomAction(ASender: TObject;
      const AKind: TGorillaCharacterControllerHotKey;
      const AHotKey: TGorillaHotKeyItem;
      const AStates: TGorillaCharacterControllerStates;
      const AMode: TGorillaInputMode);
    procedure CheckBox2Change(Sender: TObject);
  private
    FConeRange : Single;
    FConeSpeed : Single;
    FMove      : Single;
    FIsMoving  : Boolean;

    FTP_Nearby : TGorillaTriggerPoint;
    FTP_InDir  : TGorillaTriggerPoint;

    procedure DoOnTriggerPoint(ASender : TGorillaTriggerPointManager;
      const APos : TPoint3D; const AViewDir : TPoint3D;
      const APoint : TGorillaTriggerPoint; const ADistance : Single);
    procedure DoOnUnTriggerPoint(ASender : TGorillaTriggerPointManager;
      const APos : TPoint3D; const AViewDir : TPoint3D;
      const APoint : TGorillaTriggerPoint; const ADistance : Single);
  public
    { Public-Deklarationen }
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

procedure TForm2.FormCreate(Sender: TObject);
begin
  FConeRange := 15;
  FConeSpeed := 0.01;
  FMove := 0;
  FIsMoving := true;

  // render textlayer content
  GorillaTextLayer3D1.UpdateBuffer();
  GorillaTextLayer3D1.Visible := false;

  // set callback method
  // in nearby trigger-point
  FTP_Nearby := GorillaTriggerPointManager1.Items.Items[0] as TGorillaTriggerPoint;
  FTP_Nearby.OnTriggered := DoOnTriggerPoint;
  FTP_Nearby.OnUnTriggered := DoOnUnTriggerPoint;

  // in look-in-direction trigger-point
  FTP_InDir := GorillaTriggerPointManager1.Items.Items[1] as TGorillaTriggerPoint;
  FTP_InDir.OnTriggered := DoOnTriggerPoint;
  FTP_InDir.OnUnTriggered := DoOnUnTriggerPoint;
end;

procedure TForm2.FormShow(Sender: TObject);
begin
  // enable user control
  GorillaInputController1.Enabled := true;
  Timer1.Enabled := true;
end;

procedure TForm2.GorillaFirstPersonController1CustomAction(ASender: TObject;
  const AKind: TGorillaCharacterControllerHotKey;
  const AHotKey: TGorillaHotKeyItem;
  const AStates: TGorillaCharacterControllerStates;
  const AMode: TGorillaInputMode);
begin
  case AKind of
    TGorillaCharacterControllerHotKey.KeyboardCustomAction1 :
      begin
        // switch movement
        CheckBox2.IsChecked := not CheckBox2.IsChecked;
      end;

    TGorillaCharacterControllerHotKey.KeyboardCustomAction2 :
      begin
        // switch types
        CheckBox1.IsChecked := not CheckBox1.IsChecked;
      end;
  end;
end;

procedure TForm2.Timer1Timer(Sender: TObject);
begin
  // leave if we do not move the cone
  if not FIsMoving then
    Exit;

  // we move the cone to have a moving target simulation
  GorillaViewport1.BeginUpdate();
  try
    GorillaCone1.Position.Point := Point3D(Sin(FMove) * FConeRange, 0, Cos(FMove) * FConeRange);
    FMove := FMove + FConeSpeed;
  finally
    GorillaViewport1.EndUpdate();
  end;
end;

procedure TForm2.CheckBox1Change(Sender: TObject);
begin
  // switches between trigger point types
  // 1 - only nearby, 2 - nearby and looking in direction
  if CheckBox1.IsChecked then
  begin
    FTP_Nearby.Enabled := false;
    FTP_InDir.Enabled  := true;
  end
  else
  begin
    FTP_Nearby.Enabled := true;
    FTP_InDir.Enabled  := false;
  end;
end;

procedure TForm2.CheckBox2Change(Sender: TObject);
begin
  // switches between trigger point detection kinds (static or dynamic)
  // and enables / disables the cone movement
  FIsMoving := CheckBox2.IsChecked;

  if FIsMoving then
  begin
    FTP_Nearby.Kind := TGorillaTriggerPointKind.DynamicTriggerPoint;
    FTP_InDir.Kind  := TGorillaTriggerPointKind.DynamicTriggerPoint;
  end
  else
  begin
    FTP_Nearby.Kind := TGorillaTriggerPointKind.StaticTriggerPoint;
    FTP_InDir.Kind  := TGorillaTriggerPointKind.StaticTriggerPoint;
  end;
end;

procedure TForm2.DoOnTriggerPoint(ASender : TGorillaTriggerPointManager;
  const APos : TPoint3D; const AViewDir : TPoint3D;
  const APoint : TGorillaTriggerPoint; const ADistance : Single);
begin
  if not GorillaTextLayer3D1.Visible then
    GorillaTextLayer3D1.Visible := true;
end;

procedure TForm2.DoOnUnTriggerPoint(ASender : TGorillaTriggerPointManager;
  const APos : TPoint3D; const AViewDir : TPoint3D;
  const APoint : TGorillaTriggerPoint; const ADistance : Single);
begin
  if GorillaTextLayer3D1.Visible then
    GorillaTextLayer3D1.Visible := false;
end;

end.
