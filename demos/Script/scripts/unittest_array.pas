program unittest_array;

uses
	System.SysUtils;

	procedure Main();
	var LArr1 : TArray;
	begin
		System.WriteLn('--- UNITTEST - ARRAY-STATEMENTS --------------------------------');

		System.WriteLn('Should compute: [abc, 123,25, True, (empty), abc]');
		LArr1 := TArray.Create(5);
		LArr1[0] := 'abc';
		LArr1[1] := 123.25;
		LArr1[2] := true;
		LArr1[4] := LArr1[0];
		System.WriteLn(LArr1.ToString() + #13#10);

		System.WriteLn('Should compute: [2, 123,25, 242.5]');
		LArr1 := TArray.Create(3);
		LArr1[0] := 2;
		LArr1[1] := 123.25;
		LArr1[2] := (LArr1[1] - LArr1[0]) * 2;
		System.WriteLn(LArr1.ToString() + #13#10);

		System.WriteLn('IsEmpty() should compute: FALSE');
		System.WriteLn(LArr1.IsEmpty().ToString() + #13#10);

		System.WriteLn('Length() should compute: 3');
		System.WriteLn(LArr1.Length().ToString() + #13#10);

		System.WriteLn('IsEmpty() should compute: True');
		LArr1 := TArray.Create(0);
		System.WriteLn(LArr1.IsEmpty().ToString() + #13#10);

		System.WriteLn('SetLength() should compute: [2, 123,25, (empty), (empty), (empty)]');
		LArr1 := TArray.Create(2);
		LArr1[0] := 2;
		LArr1[1] := 123.25;
		LArr1 := LArr1.SetLength(5);
		System.WriteLn(LArr1.ToString() + #13#10);

		System.WriteLn('Peek() + High() should compute: "highest value"');
		LArr1[LArr1.High()] := 'highest value';
		System.WriteLn(LArr1.Peek().ToString() + #13#10);

		System.WriteLn('Low() should compute: "lowest value"');
		LArr1[0] := 'lowest value';
		System.WriteLn(LArr1[LArr1.Low()].ToString() + #13#10);
		System.WriteLn('First() should compute: "lowest value"');
		System.WriteLn(LArr1.First().ToString() + #13#10);

		System.WriteLn('Slice() should compute: [(empty), (empty), (empty)]');
		LArr1 := TArray.Create(2);
		LArr1[0] := 'Hello';
		LArr1[1] := 'World';
		LArr1 := LArr1.SetLength(5);
		LArr1 := LArr1.Slice(2);
		System.WriteLn(LArr1.ToString() + #13#10);
		
		System.WriteLn('Reduce() should compute: ["Hello", "World"]');
		LArr1 := TArray.Create(2);
		LArr1[0] := 'Hello';
		LArr1[1] := 'World';
		LArr1 := LArr1.SetLength(5);
		LArr1 := LArr1.Reduce(3);
		System.WriteLn(LArr1.ToString() + #13#10);
		
		System.WriteLn('Copy() should compute: ["World"]');
		LArr1 := TArray.Create(2);
		LArr1[0] := 'Hello';
		LArr1[1] := 'World';
		LArr1 := LArr1.Copy(1, 1);
		System.WriteLn(LArr1.ToString() + #13#10);
	end;

begin
	Main();
end.