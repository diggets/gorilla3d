unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.MaterialSources,
  Gorilla.Material.Default, Gorilla.Material.Lambert, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Plane, FMX.Controls3D, Gorilla.Light, Gorilla.Camera,
  FMX.Objects3D, Gorilla.Viewport, Gorilla.Torus, FMX.Ani,
  Gorilla.Material.Blinn, Gorilla.Controller.Passes.SMStoreDepth,
  Gorilla.Animation, FMX.Controls.Presentation, FMX.StdCtrls, FMX.Objects,
  FMX.ListBox;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    Dummy1: TDummy;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaPlane1: TGorillaPlane;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    GorillaTorus1: TGorillaTorus;
    GorillaBlinnMaterialSource1: TGorillaBlinnMaterialSource;
    FloatAnimation2: TFloatAnimation;
    FloatAnimation1: TFloatAnimation;
    FloatAnimation3: TFloatAnimation;
    Sphere1: TSphere;
    ToolBar1: TToolBar;
    ComboBox1: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure GorillaViewport1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure GorillaViewport1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Single);
    procedure GorillaViewport1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure ComboBox1Change(Sender: TObject);
  private
    FMove : Boolean;
    FLastPos : TPointF;

    FShadowMaps : TArray<TGorillaRenderPassSMStoreDepth>;
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  Gorilla.Material.Types;

procedure TForm1.FormCreate(Sender: TObject);
var LShadowMap : TGorillaRenderPassSMStoreDepth;
begin
  /// disable hittest to allow viewport mouse movement
  GorillaPlane1.SetHitTestValue(false);
  GorillaTorus1.SetHitTestValue(false);

  /// disable frustumculling check for torus, because in shadowmap render pass
  /// it may be invisible and may be clipped
  GorillaTorus1.FrustumCullingCheck := false;

  /// setting up an array for multiple shadowmaps
  /// NOTICE: Currently only 1 shadow map supported due to a bug
  System.SetLength(FShadowMaps, 1);

  /// create shadow map render pass for GorillaLight1
  /// CAUTION: for each light source a single shadow map is needed
  LShadowMap := TGorillaRenderPassSMStoreDepth.Create(GorillaViewport1, 'ShadowMapLight1');
  LShadowMap.MapSize := Point(512, 512);
  LShadowMap.Viewport := GorillaViewport1;
  LShadowMap.Light := GorillaLight1;
  LShadowMap.IgnoreControl(Sphere1);
  FShadowMaps[0] := LShadowMap;

  /// we then apply the shadow map to the material where the shadow should appear
  /// in this demo, we want the shadow to appear on the plane
  /// 1) Material for the plane
  GorillaLambertMaterialSource1.Shadows := true;
  GorillaLambertMaterialSource1.ShadowMethod := Gorilla.Material.Types.TGorillaShadowMethod.ShadowMapping;
  GorillaLambertMaterialSource1.ShadowMapPass[0] := FShadowMaps[0];

  /// KNOWN BUGS in v0.8.3.2265:
  /// + only for 1 light source allowed
  /// + only point lights working properly
  /// + if light direction vector (0,1,0) or (0,-1,0) shadowmapping fails
  /// + it can also be applied only to default material sources with enabled "UseTexturing" property!
end;

procedure TForm1.ComboBox1Change(Sender: TObject);
begin
  case ComboBox1.ItemIndex of
    0 : begin
          GorillaLight1.LightType := TLightType.Directional;
          GorillaLight1.ResetRotationAngle();
          GorillaLight1.RotationAngle.X := -75;
        end;

    2 : begin
          GorillaLight1.LightType := TLightType.Spot;
          GorillaLight1.ResetRotationAngle();
          GorillaLight1.RotationAngle.X := -75;
        end;

    else
        begin
          GorillaLight1.LightType := TLightType.Point;
          GorillaLight1.ResetRotationAngle();
          GorillaLight1.RotationAngle.X := 0;
        end;
  end;

  GorillaViewport1.Invalidate();
end;

/// MOUSE INTERACTION //////////////////////////////////////////////////////////

procedure TForm1.GorillaViewport1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  FMove := true;
end;

procedure TForm1.GorillaViewport1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Single);
var LDiff : TPointF;
begin
  if not FMove then
  begin
    FLastPos := PointF(X, Y);
    Exit;
  end;

  if (ssLeft in Shift) then
  begin
    LDiff := PointF(X, Y) - FLastPos;
    Dummy1.RotationAngle.Y := Dummy1.RotationAngle.Y + LDiff.X;
  end
  else if (ssRight in Shift) then
  begin
    LDiff := PointF(X, Y) - FLastPos;
    GorillaCamera1.Position.Y := GorillaCamera1.Position.Y + LDiff.X;
  end;

  FLastPos := PointF(X, Y);
end;

procedure TForm1.GorillaViewport1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  FMove := false;
end;

end.
