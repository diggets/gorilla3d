unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Gorilla.Viewport, System.Math.Vectors, FMX.Types3D, FMX.Objects3D, Gorilla.DefTypes,
  FMX.Controls3D, Gorilla.Control, Gorilla.Mesh, Gorilla.Model,
  Gorilla.AssetsManager, FMX.Controls.Presentation, FMX.StdCtrls, FMX.Gestures,
  SelectWindow, FMX.ListBox, Gorilla.Transform, Gorilla.Cube, Gorilla.SkyBox;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaAssetsManager1: TGorillaAssetsManager;
    GorillaModel1: TGorillaModel;
    Camera1: TCamera;
    Light1: TLight;
    Dummy1: TDummy;
    Button1: TButton;
    TrackBar1: TTrackBar;
    GestureManager1: TGestureManager;
    TrackBar2: TTrackBar;
    ComboBox1: TComboBox;
    Timer1: TTimer;
    GorillaSkyBox1: TGorillaSkyBox;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure GorillaViewport1Gesture(Sender: TObject;
      const EventInfo: TGestureEventInfo; var Handled: Boolean);
    procedure TrackBar2Change(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
  private
    FPackage : TGorillaAssetsPackage;
    FLastPos : TPointF;
  public
    procedure DoOnSelectFile(ASelected : String);
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.Math,
  System.IOUtils,
  Gorilla.G3D.Loader,
  Gorilla.DAE.Loader,
  Gorilla.OBJ.Loader,
  Gorilla.STL.Loader,
  Gorilla.FBX.Loader,
  Gorilla.PLY.Loader,
{$IFDEF MSWINDOWS}
  {$IFDEF CPUX64}
  Gorilla.SKP.Loader,
  {$ENDIF}
{$ENDIF}
  Gorilla.GLTF.Loader,
  Gorilla.GLB.Loader,
  Gorilla.Babylon.Loader,
  Gorilla.X3D.Loader,
  Gorilla.X3DZ.Loader,
  Gorilla.X3DVZ.Loader;

procedure TForm1.Button1Click(Sender: TObject);
begin
{$IFDEF ANDROID}
  FMX.Dialogs.InputQuery('', ['Filename'], [TPath.GetSharedDownloadsPath()],
    procedure(const AResult: TModalResult; const AValues: array of string)
    begin
      if (AResult = mrOK) then
      begin
        DoOnSelectFile(AValues[0]);
      end;
    end);
{$ELSE}
  SelectFile(DoOnSelectFile);
{$ENDIF}
end;

procedure TForm1.ComboBox1Change(Sender: TObject);
begin
  case ComboBox1.ItemIndex of
    0 : TrackBar2.Value := GorillaModel1.RotationAngle.X;
    1 : TrackBar2.Value := GorillaModel1.RotationAngle.Y;
    2 : TrackBar2.Value := GorillaModel1.RotationAngle.Z;
  end;
end;

procedure TForm1.DoOnSelectFile(ASelected: String);
begin
  if ASelected.IsEmpty then
    Exit;

  GorillaModel1.DisposeOf();
  GorillaModel1 := TGorillaModel.LoadNewModelFromFile(GorillaViewport1, FPackage,
    ASelected, []);
  GorillaModel1.Parent := GorillaViewport1;
  GorillaModel1.Scale.Point := Point3D(TrackBar1.Value, TrackBar1.Value, TrackBar1.Value);

  if (GorillaModel1.AnimationManager.Animations.Count > 0) then
    GorillaModel1.AnimationManager.Current.Start();

  ComboBox1Change(ComboBox1);
end;

procedure TForm1.FormCreate(Sender: TObject);
var LPath : String;
begin
  LPath := 'elf-rigged-anim-walk.dae';
{$IFDEF ANDROID}
  LPath := IncludeTrailingPathDelimiter(TPath.GetHomePath()) + LPath;
{$ENDIF}

  FPackage := GorillaAssetsManager1.AddEmptyPackage('TestPackage');
  GorillaModel1.LoadFromFile(FPackage, LPath, []);
  if GorillaModel1.AnimationManager.Animations.Count > 0 then
    GorillaModel1.AnimationManager.Current.Start();
  GorillaModel1.HitTest := false;
  GorillaModel1.WrapMode := TMeshWrapMode.Fit;

  ComboBox1Change(ComboBox1);
end;

procedure TForm1.GorillaViewport1Gesture(Sender: TObject;
  const EventInfo: TGestureEventInfo; var Handled: Boolean);

  procedure DoZoom();
  var LScale : Single;
  begin
    if not (TInteractiveGestureFlag.gfBegin in EventInfo.Flags)
    and not (TInteractiveGestureFlag.gfEnd in EventInfo.Flags) then
    begin
      LScale := EventInfo.Distance / 100;
      GorillaModel1.Scale.Point := Point3D(LScale, LScale, LScale);
    end;
  end;

  procedure DoRotate();
  var LDist : Single;
      LDiff,
      LNew,
      LPos  : TPointF;
  begin
    LNew := EventInfo.Location;

    if not Assigned(GorillaModel1) then
    begin
      FLastPos := LNew;
      Exit;
    end;

    if not (TInteractiveGestureFlag.gfBegin in EventInfo.Flags)
    and not (TInteractiveGestureFlag.gfEnd in EventInfo.Flags) then
    begin
      LPos := EventInfo.Location;
      LDiff := FLastPos - LPos;

      LDist := LDiff.X / 5;

      case ComboBox1.ItemIndex of
        0 : GorillaModel1.RotationAngle.X := GorillaModel1.RotationAngle.X + LDist;
        1 : GorillaModel1.RotationAngle.Y := GorillaModel1.RotationAngle.Y + LDist;
        2 : GorillaModel1.RotationAngle.Z := GorillaModel1.RotationAngle.Z + LDist;
      end;

      ComboBox1Change(ComboBox1);
    end;

    FLastPos := LNew;
  end;

begin
  // just a dirty and quick solution for gesture handling
  case EventInfo.GestureID of
    igiZoom :
      begin
        // zoom
        DoZoom();
        Handled := true;
      end;

    igiPan :
      begin
        // rotate model
        DoRotate();
        Handled := true;
      end;

    else
      begin
        Handled := false;
      end;
  end;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  GorillaModel1.Scale.Point := Point3D(TrackBar1.Value, TrackBar1.Value, TrackBar1.Value);
end;

procedure TForm1.TrackBar2Change(Sender: TObject);
begin
  case ComboBox1.ItemIndex of
    0 : GorillaModel1.RotationAngle.X := TrackBar2.Value;
    1 : GorillaModel1.RotationAngle.Y := TrackBar2.Value;
    2 : GorillaModel1.RotationAngle.Z := TrackBar2.Value;
  end;
end;

end.
