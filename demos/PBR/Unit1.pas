unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light,
  Gorilla.Camera, FMX.Objects3D, Gorilla.Viewport, Gorilla.Cube, Gorilla.SkyBox,
  Gorilla.Control, Gorilla.Mesh, Gorilla.Model, Gorilla.Controller.Passes.Environment,
  FMX.Ani, FMX.StdCtrls, FMX.Controls.Presentation, FMX.Objects, FMX.TabControl,
  FMX.MaterialSources, Gorilla.Material.Default, Gorilla.Material.Lambert,
  FMX.Colors, Gorilla.Sphere;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    Dummy1: TDummy;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaModel1: TGorillaModel;
    GorillaSkyBox1: TGorillaSkyBox;
    FloatAnimation1: TFloatAnimation;
    TrackBar1: TTrackBar;
    Label1: TLabel;
    Label2: TLabel;
    TrackBar2: TTrackBar;
    Label3: TLabel;
    TrackBar3: TTrackBar;
    TrackBar4: TTrackBar;
    Label4: TLabel;
    Rectangle1: TRectangle;
    Rectangle2: TRectangle;
    Rectangle3: TRectangle;
    Rectangle4: TRectangle;
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    TabItem2: TTabItem;
    AlbedoTex: TImage;
    RoughnessTex: TImage;
    MetalnessTex: TImage;
    NormalTex: TImage;
    AOTex: TImage;
    HeightTex: TImage;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    TabItem3: TTabItem;
    IsPOM: TCheckBox;
    GorillaCube2: TGorillaCube;
    GorillaLambertMaterialSource2: TGorillaLambertMaterialSource;
    GorillaCube1: TGorillaCube;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    Timer1: TTimer;
    FloatAnimation2: TFloatAnimation;
    FloatAnimation3: TFloatAnimation;
    ComboColorBox1: TComboColorBox;
    Label11: TLabel;
    ComboColorBox2: TComboColorBox;
    Label12: TLabel;
    ComboColorBox3: TComboColorBox;
    Label13: TLabel;
    GorillaLight2: TGorillaLight;
    GorillaLight3: TGorillaLight;
    Dummy2: TDummy;
    FloatAnimation4: TFloatAnimation;
    GorillaSphere1: TGorillaSphere;
    GorillaSphere2: TGorillaSphere;
    GorillaLambertMaterialSource3: TGorillaLambertMaterialSource;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    Label5: TLabel;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    TabItem4: TTabItem;
    ComboColorBox4: TComboColorBox;
    Label15: TLabel;
    CheckBox5: TCheckBox;
    ComboColorBox5: TComboColorBox;
    Label14: TLabel;
    CheckBox6: TCheckBox;
    ComboColorBox6: TComboColorBox;
    Label16: TLabel;
    CheckBox7: TCheckBox;
    CheckBox8: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure TrackBar2Change(Sender: TObject);
    procedure TrackBar3Change(Sender: TObject);
    procedure TrackBar4Change(Sender: TObject);
    procedure GorillaViewport1MouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; var Handled: Boolean);
    procedure IsPOMChange(Sender: TObject);
    procedure UseLightChange(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ComboColorBox1Change(Sender: TObject);
    procedure ComboColorBox2Change(Sender: TObject);
    procedure ComboColorBox3Change(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure CheckBox2Change(Sender: TObject);
    procedure CheckBox3Change(Sender: TObject);
    procedure CheckBox4Change(Sender: TObject);
    procedure CheckBox5Change(Sender: TObject);
    procedure CheckBox6Change(Sender: TObject);
    procedure CheckBox7Change(Sender: TObject);
    procedure ComboColorBox4Change(Sender: TObject);
    procedure ComboColorBox5Change(Sender: TObject);
    procedure ComboColorBox6Change(Sender: TObject);
    procedure CheckBox8Change(Sender: TObject);
  private
    FPath : String;
    FEnvironment : TGorillaRenderPassEnvironment;
    FInitiated   : Boolean;

    procedure ApplyEnvironmentMap(AComp : TComponent);

    function ReadFirstPBRSettings(AComp : TComponent) : Boolean;
    procedure ApplyPBRSettings(AComp : TComponent);
    procedure ReadFirstLightSettings();
    procedure ApplyLightSettings();
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.IOUtils,
  Gorilla.GLTF.Loader,
  Gorilla.Material.PBR;

procedure TForm1.FormCreate(Sender: TObject);
begin
  /// Get a platform dependent assets path
{$IFDEF MSWINDOWS}
  FPath := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))) +
    IncludeTrailingPathDelimiter('marble_bust_01_1k');
{$ELSE}
  FPath := System.IOUtils.TPath.GetHomePath();
{$ENDIF}

  /// Load our PBR material mesh directly from gltf file
  GorillaModel1.LoadFromFile(nil, FPath + 'marble_bust_01_1k.gltf', []);
  GorillaModel1.RotationAngle.X := 180;
  GorillaModel1.Scale.Point := Point3D(10, 10, 10);
  GorillaModel1.Position.Y := 2.5;
  GorillaModel1.SetHitTestValue(false);

  /// Create a render pass for a dynamic environment map
  FEnvironment := TGorillaRenderPassEnvironment.Create(GorillaViewport1);
  FEnvironment.Viewport := GorillaViewport1;
  FEnvironment.IgnoreControl(GorillaModel1);
  FEnvironment.IsDynamic := true;
  FEnvironment.Enabled := true;

  /// We need to link our environment to all PBR materials inside of the model
  ApplyEnvironmentMap(GorillaModel1);

  FInitiated := false;
  try
    /// Initially we shall read the PBR material settings from the first PBR material
    /// inside of the model and adjust TrackBarX.Value
    ReadFirstPBRSettings(GorillaModel1);

    /// Initially read light settings
    ReadFirstLightSettings();
  finally
    FInitiated := true;
  end;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  Self.Caption := Format('Gorilla3D - Physically Based Rendering Demo (%n FPS)', [GorillaViewport1.FPS]);
end;

procedure TForm1.GorillaViewport1MouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; var Handled: Boolean);
begin
  /// Zoom in and out
  if WheelDelta < 0 then
    GorillaCamera1.Position.Z := GorillaCamera1.Position.Z - 0.25
  else
    GorillaCamera1.Position.Z := GorillaCamera1.Position.Z + 0.25;
end;

procedure TForm1.IsPOMChange(Sender: TObject);
begin
  /// On activating / deactivating Parallax Occlusion Mapping we shall fix NormalIntensity
  if IsPOM.IsChecked then
    TrackBar4.Value := 0.01 * 100
  else
    TrackBar4.Value := 1 * 10;

  ApplyPBRSettings(GorillaModel1);
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  /// Change metallic bias values of all PBR materials in GorillaModel1
  ApplyPBRSettings(GorillaModel1);
end;

procedure TForm1.TrackBar2Change(Sender: TObject);
begin
  /// Change roughness bias values of all PBR materials in GorillaModel1
  ApplyPBRSettings(GorillaModel1);
end;

procedure TForm1.TrackBar3Change(Sender: TObject);
begin
  /// Change ambient occlusion bias values of all PBR materials in GorillaModel1
  ApplyPBRSettings(GorillaModel1);
end;

procedure TForm1.TrackBar4Change(Sender: TObject);
begin
  /// Change normal intensity of all PBR materials in GorillaModel1
  ApplyPBRSettings(GorillaModel1);
end;

procedure TForm1.UseLightChange(Sender: TObject);
begin
  ApplyPBRSettings(GorillaModel1);
end;

procedure TForm1.ApplyEnvironmentMap(AComp : TComponent);
var I : Integer;
    LComp : TComponent;
begin
  /// Apply the environment map to all embedded materials inside of our pbr model
  if not Assigned(AComp) then
    Exit;

  if not Assigned(FEnvironment) then
    Exit;

  for I := 0 to AComp.ComponentCount - 1 do
  begin
    LComp := AComp.Components[I];
    if LComp is TGorillaPBRMaterialSource then
    begin
      /// Apply our environment map
      TGorillaPBRMaterialSource(LComp).EnvironmentPass := FEnvironment;
      TGorillaPBRMaterialSource(LComp).Environment := true;
      TGorillaPBRMaterialSource(LComp).ReflectionPower := 1;

      /// The demo model does not have an AO texture, so we setup one for better
      /// results
      TGorillaPBRMaterialSource(LComp).UsePBRAOTexture := true;
      TGorillaPBRMaterialSource(LComp).AmbientOcclusionTexture.LoadFromFile(FPath + 'marble_bust_01_ao.png');

      /// By default POM is deactivated, but we prepare the texture for turning on
      TGorillaPBRMaterialSource(LComp).ParallaxOcclusionMap.LoadFromFile(FPath + 'marble_bust_01_displ.png');
    end;

    /// also for sub meshes
    ApplyEnvironmentMap(LComp);
  end;
end;

function TForm1.ReadFirstPBRSettings(AComp : TComponent) : Boolean;
var I     : Integer;
    LComp : TComponent;
    LEvt  : TNotifyEvent;
begin
  Result := false;

  if not Assigned(AComp) then
    Exit;

  for I := 0 to AComp.ComponentCount - 1 do
  begin
    LComp := AComp.Components[I];
    if LComp is TGorillaPBRMaterialSource then
    begin
      /// NORMAL MAPPING / PARALLAX OCCLUSION MAPPING INTENSITY
      if IsPOM.IsChecked then
        TrackBar4.Value := TGorillaPBRMaterialSource(LComp).NormalIntensity * 100
      else
        TrackBar4.Value := TGorillaPBRMaterialSource(LComp).NormalIntensity * 10;

      /// BIAS VALUES FOR PBR
      TrackBar1.Value := TGorillaPBRMaterialSource(LComp).MetallicBias * 1000;
      TrackBar2.Value := TGorillaPBRMaterialSource(LComp).RoughnessBias * 1000;
      TrackBar3.Value := TGorillaPBRMaterialSource(LComp).AOBias * 1000;

      /// ALBEDO TEXTURE
      LEvt := CheckBox1.OnChange;
      try
        CheckBox1.OnChange  := nil;
        CheckBox1.IsChecked := true; // TGorillaPBRMaterialSource(LComp).UsePBRAlbedoTexture;
      finally
        CheckBox1.OnChange := LEvt;
      end;

      if TGorillaPBRMaterialSource(LComp).UsePBRAlbedoTexture then
        AlbedoTex.Bitmap.Assign(TGorillaPBRMaterialSource(LComp).Texture)
      else
        AlbedoTex.Bitmap.Clear(TAlphaColorRec.Black);

      /// METALNESS TEXTURE
      LEvt := CheckBox2.OnChange;
      try
        CheckBox2.OnChange  := nil;
        CheckBox2.IsChecked := true; // TGorillaPBRMaterialSource(LComp).UsePBRMetallicTexture;
      finally
        CheckBox2.OnChange := LEvt;
      end;

      if TGorillaPBRMaterialSource(LComp).UsePBRMetallicTexture then
        MetalnessTex.Bitmap.Assign(TGorillaPBRMaterialSource(LComp).MetalnessTexture)
      else
        MetalnessTex.Bitmap.Clear(TAlphaColorRec.Black);

      /// ROUGHNESS TEXTURE
      LEvt := CheckBox2.OnChange;
      try
        CheckBox3.OnChange  := nil;
        CheckBox3.IsChecked := true; // TGorillaPBRMaterialSource(LComp).UsePBRRoughnessTexture;
      finally
        CheckBox3.OnChange := LEvt;
      end;

      if TGorillaPBRMaterialSource(LComp).UsePBRRoughnessTexture then
        RoughnessTex.Bitmap.Assign(TGorillaPBRMaterialSource(LComp).RoughnessTexture)
      else
        RoughnessTex.Bitmap.Clear(TAlphaColorRec.Black);

      /// AMBIENT OCCLUSION TEXTURE
      LEvt := CheckBox2.OnChange;
      try
        CheckBox4.OnChange  := nil;
        CheckBox4.IsChecked := true; // TGorillaPBRMaterialSource(LComp).UsePBRAOTexture;
      finally
        CheckBox4.OnChange := LEvt;
      end;

      if TGorillaPBRMaterialSource(LComp).UsePBRAOTexture then
        AOTex.Bitmap.Assign(TGorillaPBRMaterialSource(LComp).AmbientOcclusionTexture)
      else
        AOTex.Bitmap.Clear(TAlphaColorRec.Black);

      /// NORMALMAP AND HEIGHTMAP
      NormalTex.Bitmap.Assign(TGorillaPBRMaterialSource(LComp).NormalMap);
      HeightTex.Bitmap.Assign(TGorillaPBRMaterialSource(LComp).ParallaxOcclusionMap);

      /// ACTIVATE PARALLAX OCCLUSION MAPPING
      IsPOM.IsChecked := TGorillaPBRMaterialSource(LComp).UseParallaxOcclusion;

      /// GET COLORS
      ComboColorBox1.Color := TGorillaPBRMaterialSource(LComp).Diffuse;
      ComboColorBox2.Color := TGorillaPBRMaterialSource(LComp).Ambient;
      ComboColorBox3.Color := TGorillaPBRMaterialSource(LComp).Specular;

      /// inverted normals
      CheckBox8.IsChecked := TGorillaPBRMaterialSource(LComp).InverseNormalMapping;

      Result := true;
      Exit;
    end;

    if ReadFirstPBRSettings(LComp) then
      Exit;
  end;
end;

procedure TForm1.ApplyPBRSettings(AComp : TComponent);
var I : Integer;
    LComp : TComponent;
    LNormalInt,
    LRoughness,
    LAmbientOcc,
    LMetallic : Single;
begin
  /// Apply global material settings to all embedded materials inside of our
  /// pbr model
  if not Assigned(AComp) then
    Exit;

  if IsPOM.IsChecked then
    LNormalInt  := TrackBar4.Value / 100
  else
    LNormalInt  := TrackBar4.Value / 10;

  LMetallic   := TrackBar1.Value / 1000;
  LRoughness  := TrackBar2.Value / 1000;
  LAmbientOcc := TrackBar3.Value / 1000;

  for I := 0 to AComp.ComponentCount - 1 do
  begin
    LComp := AComp.Components[I];
    if LComp is TGorillaPBRMaterialSource then
    begin
      TGorillaPBRMaterialSource(LComp).NormalIntensity := LNormalInt;
      TGorillaPBRMaterialSource(LComp).MetallicBias := LMetallic;
      TGorillaPBRMaterialSource(LComp).RoughnessBias := LRoughness;
      TGorillaPBRMaterialSource(LComp).AOBias := LAmbientOcc;

      TGorillaPBRMaterialSource(LComp).UseParallaxOcclusion := IsPOM.IsChecked;
      TGorillaPBRMaterialSource(LComp).Diffuse := ComboColorBox1.Color;
      TGorillaPBRMaterialSource(LComp).Ambient := ComboColorBox2.Color;
      TGorillaPBRMaterialSource(LComp).Specular := ComboColorBox3.Color;

      /// ALBEDO TEXTURE
      TGorillaPBRMaterialSource(LComp).UsePBRAlbedoTexture := CheckBox1.IsChecked;

      if TGorillaPBRMaterialSource(LComp).UsePBRAlbedoTexture then
        AlbedoTex.Bitmap.Assign(TGorillaPBRMaterialSource(LComp).Texture)
      else
        AlbedoTex.Bitmap.Clear(TAlphaColorRec.Black);

      /// METALNESS TEXTURE
      TGorillaPBRMaterialSource(LComp).UsePBRMetallicTexture := CheckBox2.IsChecked;

      if TGorillaPBRMaterialSource(LComp).UsePBRMetallicTexture then
        MetalnessTex.Bitmap.Assign(TGorillaPBRMaterialSource(LComp).MetalnessTexture)
      else
        MetalnessTex.Bitmap.Clear(TAlphaColorRec.Black);

      /// ROUGHNESS TEXTURE
      TGorillaPBRMaterialSource(LComp).UsePBRRoughnessTexture := CheckBox3.IsChecked;

      if TGorillaPBRMaterialSource(LComp).UsePBRRoughnessTexture then
        RoughnessTex.Bitmap.Assign(TGorillaPBRMaterialSource(LComp).RoughnessTexture)
      else
        RoughnessTex.Bitmap.Clear(TAlphaColorRec.Black);

      /// AMBIENT OCCLUSION TEXTURE
      TGorillaPBRMaterialSource(LComp).UsePBRAOTexture := CheckBox4.IsChecked;

      if TGorillaPBRMaterialSource(LComp).UsePBRAOTexture then
        AOTex.Bitmap.Assign(TGorillaPBRMaterialSource(LComp).AmbientOcclusionTexture)
      else
        AOTex.Bitmap.Clear(TAlphaColorRec.Black);

      /// inverted normals
      TGorillaPBRMaterialSource(LComp).InverseNormalMapping := CheckBox8.IsChecked;
    end;

    ApplyPBRSettings(LComp);
  end;
end;

procedure TForm1.CheckBox1Change(Sender: TObject);
begin
  ApplyPBRSettings(GorillaModel1);
end;

procedure TForm1.CheckBox2Change(Sender: TObject);
begin
  ApplyPBRSettings(GorillaModel1);
end;

procedure TForm1.CheckBox3Change(Sender: TObject);
begin
  ApplyPBRSettings(GorillaModel1);
end;

procedure TForm1.CheckBox4Change(Sender: TObject);
begin
  ApplyPBRSettings(GorillaModel1);
end;

procedure TForm1.ComboColorBox1Change(Sender: TObject);
begin
  ApplyPBRSettings(GorillaModel1);
end;

procedure TForm1.ComboColorBox2Change(Sender: TObject);
begin
  ApplyPBRSettings(GorillaModel1);
end;

procedure TForm1.ComboColorBox3Change(Sender: TObject);
begin
  ApplyPBRSettings(GorillaModel1);
end;

procedure TForm1.CheckBox8Change(Sender: TObject);
begin
  ApplyPBRSettings(GorillaModel1);
end;

procedure TForm1.ReadFirstLightSettings();
begin
  CheckBox5.IsChecked := GorillaLight1.Enabled;
  ComboColorBox4.Color := GorillaLight1.Color;

  CheckBox6.IsChecked := GorillaLight2.Enabled;
  ComboColorBox5.Color := GorillaLight2.Color;

  CheckBox7.IsChecked := GorillaLight3.Enabled;
  ComboColorBox6.Color := GorillaLight3.Color;
end;

procedure TForm1.ApplyLightSettings();
begin
  if not FInitiated then
    Exit;

  GorillaLight1.Enabled := CheckBox5.IsChecked;
  GorillaLight1.Color := ComboColorBox4.Color;

  GorillaLight2.Enabled := CheckBox6.IsChecked;
  GorillaLight2.Color := ComboColorBox5.Color;

  GorillaLight3.Enabled := CheckBox7.IsChecked;
  GorillaLight3.Color := ComboColorBox6.Color;
end;

procedure TForm1.CheckBox5Change(Sender: TObject);
begin
  ApplyLightSettings();
end;

procedure TForm1.CheckBox6Change(Sender: TObject);
begin
  ApplyLightSettings();
end;

procedure TForm1.CheckBox7Change(Sender: TObject);
begin
  ApplyLightSettings();
end;

procedure TForm1.ComboColorBox4Change(Sender: TObject);
begin
  ApplyLightSettings();
end;

procedure TForm1.ComboColorBox5Change(Sender: TObject);
begin
  ApplyLightSettings();
end;

procedure TForm1.ComboColorBox6Change(Sender: TObject);
begin
  ApplyLightSettings();
end;

end.
