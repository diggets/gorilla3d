<p align="center">
  <img src="../img/logo-blue@2x.png" alt="Downloadarea">&nbsp;Gorilla3D Framework for Delphi/C++ Builder 10.1+
</p>

Developer-Edition
--------
We provide a developer edition, which you can use it to develop an application with max. of 5 developers (contains visual and encoded watermarks). But it can only be used for development purposes and it is not allowed to publish anything built with Gorilla3D. When you're ready to release, you need to buy a commercial license.

Packages
------------
We provide a package for each released IDE version.
Select your prefered IDE and download the latest package version.


Installation
------------
For installation notes please check out the documentation at: https://docs.gorilla3d.de/doku.php?id=installation

Demos
--------
All demos were build with the latest package for the latest IDE version.
So you may make changes to demo source code depending on your prefered IDE.

  - **AnimationController** - switch character animations by input or variable control
  - **Aquarium** - showing an animated fish from FBX file, multiple lights, particle effects and bloom
  - **AtlasMaterialDemo** - showing usage of atlasmaterial with shared resource
  - **Audio** - a simple demo showing usage of audio files with FMOD plugin
  - **Billboard** - multiplying grass planes and rendering those with animation
  - **Bloom** - demonstrating the bloom effect or emissive blurring and how to configure it
  - **EmbeddedModel** - the project contains an animated textured model, packed inside the .fmx file, no additional model loading necessary
  - **EnvironmentMapping** - showing how to setup and render environment mapping
  - **FirstPerson** - showing first person camera controlling (ego perspective)
  - **Fog** - configuring settings to see how fog will be rendered in viewport
  - **GUI2D** - displaying 2D FMX components on Gorilla3D viewport, f.e. for GUID displays
  - **GlobalIllumination** - a pre-constructed scene to show global illumination rendering with shadowing, reflections and light-scattering
  - **Grass** - showing how to use mesh-instancing in combination with a splatmap for rendering grass
  - **InputController** - a input controller feedback window, just recording inputs and showing them in a memo
  - **Layer3D** - a Layer3D with an image is rendered, showing 2D FMX components in 3D space
  - **Materials** - all currently available materials can be applied to different primitives to see their rendering
  - **ModelView** - a simple model loader for all currently available 3D file formats
  - **MultiCharacter** - an animated model will be loaded and cloned multiple times but with reusing logical information (TModelDef)
  - **MultiLight** - showing how multiple lights moving and merging in colors to proof correct rendering
  - **PBR** - demonstrating physically based rendering (PBR) material with a lot of configurations
  - **Particles** - a simple smoke particle effect rendered
  - **Physics** - a physics scene with box, plane and sphere showing collision detection, the sphere can be moved by cursor keys
  - **PhysicsCharacterController** - showing third person animated character controlled by input and automatic animation switching
  - **PhysicsParticles** - showing particles colliding with other bodies / terrain by Q3 physics engine
  - **PostEffects** - showing FMX post effects working on Gorilla3D viewport
  - **Prefabs** - demonstrating the PrefabManager component to load scene data from prefab files
  - **ReleaseParty** - a nice demo showing animated models, reflections and the dialogue system in action
  - **Script** - a simple script editor to test GorillaScript
  - **Shadow** - providing demos for testing ShadowMapping and VarianceShadowMapping render pass components
  - **SketchfabClient** - a client tool to request models from Sketchfab. Credentials needed
  - **SplatMap** - this demo is showing splat map texturing on a plane with dynamic changes of the splatmap
  - **Terrain** - the demo is loading a terrain from heightmap, smoothing algorithm is configurable and terrain materials can be tested
  - **TextLayers** - a demo showing 3D arranged textlayers to show transparency rendering with WBOIT
  - **ThirdPerson** - showing third person controlling by mouse and keyboard
  - **Transparency** - a demo showing 4 crates and 8 transparent boxes around
  - **TriggerPoints** - demonstrating the usage of dynamic trigger points attached to a moving object
  - **VolumeRendering** - demonstrating usage of TGorillaVolumetricMesh component for volumetric rendering, f.e. medical data or MRT/CT scans
  - **Water** - a simple water rendering demo showing the water surface setup
  - **Weapon** - a simple weapon inventory demo, showing collecting weapons, toggeling those and upgrading weapons.

Tools
------------

  - **AssetsManager** - the assetsmanager allowing to manage packages with assets, that can be used in your application
  - **DialogueDesigner** - load/save/manage dialogues between characters
  - **InventoryDesigner** - load/save/manage your inventory system with items, groups and manufacturers, sandbox mode for testing included
  - **ShaderDesigner** - a simple shader designer based on the TGorillaRuntimeMaterialSource component, that can be loaded in Gorilla3D

Copyright
---------
Terms & Conditions
Both the Licensee and Software provider agree to adhere to the following terms and conditions.

The Software Provider has granted, with acceptance of this source code license agreement, Eric Berger (through the granted permission likewise the Manaz GmbH) total licenses to be used as permitted. These licenses shall be considered perpetual and non-transferrable.

The licenses granted may be used by the Licensee as well as any employees and subcontractors providing services for the Licensee.

Throughout this agreement, “Software” shall be defined as all source codes, object codes, link libraries, utility programs, project files, scripts related to the software listed above.

The Software shall remain the intellectual property of the Software Provider Eric Berger at all times.

License Grant
In consideration of all terms and conditions contained within this contract the Licensee shall have the nonexclusive and nontransferable rights as stated below.

The Licensee will hold the right to incorporate the dynamic and statically linked libraries that the Software Provider has developed.

Restrictions
Unless prior written consent has been obtained from the Software Owner, the following shall not be allowed.

The Licensee is not allowed to make and distribute an unlimited number of copies of the libraries mentioned above.

The distribution of any source codes, header files, make files, or libraries of the software.
The software should not be merged, compiled, or copied unless expressed in this agreement.
Alteration or removal of any notices in or on the software or within the documentation included within the software.
Any distribution of the software by the Licensee that is not conducted with approval from the Software Provider.
Disclosure of any source codes pertaining to the software to any additional parties not included in this agreement.
The Licensee shall not disassemble, decompile or reverse engineer any object code contained within the software.
The number of licenses being used may at no point and time be more than the number of licenses purchased through this agreement.

Warranties
All software included in this source code license agreement as well as all documentation included with said software is provided in an “as is” condition.

The Licensee shall be granted a 72 hour period after initial access is granted. During this period, the Licensee may report any errors, missing features, or other issues related to the source code.

This initial inspection shall be performed with the source code in its original state. Any modification or other alteration of the source code shall constitute acceptance of the Software by the Licensee.

The Software Provider agrees to correct any such errors in a timely manner, after which the Licensee shall accept the source code for use pursuant to the terms of this agreement.

The Software Provider makes no guarantee as to the source code’s performance once it has been accepted by the Licensee.

Liability
Under no circumstances will either party or their representatives be liable to each other for any incidental, consequential, or indirect damages including but not limited to lost or damaged data, revenue loss, economic loss, or commercial loss arising out of a breach of any terms and conditions set forth in this source code license agreement.

This limitation of liability shall apply regardless of if the alleged breach is a fundamental breach or fundamental term.

Both parties are in understanding that some jurisdictions do not allow the exclusion of liability for consequential damages and as so the above limitation may not apply.

Term & Termination
While the term of this agreement may be perpetual, this agreement may terminate immediately in the event any breach occurs of the terms and conditions listed herein.

The defaulting party will be allotted 30 days to cure the breach upon written notice or face immediate agreement termination.

Under no condition should any disclosures of the licensed source code or theft of the Clients computers be grounds for termination of this agreement.

In the event of termination the Licensee must destroy any and all copies of the Software as well as the included documentation.

Indemnity
The Licensee agrees to hold the Software Provider harmless as well as defend the Software Provider from any and all damages, costs, and liabilities that take place from a lawsuit due to any of the following

Product Distribution
Breach of Contract
Software installation
Additionally, the Software Provider shall indemnify and hold the Licensee harmless against any damages related to the source code.

Copyright Notice
The Licensee agrees to display an appropriate copyright notice in any final versions of Software containing the source code distributed to third parties.

Applicable law
Any disputes related to this agreement shall be resolved in accordance with the laws of Manaz GmbH, Bielefeld, NRW, Germany and any and all legal proceedings shall take place as such.
