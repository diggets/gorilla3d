unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Types3D,
  System.Math.Vectors, FMX.Objects3D, FMX.Controls3D, Gorilla.Camera,
  Gorilla.Light, Gorilla.Viewport, Gorilla.Control, Gorilla.Mesh, Gorilla.Plane,
  FMX.MaterialSources, Gorilla.Material.Default, Gorilla.Material.Blinn,
  Gorilla.Torus, Gorilla.Cube, Gorilla.Model;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaLight1: TGorillaLight;
    GorillaLight2: TGorillaLight;
    GorillaLight3: TGorillaLight;
    GorillaCamera1: TGorillaCamera;
    Dummy1: TDummy;
    GorillaPlane1: TGorillaPlane;
    GorillaBlinnMaterialSource1: TGorillaBlinnMaterialSource;
    Timer1: TTimer;
    GorillaModel1: TGorillaModel;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    FLightMove : Single;
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.Math, Gorilla.OBJ.Loader;

procedure TForm1.FormCreate(Sender: TObject);
begin
  FLightMove := 0;

  GorillaModel1.LoadFromFile(nil, '.\assets\monkey.obj', []);
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  FLightMove := FLightMove + 0.05;
  GorillaViewport1.BeginUpdate();
  try
    GorillaLight1.AbsolutePosition := Vector3D(-5 + Sin(FLightMove) * 2.5, -10, Cos(FLightMove) * 5);
    GorillaLight2.AbsolutePosition := Vector3D( 5 - Cos(FLightMove) * 2.5, -10, Sin(FLightMove) * 5);
    GorillaLight3.AbsolutePosition := Vector3D(0, -10, Sin(FLightMove) * 5);

    GorillaModel1.RotationAngle.Y := GorillaModel1.RotationAngle.Y + 1;
  finally
    GorillaViewport1.EndUpdate();
  end;
end;

end.
