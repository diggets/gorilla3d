program unittest_cond;

uses
	System.SysUtils;
	
	procedure Main();
	var LInt  : Integer;
		LBool : Boolean;
	begin
		System.WriteLn('--- UNITTEST - CONDITIONS -------------------------------------');
		///
		/// if conditions
		///
		System.WriteLn('"if (LInt = 1423) then" should return : 123');
		LInt := 123;
		if (LInt = 123) then 
			System.WriteLn('123')
		else
			System.WriteLn('FALSE');
		
		System.WriteLn('"if (-(8 / 4 + 2) = -4) then" should return : -4');
		if (-(8 / 4 + 2) = -4) then
			System.WriteLn('-4')
		else
			System.WriteLn('FALSE');
		
		System.WriteLn('short "if (LInt <> 42) then" should return : TRUE');
		LInt := 42;
		if (LInt <> 42) then
		else
			System.WriteLn('TRUE');
		
		System.WriteLn('short "if (LInt = 123) then" should return : <nothing>');
		LInt := 123;
		if (LInt <> 123) then
			System.WriteLn('TRUE');
		
		System.WriteLn('empty if-condition should return : <nothing>');
		LInt := 123;
		if (LInt = 123) then
			;
			
		System.WriteLn('complex if-condition should return 123.');
		LInt := 123;
		if (LInt > 100) then
		begin
			if (LInt > 50) then
			begin
				if (LInt > 25) then
					if (LInt > 10) then
					begin
					  System.WriteLn('123');
					end
					else System.WriteLn('FALSE (4)')
				else
					System.WriteLn('FALSE (3)');
			end
			else System.WriteLn('FALSE (2)');
		end
		else System.WriteLn('FALSE (1)');
		
		System.WriteLn('multiple statement if-condition should return 100.');
		LInt := 5;
		if (LInt < 200) then
		begin
			LInt := LInt * 5;
			LInt := LInt * 4;
			LInt := LInt + 1000;
			LInt := LInt - (200 * 5);
		end;
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('multiple conditions statement should return 201.');
		LInt := 201;
		if (LInt > 50) and (LInt < 200) or (LInt = 201) then
			System.WriteLn(LInt.ToString())
		else 
			System.WriteLn('FALSE');
			
		System.WriteLn('complex multiple conditions statement should return 201.');
		LInt := 201;
		if ((-(8 / 4 + 2) = -4) and (LInt <= 250)) and (LInt = 201) then
			System.WriteLn(LInt.ToString())
		else 
			System.WriteLn('FALSE');
		  
		///
		/// boolean expressions
		///
		
		System.WriteLn('EQ: boolean expression statement (LInt = 456) should return : TRUE');
		LInt  := 456;
		LBool := (LInt = 456);
		System.WriteLn(LBool.ToString());
		
		System.WriteLn('LT: boolean expression statement (LInt < 457) should return : TRUE');
		LInt  := 456;
		LBool := (LInt < 457);
		System.WriteLn(LBool.ToString());
		
		System.WriteLn('LEQ: boolean expression statement (LInt <= 456) should return : TRUE');
		LInt  := 456;
		LBool := (LInt <= 456);
		System.WriteLn(LBool.ToString());
		
		System.WriteLn('GT: boolean expression statement (LInt > 455) should return : TRUE');
		LInt  := 456;
		LBool := (LInt > 455);
		System.WriteLn(LBool.ToString());
		
		System.WriteLn('GEQ: boolean expression statement (LInt >= 456) should return : TRUE');
		LInt  := 456;
		LBool := (LInt >= 456);
		System.WriteLn(LBool.ToString());
		
		
		
		System.WriteLn('AND: boolean expression statement (true AND true) should return : TRUE');
		LBool := (true AND true);
		System.WriteLn(LBool.ToString());
		
		System.WriteLn('AND: boolean expression statement (false AND true) should return : FALSE');
		LBool := (false AND true);
		System.WriteLn(LBool.ToString());
		
		System.WriteLn('AND: boolean expression statement (true AND false) should return : FALSE');
		LBool := (true AND false);
		System.WriteLn(LBool.ToString());
		
		System.WriteLn('AND: boolean expression statement (false AND false) should return : FALSE');
		LBool := (false AND false);
		System.WriteLn(LBool.ToString());
		
		
		
		System.WriteLn('OR: boolean expression statement (true OR true) should return : TRUE');
		LBool := (true OR true);
		System.WriteLn(LBool.ToString());
		
		System.WriteLn('OR: boolean expression statement (false OR true) should return : TRUE');
		LBool := (false OR true);
		System.WriteLn(LBool.ToString());
		
		System.WriteLn('OR: boolean expression statement (true OR false) should return : TRUE');
		LBool := (true OR false);
		System.WriteLn(LBool.ToString());
		
		System.WriteLn('OR: boolean expression statement (false OR false) should return : FALSE');
		LBool := (false OR false);
		System.WriteLn(LBool.ToString());
		
		
		System.WriteLn('XOR: boolean expression statement (true XOR true) should return : FALSE');
		LBool := (true XOR true);
		System.WriteLn(LBool.ToString());
		
		System.WriteLn('XOR: boolean expression statement (false XOR true) should return : TRUE');
		LBool := (false XOR true);
		System.WriteLn(LBool.ToString());
		
		System.WriteLn('XOR: boolean expression statement (true XOR false) should return : TRUE');
		LBool := (true XOR false);
		System.WriteLn(LBool.ToString());
		
		System.WriteLn('XOR: boolean expression statement (false XOR false) should return : FALSE');
		LBool := (false XOR false);
		System.WriteLn(LBool.ToString());
		
		
		System.WriteLn('complex boolean expression statement should return : TRUE');
		LInt  := 201;
		LBool := ( ((-(8 / 4 + 2) = -4) and (LInt <= 250)) and (LInt = 201) );
		System.WriteLn(LBool.ToString());
		
		System.WriteLn('---------------------------------------------------------------');
	end;
	
begin
	Main();  
end.