unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, Gorilla.Terrain, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Plane, FMX.Controls3D, Gorilla.Light, Gorilla.Camera,
  FMX.Objects3D, Gorilla.Viewport, FMX.MaterialSources,
  Gorilla.Material.Default, Gorilla.Material.Terrain,
  Gorilla.Controller.Passes.Reflection, Gorilla.Controller,
  Gorilla.Controller.Passes.Refraction, Gorilla.Material.Water,
  Gorilla.Material.Lambert, Gorilla.Cube, FMX.Ani, Gorilla.SkyBox,
  Gorilla.Transform, Gorilla.Controller.Passes.Environment, FMX.ListBox;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaLight1: TGorillaLight;
    GorillaPlane1: TGorillaPlane;
    GorillaTerrain1: TGorillaTerrain;
    GorillaTerrainMaterialSource1: TGorillaTerrainMaterialSource;
    GorillaWaterMaterialSource1: TGorillaWaterMaterialSource;
    GorillaCube1: TGorillaCube;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    FloatAnimation2: TFloatAnimation;
    GorillaRenderPassRefraction1: TGorillaRenderPassRefraction;
    GorillaRenderPassReflection1: TGorillaRenderPassReflection;
    GorillaSkyBox1: TGorillaSkyBox;
    ComboBox1: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FloatAnimation2Process(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
  private
    FRippleAdded : Boolean;
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.ComboBox1Change(Sender: TObject);
begin
  GorillaWaterMaterialSource1.OutputLayer := TWaterOutputLayer(ComboBox1.ItemIndex);
end;

procedure TForm1.FloatAnimation2Process(Sender: TObject);
begin
  if GorillaCube1.AbsolutePosition.Y > -2.25 then
  begin
    if not FRippleAdded then
    begin
      GorillaWaterMaterialSource1.AddRipple(TPoint3D.Zero);
      FRippleAdded := true;
    end;
  end
  else
  begin
    // Reset for next ripple
    FRippleAdded := false;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  // For fast rendering and without continously hit-testing
  GorillaPlane1.SetHitTestValue(false);
  GorillaTerrain1.SetHitTestValue(false);
  GorillaCube1.SetHitTestValue(false);

  GorillaRenderPassRefraction1.SurfacePosition.Point :=
    TPoint3D(GorillaPlane1.AbsolutePosition) + Point3D(0, -1, 0) +
    Point3D(0, -GorillaWaterMaterialSource1.DisplIntensity, 0);

  GorillaRenderPassReflection1.MirrorPosition.Point :=
    TPoint3D(GorillaPlane1.AbsolutePosition) + Point3D(0, -1, 0) +
    Point3D(0, -GorillaWaterMaterialSource1.DisplIntensity, 0);
end;

end.
