unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Gorilla.Viewport, System.Math.Vectors, FMX.Types3D, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Cube, FMX.Controls3D, FMX.Objects3D, Gorilla.Controller,
  Gorilla.Controller.Input, Gorilla.Controller.Input.Consts,
  Gorilla.Controller.Input.Types, FMX.Controls.Presentation, FMX.StdCtrls,
  Gorilla.Transform, FMX.MaterialSources, Gorilla.Material.Default,
  Gorilla.Material.Blinn;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    Dummy1: TDummy;
    Camera1: TCamera;
    Light1: TLight;
    GorillaCube1: TGorillaCube;
    GorillaInputController1: TGorillaInputController;
    Timer1: TTimer;
    Label1: TLabel;
    Label2: TLabel;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    GorillaBlinnMaterialSource1: TGorillaBlinnMaterialSource;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
  private
    procedure DoOnHotKeyA(const AItem : TGorillaHotKeyItem; const ACurrentInput : TGorillaHotKeyRaw;
      const AMode : TGorillaInputMode);
    procedure DoOnHotKeyB(const AItem : TGorillaHotKeyItem; const ACurrentInput : TGorillaHotKeyRaw;
      const AMode : TGorillaInputMode);
    procedure DoOnHotKeyX(const AItem : TGorillaHotKeyItem; const ACurrentInput : TGorillaHotKeyRaw;
      const AMode : TGorillaInputMode);
    procedure DoOnHotKeyY(const AItem : TGorillaHotKeyItem; const ACurrentInput : TGorillaHotKeyRaw;
      const AMode : TGorillaInputMode);

    procedure DoOnSequence(const ASequence : TGorillaInputSequenceItem);

    procedure DoSetupKeyboardHotKeys();
    procedure DoSetupGamePadHotKeys();
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.FormCreate(Sender: TObject);
begin
  DoSetupKeyboardHotKeys();
  DoSetupGamePadHotKeys();

  // enabled input controller
  GorillaInputController1.Enabled := true;

  // enable time
  Timer1.Enabled := true;
end;

procedure TForm1.DoSetupKeyboardHotKeys();
var A, B, X, Y : TGorillaHotKeyItem;
    LSequence : TGorillaInputSequenceItem;
begin
  A := GorillaInputController1.AddHotKey('KEY_A');
  A.AddInput(TGorillaInputDeviceType.Keyboard, GORILLA_INPUT_KEY_A);
  A.OnTriggered := DoOnHotKeyA;

  B := GorillaInputController1.AddHotKey('KEY_D');
  B.AddInput(TGorillaInputDeviceType.Keyboard, GORILLA_INPUT_KEY_D);
  B.OnTriggered := DoOnHotKeyB;

  X := GorillaInputController1.AddHotKey('KEY_W');
  X.AddInput(TGorillaInputDeviceType.Keyboard, GORILLA_INPUT_KEY_W);
  X.OnTriggered := DoOnHotKeyX;

  Y := GorillaInputController1.AddHotKey('KEY_S');
  Y.AddInput(TGorillaInputDeviceType.Keyboard, GORILLA_INPUT_KEY_S);
  Y.OnTriggered := DoOnHotKeyY;

  LSequence := GorillaInputController1.AddSequence('SEQUENCE_KEYS', [A, B, X, Y]);
  LSequence.Sensitivity := 500;
  LSequence.OnTriggered := DoOnSequence;
end;

procedure TForm1.DoSetupGamePadHotKeys();
var A, B, X, Y : TGorillaHotKeyItem;
    LSequence : TGorillaInputSequenceItem;
begin
  A := GorillaInputController1.AddHotKey('GAMEPAD_A');
  A.AddInput(TGorillaInputDeviceType.GamePad, GORILLA_INPUT_GAMEPAD_BUTTON_A);
  A.OnTriggered := DoOnHotKeyA;
  A.LockTime := 250;

  B := GorillaInputController1.AddHotKey('GAMEPAD_B');
  B.AddInput(TGorillaInputDeviceType.GamePad, GORILLA_INPUT_GAMEPAD_BUTTON_B);
  B.OnTriggered := DoOnHotKeyB;
  B.LockTime := 250;

  X := GorillaInputController1.AddHotKey('GAMEPAD_X');
  X.AddInput(TGorillaInputDeviceType.GamePad, GORILLA_INPUT_GAMEPAD_BUTTON_X);
  X.OnTriggered := DoOnHotKeyX;
  X.LockTime := 250;

  Y := GorillaInputController1.AddHotKey('GAMEPAD_Y');
  Y.AddInput(TGorillaInputDeviceType.GamePad, GORILLA_INPUT_GAMEPAD_BUTTON_Y);
  Y.OnTriggered := DoOnHotKeyY;
  Y.LockTime := 250;

  LSequence := GorillaInputController1.AddSequence('SEQUENCE_GAMEPAD', [A, B, X, Y]);
  LSequence.Sensitivity := 500;
  LSequence.OnTriggered := DoOnSequence;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var LMsg : TGorillaInputMessage;
    LX, LY : Single;
begin
  // get the left thumbstick position to rotate cube
  if GorillaInputController1.GetPersistentMessage(TGorillaInputDeviceType.GamePad,
    GORILLA_INPUT_GAMEPAD_THUMBSTICK_POS_LEFT, LMsg) then
  begin
    LX := High(Word) / Hi(LMsg.WParam);
    LY := High(Word) / Lo(LMsg.WParam);

    GorillaCube1.BeginUpdate();
    try
      GorillaCube1.RotationAngle.Point := GorillaCube1.RotationAngle.Point + Point3D(LX, 0, LY);
    finally
      GorillaCube1.EndUpdate();
    end;
  end;
end;

procedure TForm1.CheckBox1Change(Sender: TObject);
var LDevs : TGorillaInputDeviceTypes;
begin
  LDevs := [];

  if CheckBox1.IsChecked then
    LDevs := LDevs + [TGorillaInputDeviceType.Keyboard];

  if CheckBox2.IsChecked then
    LDevs := LDevs + [TGorillaInputDeviceType.GamePad];

  GorillaInputController1.Supported := LDevs;
end;

procedure TForm1.DoOnHotKeyA(const AItem : TGorillaHotKeyItem; const ACurrentInput : TGorillaHotKeyRaw;
  const AMode : TGorillaInputMode);
begin
  GorillaViewport1.BeginUpdate();
  try
    Label2.Text := AItem.DisplayName;
    GorillaCube1.RotationAngle.Y := GorillaCube1.RotationAngle.Y - 1;
  finally
    GorillaViewport1.EndUpdate();
  end;
end;

procedure TForm1.DoOnHotKeyB(const AItem : TGorillaHotKeyItem; const ACurrentInput : TGorillaHotKeyRaw;
  const AMode : TGorillaInputMode);
begin
  GorillaViewport1.BeginUpdate();
  try
    Label2.Text := AItem.DisplayName;
    GorillaCube1.RotationAngle.Y := GorillaCube1.RotationAngle.Y + 1;
  finally
    GorillaViewport1.EndUpdate();
  end;
end;

procedure TForm1.DoOnHotKeyX(const AItem : TGorillaHotKeyItem; const ACurrentInput : TGorillaHotKeyRaw;
  const AMode : TGorillaInputMode);
begin
  GorillaViewport1.BeginUpdate();
  try
    Label2.Text := AItem.DisplayName;
    GorillaCube1.RotationAngle.X := GorillaCube1.RotationAngle.X - 1;
  finally
    GorillaViewport1.EndUpdate();
  end;
end;

procedure TForm1.DoOnHotKeyY(const AItem : TGorillaHotKeyItem; const ACurrentInput : TGorillaHotKeyRaw;
  const AMode : TGorillaInputMode);
begin
  GorillaViewport1.BeginUpdate();
  try
    Label2.Text := AItem.DisplayName;
    GorillaCube1.RotationAngle.X := GorillaCube1.RotationAngle.X + 1;
  finally
    GorillaViewport1.EndUpdate();
  end;
end;

procedure TForm1.DoOnSequence(const ASequence : TGorillaInputSequenceItem);
begin
  GorillaCube1.AnimateFloat('Scale.X', 2, 0.5, TAnimationType.InOut);
  GorillaCube1.AnimateFloat('Scale.Y', 2, 0.5, TAnimationType.InOut);
  GorillaCube1.AnimateFloat('Scale.Z', 2, 0.5, TAnimationType.InOut);
end;

end.
