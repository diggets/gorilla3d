unit SelectWindow;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls, FMX.Edit, FMX.Layouts, FMX.ListBox, FMX.Controls.Presentation;

type
  TCallback = procedure (ASelected: String) of object;

  TfmSelect = class(TForm)
    Panel1: TPanel;
    btnRefresh: TButton;
    lstItems: TListBox;
    edtCurrentFolder: TEdit;
    pnlDirectoryNotExist: TPanel;
    lblDirectoryNotExist: TLabel;
    btnSelect: TButton;
    SearchEditButton1: TSearchEditButton;
    procedure btnRefreshClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lstItemsClick(Sender: TObject);
    procedure btnSelectClick(Sender: TObject);
    procedure SearchEditButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    const
      CONST_STRING_PARENT = '..';

    var
      Callback: TCallback;

    { Public declarations }
    function CD(AFolder: String): Boolean;
  end;

var
  fmSelect: TfmSelect;

  procedure SelectFile(ACallback : TCallback);

implementation

{$R *.fmx}

uses
  System.IOUtils;

procedure SelectFile(ACallback : TCallback);
begin
  fmSelect.Callback := ACallback;
  fmSelect.CD(TPath.GetDocumentsPath);
  fmSelect.ShowModal(
    procedure(AResult : TModalResult)
    var LRes : String;
    begin
      if Assigned(fmSelect.Callback) then
      begin
        if fmSelect.lstItems.ItemIndex = -1 then
          LRes := EmptyStr
        else
          LRes := fmSelect.lstItems.Items[fmSelect.lstItems.ItemIndex];

        fmSelect.Callback(LRes);
      end;
    end);
end;
  
{ fmSelect }

procedure TfmSelect.btnSelectClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

function TfmSelect.CD(AFolder: String): Boolean;
var
  LParent: String;
  LDirs,
  LFiles: TStringDynArray;
  s: String;
begin
  lstItems.Clear;
  pnlDirectoryNotExist.Visible := False;
  if (AFolder <> EmptyStr) then
    AFolder := IncludeTrailingPathDelimiter(AFolder);
  edtCurrentFolder.Text := AFolder;

  { http://stackoverflow.com/questions/20318875/how-to-show-the-availble-files-in-android-memory-with-firemonkey }
  if not TDirectory.Exists(AFolder, True) then
    begin
      lblDirectoryNotExist.Text := 'Directory ' + AFolder + ' does not exist.';
      pnlDirectoryNotExist.Visible := True;
      Exit(False);
    end;

  { }
  LParent := TDirectory.GetParent(AFolder);

  { }
  if LParent <> AFolder then
    lstItems.Items.Add(CONST_STRING_PARENT);

  { }
  LDirs := TDirectory.GetDirectories(AFolder, '*');

  // Get all files. Non-Windows systems don't typically care about
  // extensions, so we just use a single '*' as a mask.
  LFiles := TDirectory.GetFiles(AFolder, '*');

  for s in LDirs do
    lstItems.Items.Add(IncludeTrailingPathDelimiter(s));

  for s in LFiles do
    lstItems.Items.Add(s);

  Result := True;
end;

procedure TfmSelect.FormCreate(Sender: TObject);
begin
  pnlDirectoryNotExist.Visible := False;
end;

procedure TfmSelect.lstItemsClick(Sender: TObject);
var
  s: String;
begin
  if lstItems.ItemIndex = -1 then
    Exit;

  if SameText(lstItems.Items[lstItems.ItemIndex], CONST_STRING_PARENT) then
  begin
    s := TDirectory.GetParent(edtCurrentFolder.Text); // only removes delim at the end
    s := TDirectory.GetParent(s);
    CD(s);
  end
  else
    begin
      s := lstItems.Items[lstItems.ItemIndex];

      if s = EmptyStr then
        Exit;

      if s.EndsWith(PathDelim) then
        CD(s);
    end;
end;

procedure TfmSelect.SearchEditButton1Click(Sender: TObject);
begin
  CD(edtCurrentFolder.Text);
end;

procedure TfmSelect.btnRefreshClick(Sender: TObject);
begin
  if edtCurrentFolder.Text <> EmptyStr then
    CD(edtCurrentFolder.Text)
  else
    CD(TPath.GetDocumentsPath);
end;

end.

