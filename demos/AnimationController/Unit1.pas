unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light,
  Gorilla.Camera, Gorilla.Viewport, Gorilla.Controller, Gorilla.DefTypes,
  Gorilla.Animation.Controller, Gorilla.Control, Gorilla.Mesh, Gorilla.Model,
  Gorilla.AssetsManager, FMX.Controls.Presentation, FMX.StdCtrls,
  Gorilla.Controller.Input, FMX.MaterialSources, Gorilla.Material.Default,
  Gorilla.Material.Lambert, Gorilla.Transform;

const
  MODEL_ANIM_NONE  = 'c.dae';
  MODEL_ANIM_IDLE  = 'c-idle.dae';
  MODEL_ANIM_HIT1  = 'c-hit1.dae';
  MODEL_ANIM_HIT2  = 'c-hit2.dae';

type
  TForm2 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaModel1: TGorillaModel;
    GorillaAnimationController1: TGorillaAnimationController;
    GorillaAssetsManager1: TGorillaAssetsManager;
    Button2: TButton;
    Button3: TButton;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    procedure FormCreate(Sender: TObject);
    procedure GorillaViewport1MouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; var Handled: Boolean);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    FLayer1 : TGorillaAnimationTransitionLayer;
  public
    procedure AddCustomTransitions();
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

uses
  System.IOUtils,
  System.Rtti,
  Gorilla.DAE.Loader,
  Gorilla.Animation;

procedure TForm2.Button2Click(Sender: TObject);
begin
  GorillaAnimationController1.DispatchInput('anim', 1);
end;

procedure TForm2.Button3Click(Sender: TObject);
begin
  GorillaAnimationController1.DispatchInput('anim', 2);
end;

procedure TForm2.FormCreate(Sender: TObject);
var LPath : String;
    LPckg : TGorillaAssetsPackage;
    LOpts : TGorillaLoadOptions;
    LAnim : TGorillaAnimation;
begin
{$IFDEF MSWINDOWS}
  LPath := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))) +
    IncludeTrailingPathDelimiter('assets');
{$ENDIF}
{$IFDEF ANDROID}
  LPath := IncludeTrailingPathDelimiter(TPath.GetHomePath()) +
    IncludeTrailingPathDelimiter('assets');
{$ENDIF}

  // create a temporary assets package
  LPckg := GorillaAssetsManager1.AddEmptyPackage('TempPackage');

  // load model
  LOpts := TGorillaLoadOptions.Create(LPath + MODEL_ANIM_NONE, LPckg);
  GorillaModel1.LoadFromFile(LOpts);

  // load additional animations
  LAnim := GorillaModel1.AddAnimationFromFile(LPath + MODEL_ANIM_IDLE);
  GorillaModel1.AddAnimationFromFile(LPath + MODEL_ANIM_HIT1);
  GorillaModel1.AddAnimationFromFile(LPath + MODEL_ANIM_HIT2);

  // modify a lambert material to apply to the character, which has no material
  GorillaLambertMaterialSource1.DiffuseF := TAlphaColorF.Create(0.75, 1, 0.75, 1.0);
  GorillaLambertMaterialSource1.UseTexture0 := false;
  GorillaLambertMaterialSource1.UseTexturing := false;
  GorillaModel1.Meshes[0].MaterialSource := GorillaLambertMaterialSource1;

  // add all transitions
  AddCustomTransitions();
end;

procedure TForm2.AddCustomTransitions();
begin
  GorillaAnimationController1.DefaultAnimation := MODEL_ANIM_IDLE;
  FLayer1 := GorillaAnimationController1.AddLayer('Layer1');

  // add a single animation playback for hits
  FLayer1.AddTransition('idle > hit1', MODEL_ANIM_IDLE, MODEL_ANIM_HIT1, false,
    'anim', 1, TGorillaAnimationTransitionMode.tmImmediatly);
  FLayer1.AddTransition('hit1 > idle', MODEL_ANIM_HIT1, MODEL_ANIM_IDLE, true,
    'anim', 1, TGorillaAnimationTransitionMode.tmImmediatly);

  // add a single animation playback for death sequence
  FLayer1.AddTransition('idle > hit2', MODEL_ANIM_IDLE, MODEL_ANIM_HIT2, false,
    'anim', 2, TGorillaAnimationTransitionMode.tmImmediatly);
  FLayer1.AddTransition('hit2 > idle', MODEL_ANIM_HIT2, MODEL_ANIM_IDLE, true,
    'anim', 2, TGorillaAnimationTransitionMode.tmImmediatly);

  FLayer1.AddTransition('hit2 > hit1', MODEL_ANIM_HIT2, MODEL_ANIM_HIT1, false,
    'anim', 1, TGorillaAnimationTransitionMode.tmImmediatly);
  FLayer1.AddTransition('hit1 > hit2', MODEL_ANIM_HIT1, MODEL_ANIM_HIT2, false,
    'anim', 2, TGorillaAnimationTransitionMode.tmImmediatly);
end;

procedure TForm2.GorillaViewport1MouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; var Handled: Boolean);
begin
  if WheelDelta > 0 then
    GorillaCamera1.Position.Z := GorillaCamera1.Position.Z + 0.25
  else
    GorillaCamera1.Position.Z := GorillaCamera1.Position.Z - 0.25
end;

end.
