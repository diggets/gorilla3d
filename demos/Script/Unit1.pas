unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, Gorilla.Script,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.ScrollBox, FMX.Memo,
  Gorilla.Script.Types, FMX.Memo.Types;

type
  TForm1 = class(TForm)
    GorillaScriptEngine1: TGorillaScriptEngine;
    Memo1: TMemo;
    Button1: TButton;
    Memo2: TMemo;
    Splitter1: TSplitter;
    OpenDialog1: TOpenDialog;
    ToolBar1: TToolBar;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

  TGorillaScriptIOHandlerGUI = class(TGorillaScriptIOHandler)
    public
      class var Memo : TMemo;

      class procedure Write(AValue : String); override;
      class procedure WriteLn(AValue : String); override;
      class function Read() : String; override;
      class function ReadLn() : String; override;
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.Diagnostics;

{ TGorillaScriptIOHandlerGUI }

class procedure TGorillaScriptIOHandlerGUI.Write(AValue : String);
begin
  TThread.Synchronize(nil,
    procedure()
    begin
      Memo.Lines.Add(AValue);
    end
  );
end;

class procedure TGorillaScriptIOHandlerGUI.WriteLn(AValue : String);
begin
  TThread.Synchronize(nil,
    procedure()
    begin
      Memo.Lines.Add(AValue);
    end
  );
end;

class function TGorillaScriptIOHandlerGUI.Read() : String;
begin
  Result := '';
end;

class function TGorillaScriptIOHandlerGUI.ReadLn() : String;
begin
  Result := '';
end;

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
var LStr : TStringStream;
    LWatch : System.Diagnostics.TStopwatch;
begin
  Memo2.Lines.Clear();
  try
    LStr := TStringStream.Create(Memo1.Text);
    try
      GorillaScriptEngine1.LoadFromStream(LStr);
      GorillaScriptEngine1.Compile();

      LWatch := System.Diagnostics.TStopwatch.Create();
      LWatch.Start();

      GorillaScriptEngine1.Execute(false);

      LWatch.Stop();
      Memo2.Lines.Add(Format('Elapsed Time: %n sec.', [LWatch.ElapsedMilliseconds / 1000]));
    finally
      FreeAndNil(LStr);
    end;
  except
    on E: Exception do
    begin
      Memo2.Lines.Add(E.Message);
    end;
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var LWatch : System.Diagnostics.TStopwatch;
begin
  if OpenDialog1.Execute() then
    Memo1.Lines.LoadFromFile(OpenDialog1.FileName);

  Memo2.Lines.Clear();
  try
    GorillaScriptEngine1.LoadFromFile(OpenDialog1.FileName);
    GorillaScriptEngine1.Compile();

    LWatch := System.Diagnostics.TStopwatch.Create();
    LWatch.Start();

    GorillaScriptEngine1.Execute(false);

    LWatch.Stop();
    Memo2.Lines.Add(Format('Elapsed Time: %n sec.', [LWatch.ElapsedMilliseconds / 1000]));
  except
    on E: Exception do
    begin
      Memo2.Lines.Add(E.Message);
    end;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  TGorillaScriptIOHandlerGUI.Memo := Memo2;
end;

initialization

  TGorillaScriptIOHandler.Default := TGorillaScriptIOHandlerGUI;

end.
