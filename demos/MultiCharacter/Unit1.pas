unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  System.Generics.Collections,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light,
  Gorilla.Camera, Gorilla.Viewport, Gorilla.AssetsManager, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Model, Gorilla.Plane, FMX.Objects3D,
  FMX.MaterialSources, Gorilla.Material.Default, Gorilla.Material.Lambert;

type
  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FViewport: TGorillaViewport;
    FCamera: TGorillaCamera;
    FLight: TGorillaLight;
    FAssetsManager: TGorillaAssetsManager;
    FTarget: TDummy;
    FFloorMat,
    FMaterial: TGorillaLambertMaterialSource;
    FInMemoryPackage : TGorillaAssetsPackage;
    FCharacters : TList<TGorillaModel>;
    FFloor : TGorillaPlane;
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.IOUtils, System.Math,
  Gorilla.DefTypes,
  Gorilla.Animation,
  Gorilla.DAE.Loader;

procedure TForm1.FormCreate(Sender: TObject);
var LAssetsPath : String;
    I : Integer;
    LSrcMesh,
    LCopiedMesh : TGorillaModel;
begin
  // set platform specific assets path
{$IFDEF MSWINDOWS}
  LAssetsPath := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))) +
    IncludeTrailingPathDelimiter('assets');
{$ELSE}
  LAssetsPath := IncludeTrailingPathDelimiter(TPath.GetHomePath()) +
    IncludeTrailingPathDelimiter('assets');
{$ENDIF}

  FViewport := TGorillaViewport.Create(Self);
  FViewport.Parent := Self;

  FTarget := TDummy.Create(FViewport);
  FTarget.Parent := FViewport;

  FCamera := TGorillaCamera.Create(FViewport);
  FCamera.Parent := FTarget;
  FCamera.Target := FTarget;
  FCamera.Position.Point := Point3D(0, -10, -35);
  FViewport.Camera := FCamera;
  FViewport.UsingDesignCamera := false;

  FLight := TGorillaLight.Create(FViewport);
  FLight.Parent := FViewport;
  FLight.Position.Point := Point3D(0, -100, -100);
  FLight.LightType := TLightType.Point;

  // create an in-memory assets package to put mesh and animation to
  FAssetsManager := TGorillaAssetsManager.Create(Self);
  FInMemoryPackage := FAssetsManager.AddEmptyPackage('In-Memory');

  // create a material for all meshes
  FFloorMat := TGorillaLambertMaterialSource.Create(FViewport);
  FFloorMat.Parent := FViewport;
  FFloorMat.Texture.LoadFromFile(LAssetsPath + 'wood.jpg');

  // add floor plane
  FFloor := TGorillaPlane.Create(FViewport);
  FFloor.Parent := FViewport;
  FFloor.RotationAngle.X := 90;
  FFloor.SetSize(100, 100, 1);
  FFloor.MaterialSource := FFloorMat;

  FMaterial := TGorillaLambertMaterialSource.Create(FViewport);
  FMaterial.Parent := FViewport;
  FMaterial.Texture.LoadFromFile(LAssetsPath + 'candycane.jpg');

  // load mesh base once
  LSrcMesh := TGorillaModel.LoadNewModelFromFile(FViewport,
    FInMemoryPackage, LAssetsPath + 'c.dae',
    GORILLA_ANIMATION_CACHING_DEFAULT);
  LSrcMesh.Parent := FViewport; // source mesh not visible
  LSrcMesh.RotationAngle.X := 180;
  LSrcMesh.MaterialSource := FMaterial;
  LSrcMesh.Meshes[0].MaterialSource := FMaterial;

  // load run animation for character
  LSrcMesh.AddAnimationFromFile(LAssetsPath + 'c-run.dae',
    GORILLA_ANIMATION_CACHING_DEFAULT);
  LSrcMesh.AnimationManager.Current.Start;

  // create a list to hold all copied characters
  FCharacters := TList<TGorillaModel>.Create();

  // we create 16 copies of the character
  for I := 0 to 15 do
  begin
    LCopiedMesh := LSrcMesh.Duplicate(FViewport, false) as TGorillaModel;
    LCopiedMesh.Parent := FViewport;
    LCopiedMesh.RotationAngle.Z := 180;
    LCopiedMesh.MaterialSource := FMaterial;
    LCopiedMesh.Meshes[0].MaterialSource := FMaterial;
    LCopiedMesh.Name := 'Copy' + IntToStr(I+1);
    LCopiedMesh.Position.Point := Point3D(
      -12 + ((I mod 4) * 6),
      -5,
      -12 + (Floor(I / 4) * 6)
      );
    LCopiedMesh.AnimationManager.Current.Start;

    FCharacters.Add(LCopiedMesh);
  end;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FCharacters);
end;

end.
