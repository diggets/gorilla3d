unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light, Gorilla.DefTypes,
  Gorilla.Camera, FMX.Objects3D, Gorilla.Viewport, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Terrain, Gorilla.SkyBox, FMX.MaterialSources,
  Gorilla.Material.Default, Gorilla.Material.Terrain, Gorilla.Material.Lambert,
  FMX.ListBox, FMX.Controls.Presentation, FMX.StdCtrls, Gorilla.Cube,
  Gorilla.Transform;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    Dummy1: TDummy;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaTerrain1: TGorillaTerrain;
    GorillaSkyBox1: TGorillaSkyBox;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    Timer1: TTimer;
    ComboBox1: TComboBox;
    TrackBar1: TTrackBar;
    Button1: TButton;
    ComboBox2: TComboBox;
    GorillaTerrainMaterialSource2: TGorillaTerrainMaterialSource;
    GorillaTerrainMaterialSource1: TGorillaTerrainMaterialSource;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  Gorilla.Context.Texturing,
  Gorilla.Terrain.Algorithm,
  System.IOUtils;

procedure TForm1.Button1Click(Sender: TObject);
begin
  if ComboBox2.ItemIndex < 0 then
    ComboBox2.ItemIndex := 0;
  
  // we generate a random terrain, depending on the used algorithm
  // Hill, DiamondSquare, Mandelbrot, Perlin Noise, Plateau, Brownian
  GorillaTerrain1.RandomTerrain(TRandomTerrainAlgorithmType(ComboBox2.ItemIndex), true, false);
end;

procedure TForm1.ComboBox1Change(Sender: TObject);
begin
  case ComboBox1.ItemIndex of
    0 : GorillaTerrain1.MaterialSource := GorillaLambertMaterialSource1;
    1 : GorillaTerrain1.MaterialSource := GorillaTerrainMaterialSource1;
    2 : GorillaTerrain1.MaterialSource := GorillaTerrainMaterialSource2;

    else GorillaTerrain1.MaterialSource := GorillaLambertMaterialSource1;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
var LAssetsPath : String;
    LTerrainPath,
    LSkyBoxPath : String;
    LBmp        : TBitmap;
    LTerrBmp    : TGorillaTerrainBitmap;
begin
  LAssetsPath := IncludeTrailingPathDelimiter('assets');

  // ignore - because of mouse movement it will perform a hittest
  GorillaTerrain1.SetHitTestValue(false);

{$IFDEF ANDROID}
  LAssetsPath := IncludeTrailingPathDelimiter(TPath.GetHomePath()) +
    LAssetsPath;
{$ENDIF}

  // load terrain material
  LTerrainPath := LAssetsPath + IncludeTrailingPathDelimiter('terrain');

  // 1) a simple lambert material
  GorillaLambertMaterialSource1.UseTexture0 := true;
  GorillaLambertMaterialSource1.Texture.LoadFromFile(LTerrainPath + 'terrain-3.jpg');

  // 2) a complex terrain height material
  with GorillaTerrainMaterialSource1 do
  begin
    // height level 1
    LTerrBmp := Bitmaps.Add() as TGorillaTerrainBitmap;
    LTerrBmp.DisplayName := 'TerrainTexture0';
    LTerrBmp.MinHeight := 1;
    LTerrBmp.MaxHeight := -0.25;
    LTerrBmp.LowTransition := 0.25;
    LTerrBmp.HighTransition := 0.25;
    LTerrBmp.Tiling := TPointF.Create(8, 8);
    LTerrBmp.Bitmap.LoadFromFile(LTerrainPath + 'terrain-1.jpg');

    // height level 2
    LTerrBmp:= Bitmaps.Add() as TGorillaTerrainBitmap;
    LTerrBmp.DisplayName := 'TerrainTexture1';
    LTerrBmp.MinHeight := -0.25;
    LTerrBmp.MaxHeight := -0.5;
    LTerrBmp.LowTransition := 0.25;
    LTerrBmp.HighTransition := 0.25;
    LTerrBmp.Tiling := TPointF.Create(8, 8);
    LTerrBmp.Bitmap.LoadFromFile(LTerrainPath + 'terrain-2.jpg');

    // height level 3
    LTerrBmp:= Bitmaps.Add() as TGorillaTerrainBitmap;
    LTerrBmp.DisplayName := 'TerrainTexture2';
    LTerrBmp.MinHeight := -0.5;
    LTerrBmp.MaxHeight := -0.75;
    LTerrBmp.LowTransition := 0.25;
    LTerrBmp.HighTransition := 0.25;
    LTerrBmp.Tiling := TPointF.Create(8, 8);
    LTerrBmp.Bitmap.LoadFromFile(LTerrainPath + 'terrain-3.jpg');

    // height level 4
    LTerrBmp:= Bitmaps.Add() as TGorillaTerrainBitmap;
    LTerrBmp.DisplayName := 'TerrainTexture3';
    LTerrBmp.MinHeight := -0.75;
    LTerrBmp.MaxHeight := -1;
    LTerrBmp.LowTransition := 0.25;
    LTerrBmp.HighTransition := 0.25;
    LTerrBmp.Tiling := TPointF.Create(8, 8);
    LTerrBmp.Bitmap.LoadFromFile(LTerrainPath + 'terrain-4.jpg');
  end;

  // load skybox textures
  LSkyBoxPath := LAssetsPath + IncludeTrailingPathDelimiter('skybox');
  LBmp := TBitmap.CreateFromFile(LSkyBoxPath + 'negz.jpg');
  try
    GorillaSkyBox1.Textures[TGorillaCubeMapFace.NegativeZ] := LBmp;
    LBmp.LoadFromFile(LSkyBoxPath + 'posz.jpg');
    GorillaSkyBox1.Textures[TGorillaCubeMapFace.PositiveZ] := LBmp;
    LBmp.LoadFromFile(LSkyBoxPath + 'negy.jpg');
    GorillaSkyBox1.Textures[TGorillaCubeMapFace.NegativeY] := LBmp;
    LBmp.LoadFromFile(LSkyBoxPath + 'posy.jpg');
    GorillaSkyBox1.Textures[TGorillaCubeMapFace.PositiveY] := LBmp;
    LBmp.LoadFromFile(LSkyBoxPath + 'negx.jpg');
    GorillaSkyBox1.Textures[TGorillaCubeMapFace.NegativeX] := LBmp;
    LBmp.LoadFromFile(LSkyBoxPath + 'posx.jpg');
    GorillaSkyBox1.Textures[TGorillaCubeMapFace.PositiveX] := LBmp;
  finally
    FreeAndNil(LBmp);
  end;

  Timer1.Enabled := true;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  Dummy1.RotationAngle.Y := Dummy1.RotationAngle.Y + 1;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  GorillaTerrain1.Smoothing := Round(TrackBar1.Value);
end;

end.
