{*******************************************************}
{                                                       }
{           diggets.com Gorilla3D Framework             }
{                                                       }
{          Copyright(c) 2017-2022 Eric Berger           }
{              All rights reserved                      }
{                                                       }
{   Copyright and license exceptions noted in source    }
{                                                       }
{*******************************************************}

unit Unit1;

interface

/// by default we load all assets from package
{$DEFINE USE_PACKAGE}

/// during development we set up the package at runtime
/// only if assets are available as single this will work
{ $DEFINE SETUP_PACKAGE}

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Ani,
  Gorilla.Viewport, Gorilla.AssetsManager, System.Math.Vectors, FMX.Types3D,
  FMX.Controls3D, Gorilla.Light, Gorilla.Camera, FMX.Objects3D, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Model, Gorilla.Utils.Dialogue.Types,
  Gorilla.Utils.Dialogue.System, Gorilla.Utils.Path, Gorilla.UI.Dialogue.Overlay,
  Gorilla.Audio.FMOD, Gorilla.Audio.FMOD.Intf.ChannelGroup, Gorilla.Plane,
  FMX.MaterialSources, Gorilla.Material.Default, Gorilla.Material.Blinn,
  Gorilla.Controller, Gorilla.Controller.Passes.Reflection,
  Gorilla.Controller.Input, Gorilla.Controller.Input.Types,
  FMX.Controls.Presentation, FMX.StdCtrls, Gorilla.Particle.Influencer,
  Gorilla.Particle.Emitter, Gorilla.Particle.Explosion, Gorilla.Transform;

type
  /// Nothing is perfect, so aren't we!
  /// because character names were overwritten by a default behaviour
  /// we need to fix name display in the HUD
  ///
  /// We also execute the particle explosion event here, if the last
  /// dialogue item has beend reached (because HUD overwrites the
  /// default events of a dialogue system component)
  TFixedDialogueHUD = class(TGorillaDialogueHUD)
    procedure DoShowItem(const AItem : TGorillaDialogueItem;
      const ADoStartItem : Boolean); override;
  end;

  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    Dummy1: TDummy;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaDialogueSystem1: TGorillaDialogueSystem;
    KilianModel: TGorillaModel;
    FrankModel: TGorillaModel;
    SpeakerModel: TGorillaModel;
    FloatAnimation1: TFloatAnimation;
    GorillaFMODAudioManager1: TGorillaFMODAudioManager;
    GorillaPlane1: TGorillaPlane;
    GorillaBlinnMaterialSource1: TGorillaBlinnMaterialSource;
    GorillaRenderPassReflection1: TGorillaRenderPassReflection;
    GorillaInputController1: TGorillaInputController;
    Label1: TLabel;
    FloatAnimation2: TFloatAnimation;
    GorillaAssetsManager1: TGorillaAssetsManager;
    GorillaExplosionParticleEmitter1: TGorillaExplosionParticleEmitter;
    GorillaLight2: TGorillaLight;
    GorillaLight3: TGorillaLight;
    FloatAnimation3: TFloatAnimation;
    FloatAnimation4: TFloatAnimation;
    FloatAnimation5: TFloatAnimation;
    procedure FormCreate(Sender: TObject);
    procedure GorillaPath3DAnimation1Finish(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure GorillaInputController1HotKeys0Triggered(
      const AItem: TGorillaHotKeyItem; const ACurrentInput: TGorillaHotKeyRaw;
      const AMode: TGorillaInputMode);
    procedure GorillaDialogueSystem1BeginDialogueItem(
      const AItem: TGorillaDialogueItem;
      const AItems: TArray<Gorilla.Utils.Dialogue.System.TGorillaDialogueItem>);
  private
    FHUD : TGorillaDialogueHUD;
    FMusic : IGorillaFMODChannelGroup;

    procedure LoadSoundFilesFromPackage(APckg : TGorillaAssetsPackage;
      AGrp : TGorillaAssetsGroup; AFiles : TArray<String>; APlayIdx : Integer);
    procedure BugfixDialogueIds();
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.Rtti,
  Gorilla.DefTypes,
  Gorilla.FBX.Loader,
  Gorilla.DAE.Loader,
  Gorilla.Audio.FMOD.Intf.Sound;

{ TFixedDialogueHUD }

procedure TFixedDialogueHUD.DoShowItem(const AItem : TGorillaDialogueItem;
  const ADoStartItem : Boolean);
var LOverlay : TGorillaDialogueOverlay;
begin
  case AItem.Kind of
    TGorillaDialogueItemKind.Question:
      begin
        LOverlay := TGorillaDialogueQuestionOverlay.Create(Self);
        LOverlay.Parent := Self;
        LOverlay.Item := AItem;
        AItem.UIElement := LOverlay;
        LOverlay.Stored := false;
        // BUGFIX: because displayname is overwritten, the wrong character name is set
        TGorillaDialogueTextOverlay(LOverlay).Character.Text := AItem.Character;
      end;

    TGorillaDialogueItemKind.Answer:
      begin
        // answer items will automatically be created by the parent question
        // overlay component.
      end;

    TGorillaDialogueItemKind.Flow:
      begin
        LOverlay := TGorillaDialogueFlowOverlay.Create(Self);
        LOverlay.Parent := Self;
        LOverlay.Item := AItem;
        AItem.UIElement := LOverlay;
        LOverlay.Stored := false;
        // BUGFIX: because displayname is overwritten, the wrong character name is set
        TGorillaDialogueTextOverlay(LOverlay).Character.Text := AItem.Character;
      end;

    TGorillaDialogueItemKind.Reference:
      begin
        // do nothing
      end;

    else
      raise Exception.Create('undefined dialogue item kind');
  end;

  if ADoStartItem then
  begin
    AItem.Start();

    // BUGFIX: because HUD overwrites the GorillaDialogueSystem events
    // we call the event here
    Form1.GorillaDialogueSystem1BeginDialogueItem(AItem, []);
  end;
end;

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
var LAstPath,
    LAppPath,
    LAudioPath,
    LPath : String;
    LOpts : TGorillaLoadOptions;
    LPckg : TGorillaAssetsPackage;
  {$IFDEF USE_PACKAGE}
    LGroup  : TGorillaAssetsGroup;
    LDlgAst : TGorillaDialogueAsset;
    LImgAst : TGorillaTextureAsset;
  {$ELSE}
    LSpeach,
    LSound : IGorillaFMODSound;
  {$ENDIF}
begin
  /// for loading from file, we need our general application path 
  /// and an assets root path
  LAppPath := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0)));
  LPath := LAppPath + IncludeTrailingPathDelimiter('assets');

  // we set viewport background to white
  GorillaViewport1.Color := TAlphaColorRec.White;

  // create a head up display for our dialogue
  FHUD := TFixedDialogueHUD.Create(GorillaViewport1);
  FHUD.Parent := GorillaViewport1;
  FHUD.Align  := TAlignLayout.MostTop;
  FHUD.Viewport := GorillaViewport1;
  FHUD.AudioManager := GorillaFMODAudioManager1;

  /// depending on the USE_PACKAGE flag we load assets from files directly
  /// or out of a package file
{$IFDEF USE_PACKAGE}
  {$IFnDEF SETUP_PACKAGE}
  /// set to an invalid path, to test loading from package
  LPath := 'C:\Temp\';
  {$ENDIF}

  /// get the package by name (defined at design time)
  LPckg := GorillaAssetsManager1.GetPackage('Assets');
  /// but we set the file path at runtime, otherwise it will
  /// get loaded at design time, which conflicts runtime setup.
  LPckg.Filename := LAppPath + 'assets.zip';

  /// load the dialogue from assets package
  LGroup  := LPckg.GetGroup(GORILLA_ASSETS_DIALOGUE_DESCR);
  LDlgAst := LGroup.GetAssetFromFile(LPath + 'dialog.dia') as TGorillaDialogueAsset;
  if Assigned(LDlgAst) then
  begin
    // BUG: do not copy character names in items
    //GorillaDialogueSystem1.Assign(LDlgAst.DialogueSystem as TPersistent);

    // BUGFIX: we load the dialogue again from stream, then character names are available
    LDlgAst.Stream.Position := 0;
    try
      GorillaDialogueSystem1.LoadFromStream(LDlgAst.Stream);
    finally
      LDlgAst.Stream.Position := 0;
    end;
  end;
{$ELSE}
  /// without a package we set this to nil
  LPckg := nil;

  /// load the dialogue from file
  GorillaDialogueSystem1.LoadFromFile(LPath + 'dialog.dia');
{$ENDIF}

  /// load speaker model from file or from package if (LPckg != nil)
  LAstPath := LPath + IncludeTrailingPathDelimiter('speaker');
  LOpts := TGorillaLoadOptions.Create(LAstPath + 'big speaker.fbx', [], LPckg, nil);
  LOpts.TextureLimits := Point(512, 512);
  SpeakerModel.LoadFromFile(LOpts);
  SpeakerModel.Scale.Point := Point3D(0.005, 0.005, 0.005);
  SpeakerModel.RotationAngle.Y := 90;
  SpeakerModel.Position.Point := Point3D(0, 0, 5);
  SpeakerModel.SetHitTestValue(false);

  /// load Frank model from file or from package if (LPckg != nil)
  LAstPath := LPath + IncludeTrailingPathDelimiter('frank');
  LOpts := TGorillaLoadOptions.Create(LAstPath + 'Frank.dae', [], LPckg, nil);
  LOpts.TextureLimits := Point(512, 512);
  FrankModel.LoadFromFile(LOpts);
  FrankModel.Scale.Point := Point3D(0.01, 0.01, 0.01);
  FrankModel.RotationAngle.X := 180;
{$IFDEF SETUP_PACKAGE}
  FrankModel.AddAnimationFromFile(LAstPath + 'Frank-Talking.dae');
  FrankModel.AddAnimationFromFile(LAstPath + 'Frank-Dance.dae');
{$ENDIF}

  FrankModel.AnimationManager.PlayAnimation('Frank-Dance.dae');
  FrankModel.SetHitTestValue(false);
  FrankModel.FrustumCullingCheck := false;

  /// load Kilian model from file or from package if (LPckg != nil)
  LAstPath := LPath + IncludeTrailingPathDelimiter('kilian');
  LOpts := TGorillaLoadOptions.Create(LAstPath + 'Kilian.dae', [], LPckg, nil);
  LOpts.TextureLimits := Point(512, 512);
  KilianModel.LoadFromFile(LOpts);
  KilianModel.Scale.Point := Point3D(0.01, 0.01, 0.01);
  KilianModel.RotationAngle.X := 180;
  KilianModel.RotationAngle.Y := -90;
{$IFDEF SETUP_PACKAGE}
  KilianModel.AddAnimationFromFile(LAstPath + 'Kilian-Walking.dae');
  KilianModel.AddAnimationFromFile(LAstPath + 'Kilian-Yelling.dae');
  KilianModel.AddAnimationFromFile(LAstPath + 'Kilian-Dance.dae');
{$ENDIF}

  KilianModel.AnimationManager.PlayAnimation('Kilian-Walking.dae');
  KilianModel.SetHitTestValue(false);
  /// forces texture-setup before walking into screen
  /// otherwise we have a short loading break, which is not so nice
  KilianModel.FrustumCullingCheck := false;

  /// prepare particle effect (money explosion - preloading of the texture)
  GorillaExplosionParticleEmitter1.FrustumCullingCheck := false;
  GorillaExplosionParticleEmitter1.ParticleSize := TParticlePreset.Create(8, 16, 1, false);
  with GorillaExplosionParticleEmitter1.ParticleMaterial do
  begin
    UseTexture0 := true;
    IsTextureAtlas := true;
    AtlasRowCount := 1;
    AtlasColCount := 1;
    FrameWidth := 128;
    FrameHeight := 128;
  {$IFDEF USE_PACKAGE}
    LGroup  := LPckg.GetGroup(GORILLA_ASSETS_TEXTURE_DESCR);
    LImgAst := LGroup.GetAssetFromFile('dollar.jpg') as TGorillaTextureAsset;
    if Assigned(LImgAst) then
      Texture.Assign(LImgAst.Bitmap);
  {$ELSE}
    Texture.LoadFromFile('dollar.jpg');
  {$ENDIF}
  end;

  /// AUDIO FILES
  /// Setting up a channel group for background music playback
  FMusic   := GorillaFMODAudioManager1.AddChannelGroup('music');
  LAstPath := LPath + IncludeTrailingPathDelimiter('music');
  LAudioPath := LPath + IncludeTrailingPathDelimiter('audio');
  
{$IFDEF USE_PACKAGE}
  LGroup := LPckg.GetGroup(GORILLA_ASSETS_AUDIO_DESCR);

  // load music and speech files from package
  LoadSoundFilesFromPackage(LPckg, LGroup,
    [
    LAstPath + 'Mystery Mammal - Unauthorized Forms of Space Propulsion.mp3',
    LAudioPath + '01_whats-up.mp3',
    LAudioPath + '02_did-i-miss-something-or-why-are-you-dancing.mp3',
    LAudioPath + '03_its-party-time-man.mp3',
    LAudioPath + '04_what-party.mp3',
    LAudioPath + '05-01_were-celebrating-the-gorilla3d-release.mp3',
    LAudioPath + '05-02_oh-really-they-finally-made-it.mp3',
    LAudioPath + '05-03_so-shut-up-and-take-my-money.mp3',
    LAudioPath + '06-01_im-just-glad-my-rigging-works.mp3',
    LAudioPath + '06-02_okey.mp3',
    LAudioPath + '06-03_so-that-is-not-reality.mp3',
    LAudioPath + '06-04_no-man-this-is-future.mp3',
    LAudioPath + '06-05_alright-lets-dance.mp3',
    LAudioPath + '07-01_i-just-bought-the-brand-new-gorilla3d-version.mp3',
    LAudioPath + '07-02_oh-i-didnt-realize-that.mp3',
    LAudioPath + '07-03_yeah-man-im-hyped.mp3',
    LAudioPath + '07-04_me-too-lets-dance.mp3'
    ],
    0 {0 = play music}
    );
{$ELSE}
  /// load music from file
  LSound := GorillaFMODAudioManager1.LoadSoundFromFile(
    LAstPath + 'Mystery Mammal - Unauthorized Forms of Space Propulsion.mp3');

  /// load speech files from file
  LSpeach := GorillaFMODAudioManager1.LoadSoundFromFile(LAudioPath + '01_whats-up.mp3');
  LSpeach := GorillaFMODAudioManager1.LoadSoundFromFile(LAudioPath + '02_did-i-miss-something-or-why-are-you-dancing.mp3');
  LSpeach := GorillaFMODAudioManager1.LoadSoundFromFile(LAudioPath + '03_its-party-time-man.mp3');
  LSpeach := GorillaFMODAudioManager1.LoadSoundFromFile(LAudioPath + '04_what-party.mp3');
  LSpeach := GorillaFMODAudioManager1.LoadSoundFromFile(LAudioPath + '05-01_were-celebrating-the-gorilla3d-release.mp3');
  LSpeach := GorillaFMODAudioManager1.LoadSoundFromFile(LAudioPath);
  LSpeach := GorillaFMODAudioManager1.LoadSoundFromFile(LAudioPath + '05-03_so-shut-up-and-take-my-money.mp3');
  LSpeach := GorillaFMODAudioManager1.LoadSoundFromFile(LAudioPath + '06-01_im-just-glad-my-rigging-works.mp3');
  LSpeach := GorillaFMODAudioManager1.LoadSoundFromFile(LAudioPath + '06-02_okey.mp3');
  LSpeach := GorillaFMODAudioManager1.LoadSoundFromFile(LAudioPath + '06-03_so-that-is-not-reality.mp3');
  LSpeach := GorillaFMODAudioManager1.LoadSoundFromFile(LAudioPath + '06-04_no-man-this-is-future.mp3');
  LSpeach := GorillaFMODAudioManager1.LoadSoundFromFile(LAudioPath + '06-05_alright-lets-dance.mp3');
  LSpeach := GorillaFMODAudioManager1.LoadSoundFromFile(LAudioPath + '07-01_i-just-bought-the-brand-new-gorilla3d-version.mp3');
  LSpeach := GorillaFMODAudioManager1.LoadSoundFromFile(LAudioPath + '07-02_oh-i-didnt-realize-that.mp3');
  LSpeach := GorillaFMODAudioManager1.LoadSoundFromFile(LAudioPath + '07-03_yeah-man-im-hyped.mp3');
  LSpeach := GorillaFMODAudioManager1.LoadSoundFromFile(LAudioPath + '07-04_me-too-lets-dance.mp3');

  /// play background music
  LSound.Play(FMusic);
{$ENDIF}

  /// There is a strange bug on converting "event.params.id" string to int
  /// by this method we convert those values and write them back
  BugfixDialogueIds();

{$IFDEF SETUP_PACKAGE}
  /// if we setup the package by code, we need to store it, or model files
  /// are not generated inside of it
  LPckg.SaveToFile(LAppPath + 'assets.zip');
{$ENDIF}
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  /// free music channel group interface
  FMusic := nil;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  /// prepare model to allow walking
  KilianModel.Position.X := 5;
  GorillaInputController1.Enabled := true;
  Label1.BringToFront;
end;

procedure TForm1.LoadSoundFilesFromPackage(APckg : TGorillaAssetsPackage;
  AGrp : TGorillaAssetsGroup; AFiles : TArray<String>; APlayIdx : Integer);
var I       : Integer;
    LAudAst : TGorillaAudioAsset;
    LSound  : IGorillaFMODSound;
begin
  /// The method registers each file in supplied array AFiles
  /// it looks up the package group and loads it into the fmod audio manager
  for I := Low(AFiles) to High(AFiles) do
  begin
    LAudAst := AGrp.GetAssetFromFile(AFiles[I]) as TGorillaAudioAsset;
    if Assigned(LAudAst) then
    begin
      LAudAst.Stream.Position := 0;
      try
        LSound := GorillaFMODAudioManager1.LoadSoundFromStream(LAudAst.Stream);
        if (I = APlayIdx) then
          LSound.Play(FMusic);
      finally
        LAudAst.Stream.Position := 0;
      end;
    end;
  end;
end;

procedure TForm1.GorillaDialogueSystem1BeginDialogueItem(
  const AItem: TGorillaDialogueItem;
  const AItems: TArray<Gorilla.Utils.Dialogue.System.TGorillaDialogueItem>);
var LBmp : TBitmap;
begin
  /// Playback of explosion animation:
  /// But only if it is the last dialogue item!
  if AItem.Items.Count > 0 then
    Exit;

  if AItem.Ident.Equals('flow-item-5378') then
  begin
    /// we already prepared that case in FormCreate()
  end
  else
  begin
    /// unsetting preloaded texture, because we don't need it
    /// for the alternative explosion
    GorillaExplosionParticleEmitter1.ParticleSize := TParticlePreset.Create(1, 4, 1, false);
    with GorillaExplosionParticleEmitter1.ParticleMaterial do
    begin
      AtlasRowCount := 0;
      AtlasColCount := 0;
      FrameWidth := 0;
      FrameHeight := 0;
      /// unset preloaded dollar texture ...
      LBmp := TBitmap.Create();
      try
        Texture.Assign(LBmp);
      finally
        FreeAndNil(LBmp);
      end;
      IsTextureAtlas := false;
      UseTexture0    := false;
      UseTexturing   := false;
    end;
  end;

  /// start the particle effect
  GorillaExplosionParticleEmitter1.Start;
end;

procedure TForm1.GorillaInputController1HotKeys0Triggered(
  const AItem: TGorillaHotKeyItem; const ACurrentInput: TGorillaHotKeyRaw;
  const AMode: TGorillaInputMode);
begin
  /// If the tooltip, to move by arrow-left, is available
  /// hide it!
  if Label1.Visible then
  begin
    FloatAnimation2.Enabled := false;
    Label1.Visible := false;
  end;

  /// Check if character model was moved far enough by the user
  if KilianModel.Position.X < 2 then
  begin
    /// If it has reached the final position
    /// disable user control
    GorillaInputController1.Enabled := false;
    GorillaPath3DAnimation1Finish(nil);
    Exit;
  end;

  /// check if user stopped movement before reaching the final position
  /// in that case we have to switch back to idle animation
  case AMode of
    TGorillaInputMode.Activated :
      begin
        /// play walking animation while moving
        KilianModel.AnimationManager.PlayAnimation('Kilian-Walking.dae');
        /// simple movement without reminding cpu-speed / framerate (just as simple as possible)
        KilianModel.Position.X := KilianModel.Position.X - 0.01;
      end;

    TGorillaInputMode.Deactivated :
      begin
        /// play idle animation, if key was up and character stands still
        KilianModel.AnimationManager.PlayAnimation('Kilian.dae');
      end;
  end;
end;

procedure TForm1.GorillaPath3DAnimation1Finish(Sender: TObject);
var LDlg : TGorillaDialogue;
begin
  /// Character has reached the final position - We stop walking
  KilianModel.AnimationManager.PlayAnimation('Kilian.dae');

  /// We turn down volume of music to listen to the dialogue
  if Assigned(FMusic) then
    FMusic.Volume := 0.25;

  /// Start dialogue by the HUD component
  LDlg := GorillaDialogueSystem1.Dialogues.Items[0] as TGorillaDialogue;
  FHUD.Dialogue := LDlg;
  FHUD.Start();
end;

/// <summary>
/// There is a bug in converting a TValue.String (f.e. "1") into an integer
/// by TValue.AsInteger() -> this causes an exception for some reason! 
/// If TValue type is an integer by default, there is no exception. 
/// Therefor we convert those values before running the dialog.
/// </summary>
procedure TForm1.BugfixDialogueIds();

  procedure FixItemEvents(AItem : TGorillaDialogueItem);
  var I    : Integer;
      LItm : TGorillaDialogueItem;
      LEvt : TGorillaDialogueItemEvent;
      LVal : TValue;
      LStr : String;
      LInt : Integer;
  begin
    if not Assigned(AItem) then
      Exit;

    for I := 0 to AItem.Events.Count - 1 do
    begin
      LEvt := AItem.Events.Items[I] as TGorillaDialogueItemEvent;
      if LEvt.Kind = TGorillaDialogueEventKind.Audio then
      begin
        /// go through parameters to fix "id" value
        if not LEvt.Parameters.ContainsKey('id') then
          Continue;

        LVal := LEvt.Parameters.Items['id'];
        if LVal.IsOrdinal then
          LInt := LVal.AsOrdinal
        else
        begin
          LStr := LVal.ToString();
          if not TryStrToInt(LStr, LInt) then
            LInt := 0;
        end;

        LEvt.Parameters.Items['id'] := TValue.From<Integer>(LInt);
      end;
    end;

    /// scan sub items
    for I := 0 to AItem.Items.Count - 1 do
    begin
      LItm := AItem.Items.Items[I] as TGorillaDialogueItem;
      FixItemEvents(LItm);
    end;
  end;

var LDlg : TGorillaDialogue;
    I    : Integer;
    LItm : TGorillaDialogueItem;
begin
  /// get the first dialog in our system and fix all of its items.
  LDlg := GorillaDialogueSystem1.Dialogues.Items[0] as TGorillaDialogue;
  for I := 0 to LDlg.Items.Count - 1 do
  begin
    LItm := LDlg.Items.Items[I] as TGorillaDialogueItem;
    FixItemEvents(LItm);
  end;
end;

end.
