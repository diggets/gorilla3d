unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light, Gorilla.DefTypes,
  FMX.Objects3D, Gorilla.Camera, Gorilla.Viewport, FMX.MaterialSources,
  Gorilla.Material.Default, Gorilla.Material.Lambert, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Plane, FMX.StdCtrls, FMX.Controls.Presentation,
  FMX.ListBox, FMX.Colors, Gorilla.Torus, FMX.Effects, FMX.Filter.Effects,
  Gorilla.Transform;

type
  TForm2 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaCamera1: TGorillaCamera;
    Dummy1: TDummy;
    GorillaLight1: TGorillaLight;
    GorillaPlane1: TGorillaPlane;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    Timer1: TTimer;
    GorillaTorus1: TGorillaTorus;
    ComboBox1: TComboBox;
    GlowEffect1: TGlowEffect;
    InnerGlowEffect1: TInnerGlowEffect;
    RippleEffect1: TRippleEffect;
    SwirlEffect1: TSwirlEffect;
    MagnifyEffect1: TMagnifyEffect;
    SmoothMagnifyEffect1: TSmoothMagnifyEffect;
    WaveEffect1: TWaveEffect;
    WrapEffect1: TWrapEffect;
    BandedSwirlEffect1: TBandedSwirlEffect;
    PinchEffect1: TPinchEffect;
    TilerEffect1: TTilerEffect;
    PixelateEffect1: TPixelateEffect;
    EmbossEffect1: TEmbossEffect;
    SharpenEffect1: TSharpenEffect;
    ToonEffect1: TToonEffect;
    SepiaEffect1: TSepiaEffect;
    PaperSketchEffect1: TPaperSketchEffect;
    PencilStrokeEffect1: TPencilStrokeEffect;
    NormalBlendEffect1: TNormalBlendEffect;
    HueAdjustEffect1: THueAdjustEffect;
    ContrastEffect1: TContrastEffect;
    BloomEffect1: TBloomEffect;
    GloomEffect1: TGloomEffect;
    InvertEffect1: TInvertEffect;
    MonochromeEffect1: TMonochromeEffect;
    CropEffect1: TCropEffect;
    GaussianBlurEffect1: TGaussianBlurEffect;
    BoxBlurEffect1: TBoxBlurEffect;
    DirectionalBlurEffect1: TDirectionalBlurEffect;
    RadialBlurEffect1: TRadialBlurEffect;
    RasterEffect1: TRasterEffect;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
  private
    FEffects : TArray<TEffect>;
    FPreviousEffect : TEffect;
  public
    { Public-Deklarationen }
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

procedure TForm2.ComboBox1Change(Sender: TObject);
begin
  if Assigned(FPreviousEffect) then
    FPreviousEffect.Enabled := false;

  if (ComboBox1.ItemIndex > -1) and (ComboBox1.ItemIndex <= High(FEffects)) then
  begin
    FPreviousEffect := FEffects[ComboBox1.ItemIndex];
    FPreviousEffect.Enabled := true;
  end;

  // We have to force a viewport resize on changing the post effect
  GorillaViewport1.Align := TAlignLayout.Client;
  GorillaViewport1.Invalidate();
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  FPreviousEffect := nil;
  SetLength(FEffects, 31);
  FEffects := [
    GlowEffect1,
    InnerGlowEffect1,
    RippleEffect1,
    SwirlEffect1,
    MagnifyEffect1,
    SmoothMagnifyEffect1,
    WaveEffect1,
    WrapEffect1,
    BandedSwirlEffect1,
    PinchEffect1,
    TilerEffect1,
    PixelateEffect1,
    EmbossEffect1,
    SharpenEffect1,
    ToonEffect1,
    SepiaEffect1,
    PaperSketchEffect1,
    PencilStrokeEffect1,
    NormalBlendEffect1,
    HueAdjustEffect1,
    ContrastEffect1,
    BloomEffect1,
    GloomEffect1,
    InvertEffect1,
    MonochromeEffect1,
    CropEffect1,
    GaussianBlurEffect1,
    BoxBlurEffect1,
    DirectionalBlurEffect1,
    RadialBlurEffect1,
    RasterEffect1
    ];
end;

procedure TForm2.Timer1Timer(Sender: TObject);
begin
  Dummy1.RotationAngle.Y := Dummy1.RotationAngle.Y + 1;
end;

end.
