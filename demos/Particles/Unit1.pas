unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light, Gorilla.DefTypes,
  Gorilla.Camera, FMX.Objects3D, Gorilla.Viewport, Gorilla.Particle.Influencer,
  Gorilla.Particle.Emitter, Gorilla.Particle.Smoke, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Plane, FMX.MaterialSources, Gorilla.Material.Default,
  Gorilla.Material.Particle, Gorilla.Material.Lambert, Gorilla.Particle.Fire,
  Gorilla.Cube, Gorilla.SkyBox, Gorilla.Transform;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    Dummy1: TDummy;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaPlane1: TGorillaPlane;
    Timer1: TTimer;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    GorillaFireParticleEmitter1: TGorillaFireParticleEmitter;
    GorillaSmokeParticleEmitter1: TGorillaSmokeParticleEmitter;
    GorillaSkyBox1: TGorillaSkyBox;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    FFirePos : Single;
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.IOUtils;

procedure TForm1.FormCreate(Sender: TObject);
begin
{$IFDEF MSWINDOWS}
  GorillaLambertMaterialSource1.Texture.LoadFromFile('wood.jpg');
{$ENDIF}
{$IFDEF ANDROID}
//  GorillaSmokeParticleEmitter1.LoadTexture(
//    IncludeTrailingPathDelimiter(TPath.GetHomePath()) + 'smoke.png');
  GorillaLambertMaterialSource1.Texture.LoadFromFile(TPath.GetHomePath()) +
    'wood.jpg');
{$ENDIF}

  // adjust particle size
  GorillaSmokeParticleEmitter1.ParticleSize   := TParticlePreset.Create(16, 16, 1, false);
  GorillaSmokeParticleEmitter1.ParticleGrowth := TParticlePreset.Create(4, 8, 1, false);

  GorillaSmokeParticleEmitter1.Start();
  GorillaFireParticleEmitter1.Start();
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  GorillaViewport1.BeginUpdate();
  try
    FFirePos := FFirePos + 0.1;
    GorillaFireParticleEmitter1.Position.Point :=
      Point3D(Sin(FFirePos)*3, -1, Cos(FFirePos)*3);

    Dummy1.RotationAngle.Y := Dummy1.RotationAngle.Y + 1;
  finally
    GorillaViewport1.EndUpdate();
  end;
end;

end.
