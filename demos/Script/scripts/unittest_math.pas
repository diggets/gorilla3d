program unittest_math;

uses
	System.SysUtils;
	
	procedure Main();
	var LInt : Integer;
		LDbl : Double;
	begin
		/// https://lukaszwrobel.pl/blog/math-parser-part-4-tests/
		System.WriteLn('--- UNITTEST - MATH -------------------------------------------');

		System.WriteLn('should compute 89 when given 89');
		LInt := 89;
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('should compute -7 when given -7');
		LInt := -7;
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('should compute 5 when given 2 + 3');
		LInt := 2 + 3;
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('should compute 2 when given 5 - 3');
		LInt := 5 - 3;
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('should compute -1 when given 2 - 3');
		LInt := 2 - 3;
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('should compute 6 when given 2 * 3');
		LInt := 2 * 3;
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('should compute 3 when given 6 / 2');
		LDbl := 6 / 2;
		System.WriteLn(LDbl.ToString());
		
		System.WriteLn('should compute -3 when given -6 / 2');
		LDbl := -6 / 2;
		System.WriteLn(LDbl.ToString());
		
		System.WriteLn('should compute 2 when given 6 mod 4');
		LInt := 6 mod 4;
		System.WriteLn(LInt.ToString());

		System.WriteLn('should compute 2.5 when given 2.5');
		LDbl := 2.5;
		System.WriteLn(LDbl.ToString());
		
		System.WriteLn('should compute -7.765 when given -7.765');
		LDbl := -7.765;
		System.WriteLn(LDbl.ToString());
		
		System.WriteLn('should compute -4 when given -(8 / 4 + 2)');
		LDbl := -(8 / 4 + 2);
		System.WriteLn(LDbl.ToString());
		
		System.WriteLn('should compute 0 when given 8 / -4 + 2');
		LDbl := 8 / -4 + 2;
		System.WriteLn(LDbl.ToString());
		
		System.WriteLn('should compute 19 when given 4*2.5 + 8.5+1.5 / 3.0');
		LDbl := 4*2.5 + 8.5+1.5 / 3.0;
		System.WriteLn(LDbl.ToString());
		
		System.WriteLn('should compute ~5.01 when given 5.0005 + 0.0095');
		LDbl := 5.0005 + 0.0095;
		System.WriteLn(LDbl.ToString());
		
		System.WriteLn('should compute 10 when given 2 -4 +6 -1 -1- 0 +8');
		LInt := 2 -4 +6 -1 -1- 0 +8;
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('should compute 6 when given 1 -1   + 2   - 2   +  4 - 4 +    6');
		LInt := 1 -1   + 2   - 2   +  4 - 4 +    6;
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('should compute -12 when given 2*3 - 4*5 + 6/3');
		LDbl := 2*3 - 4*5 + 6/3;
		System.WriteLn(LDbl.ToString());
		
		System.WriteLn('should compute -1 when given 2*3*4/8 -   5/2*4 +  6 + 0/3');
		LDbl := 2*3*4/8 -   5/2*4 +  6 + 0/3;
		System.WriteLn(LDbl.ToString());
		
		System.WriteLn('should compute 2.5 when given 10/4');
		LDbl := 10/4;
		System.WriteLn(LDbl.ToString());
		
		System.WriteLn('should compute ~1.66 when given 5/3');
		LDbl := 5/3;
		System.WriteLn(LDbl.ToString());
		
		System.WriteLn('should compute ~ -6.4 when given 3 + 8/5 -1 -2*5');
		LDbl := 3 + 8/5 -1 -2*5;
		System.WriteLn(LDbl.ToString());
		
		
		
		System.WriteLn('should return Infinity when attempt to divide by zero occurs: 5/0');
		LDbl := 5/0;
		System.WriteLn(LDbl.ToString());
		
		System.WriteLn('should return Infinity when attempt to divide by zero occurs: 2 - 1 + 14/0 + 7');
		LDbl := 2 - 1 + 14/0 + 7;
		System.WriteLn(LDbl.ToString());
		
		
		System.WriteLn('should compute 66, when complex expressions enclosed in parenthesis given: (5 + 2*3 - 1 + 7 * 8)');
		LDbl := (5 + 2*3 - 1 + 7 * 8);
		System.WriteLn(LDbl.ToString());

		System.WriteLn('should compute 1, when complex expressions enclosed in parenthesis given: (67 + 2 * 3 - 67 + 2/1 - 7)');
		LDbl := (67 + 2 * 3 - 67 + 2/1 - 7);
		System.WriteLn(LDbl.ToString());

		System.WriteLn('should compute 8, when expressions with many subexpressions enclosed in parenthesis given: (2) + (17*2-30) * (5)+2 - (8/2)*4');
		LDbl := (2) + (17*2-30) * (5)+2 - (8/2)*4;
		System.WriteLn(LDbl.ToString());	
		
		System.WriteLn('should compute 18 when given ((2+10)/2*3)');
		LDbl := ((2+10)/2*3);
		System.WriteLn(LDbl.ToString());

		System.WriteLn('should compute 17 when given 2+10/2*3');
		LDbl := 2+10/2*3;
		System.WriteLn(LDbl.ToString());
			
		System.WriteLn('should compute 5, when nested parenthesis given: (((((5)))))');
		LDbl := (((((5)))));
		System.WriteLn(LDbl.ToString());
		
		System.WriteLn('should compute 30, when complex nested parenthesis given: (( ((2)) + 4))*((5))');
		LDbl := (( ((2)) + 4))*((5));
		System.WriteLn(LDbl.ToString());
	
		System.WriteLn('---------------------------------------------------------------');
	end;
	
begin
	Main();  
end.