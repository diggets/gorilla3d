unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Gorilla.Viewport, Gorilla.DefTypes, System.Math.Vectors, FMX.Types3D, FMX.Controls3D,
  Gorilla.Light, Gorilla.Camera, Gorilla.Control, Gorilla.Mesh, Gorilla.Model,
  Gorilla.AssetsManager, FMX.Objects3D, Gorilla.Plane, FMX.MaterialSources,
  Gorilla.Material.Default, Gorilla.Material.Lambert, Gorilla.Transform,
  Gorilla.Group, Gorilla.UI.Inventory, Gorilla.Utils.Inventory,
  System.ImageList, FMX.ImgList, FMX.Ani, Gorilla.Controller.Input.Character,
  Gorilla.Controller.Input.FirstPerson, Gorilla.Controller,
  Gorilla.Controller.Input, Gorilla.Controller.Input.Types,
  Gorilla.Controller.Input.TriggerPoint, FMX.Controls.Presentation, FMX.StdCtrls,
  FMX.Objects, Gorilla.UI.Inventory.Simple;

type
  TForm2 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    Weapon1: TGorillaModel;
    GorillaAssetsManager1: TGorillaAssetsManager;
    GorillaPlane1: TGorillaPlane;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    Weapons: TGorillaGroup;
    Weapon2: TGorillaModel;
    Weapon3: TGorillaModel;
    Weapon4: TGorillaModel;
    Weapon5: TGorillaModel;
    GorillaInventory1: TGorillaInventory;
    InventoryFrame1: TInventoryFrame;
    ImageList1: TImageList;
    WobbleAnimation: TFloatAnimation;
    GorillaFirstPersonController1: TGorillaFirstPersonController;
    GorillaInputController1: TGorillaInputController;
    GorillaTriggerPointManager1: TGorillaTriggerPointManager;
    Label1: TLabel;
    Label2: TLabel;
    AttackAnimation: TFloatAnimation;
    ProgressBar1: TProgressBar;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GorillaFirstPersonController1CustomAction(ASender: TObject;
      const AKind: TGorillaCharacterControllerHotKey;
      const AHotKey: TGorillaHotKeyItem;
      const AStates: TGorillaCharacterControllerStates;
      const AMode: TGorillaInputMode);
  private
    FActive      : TGorillaInventoryCollectable;
    FActiveModel : TGorillaModel;
    FDoCollect   : Boolean;

    procedure ToggleWeapon();
    procedure UpgradeWeapon();
    procedure CollectWeapon();
    procedure AttackWeapon();

    procedure DoOnCollectTriggered(ASender : TGorillaTriggerPointManager;
      const APos : TPoint3D; const AView : TPoint3D; const APoint : TGorillaTriggerPoint;
      const ADistance : Single);
    procedure DoOnCollectUnTriggered(ASender : TGorillaTriggerPointManager;
      const APos : TPoint3D; const AView : TPoint3D; const APoint : TGorillaTriggerPoint;
      const ADistance : Single);

    procedure DoOnWeaponUpgrade(const AManufacturer : TGorillaInventoryManufacturer;
      const AProcess : TGorillaInventoryManufacturingProcess;
      const AType : TManufacturingNotificationType);
    procedure DoOnWeaponUpgraded(const AManufacturer : TGorillaInventoryManufacturer;
      const AProcess : TGorillaInventoryManufacturingProcess;
      const AItem : TGorillaInventoryCollectable);

  public
    procedure PrepareInventory();
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

uses
  System.Rtti,
  Gorilla.GLTF.Loader,
  Gorilla.OBJ.Loader;

procedure TForm2.FormCreate(Sender: TObject);
var LPckg : TGorillaAssetsPackage;
    LTP : TGorillaTriggerPoint;
begin
  ProgressBar1.Visible := false;

  // setup inventory frame design
  InventoryFrame1.Align := TAlignLayout.Left;
  InventoryFrame1.Width := 128;
  InventoryFrame1.Margins := TBounds.Create(RectF(8, 8, 8, 8));
  InventoryFrame1.Images  := ImageList1;

  // load up all weapon types
  LPckg := GorillaAssetsManager1.GetPackage('Weapons');
  if not Assigned(LPckg) then
    raise Exception.Create('assets package not found');

  Weapon1.LoadFromFile(LPckg, '.\assets\weapon-01.obj', []);
  Weapon1.SetVisibility(true);
  FActiveModel := Weapon1;

  Weapon2.LoadFromFile(LPckg, '.\assets\weapon-02.obj', []);
  Weapon2.SetVisibility(false);

  Weapon3.LoadFromFile(LPckg, '.\assets\weapon-03.obj', []);
  Weapon3.SetVisibility(false);

  // place this weapon outside of the group - to be collected
  Weapon4.LoadFromFile(LPckg, '.\assets\weapon-04.obj', []);
  Weapon4.SetVisibility(true);

  Weapon5.LoadFromFile(LPckg, '.\assets\weapon-05.obj', []);
  Weapon5.SetVisibility(false);

  Weapons.RotationAngle.Point := Point3D(310, 230, 340);

  // configure trigger point
  LTP := GorillaTriggerPointManager1.Items.Items[0] as TGorillaTriggerPoint;
  LTP.OnTriggered := DoOnCollectTriggered;
  LTP.OnUnTriggered := DoOnCollectUnTriggered;

  PrepareInventory();
end;

procedure TForm2.FormShow(Sender: TObject);
begin
  InventoryFrame1.UpdateInventory();

  GorillaInputController1.Enabled := true;
end;

procedure TForm2.DoOnCollectTriggered(ASender : TGorillaTriggerPointManager;
  const APos : TPoint3D; const AView : TPoint3D; const APoint : TGorillaTriggerPoint;
  const ADistance : Single);
begin
  Label1.Text := 'Press [C] to collect';
  FDoCollect := true;
end;

procedure TForm2.DoOnCollectUnTriggered(ASender : TGorillaTriggerPointManager;
  const APos : TPoint3D; const AView : TPoint3D; const APoint : TGorillaTriggerPoint;
  const ADistance : Single);
begin
  Label1.Text := '';
  FDoCollect := false;
end;

procedure TForm2.DoOnWeaponUpgrade(const AManufacturer : TGorillaInventoryManufacturer;
  const AProcess : TGorillaInventoryManufacturingProcess;
  const AType : TManufacturingNotificationType);
begin
  // Progress contains the time already used - so we need to calculate
  // a procentage value from maximum time
  ProgressBar1.Value := (AProcess.Progress / AManufacturer.TimeNeeded) * 100;
end;

procedure TForm2.DoOnWeaponUpgraded(const AManufacturer : TGorillaInventoryManufacturer;
  const AProcess : TGorillaInventoryManufacturingProcess;
  const AItem : TGorillaInventoryCollectable);
begin
  InventoryFrame1.UpdateInventory();
  ProgressBar1.Visible := false;
end;

procedure TForm2.PrepareInventory();
var LGroup : TGorillaInventoryGroup;
    LW1, LW2, LW3, LW4, LW5 : TGorillaInventoryItemTemplate;
    LState : TGorillaInventoryCollectState;
begin
  // We do setup weapons inventory manually here to show how the component
  // works. You can instead use the inventory designer.

  // get weapons group
  LGroup := GorillaInventory1.FindGroup('Weapons');

  // configure weapon 1
  LW1 := GorillaInventory1.FindItemTemplate('Weapon1');
  // link to weapons group
  LW1.Group := LGroup;
  // link to 3d model
  LW1.Data := TValue.From<TGorillaModel>(Weapon1);
  Weapon1.TagObject := LW1;

  // configure weapon 2
  LW2 := GorillaInventory1.FindItemTemplate('Weapon2');
  // link to weapons group
  LW2.Group := LGroup;
  // link to 3d model
  LW2.Data := TValue.From<TGorillaModel>(Weapon2);
  Weapon2.TagObject := LW2;

  // configure weapon 3
  LW3 := GorillaInventory1.FindItemTemplate('Weapon3');
  // link to weapons group
  LW3.Group := LGroup;
  // link to 3d model
  LW3.Data := TValue.From<TGorillaModel>(Weapon3);
  Weapon3.TagObject := LW3;

  // configure weapon 4
  LW4 := GorillaInventory1.FindItemTemplate('Weapon4');
  // link to weapons group
  LW4.Group := LGroup;
  // link to 3d model
  LW4.Data := TValue.From<TGorillaModel>(Weapon4);
  Weapon4.TagObject := LW4;

  // configure weapon 5 (as a special weapon! - it's an upgrade to weapon 4)
  LW5 := GorillaInventory1.FindItemTemplate('Weapon5');
  // link to weapons group
  LW5.Group := LGroup;
  // mark weapon5 as upgrade and downgrade to weapon 4
  LW5.DowngradeItem := LW4;
  LW4.UpgradeItem := LW5;
  // link to 3d model
  LW5.Data := TValue.From<TGorillaModel>(Weapon5);
  Weapon5.TagObject := LW5;

  // finally we collect all 5 weapon types, so we have them in our "rucksack"
  FActive := GorillaInventory1.Collect(LW1, LState);
  if LState <> TGorillaInventoryCollectState.Collected then
    raise Exception.Create('failed to collect weapon 1');

  GorillaInventory1.Collect(LW2, LState);
  if LState <> TGorillaInventoryCollectState.Collected then
    raise Exception.Create('failed to collect weapon 2');

  GorillaInventory1.Collect(LW3, LState);
  if LState <> TGorillaInventoryCollectState.Collected then
    raise Exception.Create('failed to collect weapon 3');
end;

procedure TForm2.ToggleWeapon();
var I : Integer;
    LWM : TGorillaModel;
begin
  // toggle weapon - if last weapon used, then switch to first again
  I := FActive.Index + 1;
  if (I < GorillaInventory1.CollectedItems.Count) then
    FActive := GorillaInventory1.CollectedItems.Items[I] as TGorillaInventoryCollectable
  else
    FActive := GorillaInventory1.CollectedItems.Items[0] as TGorillaInventoryCollectable;

  FActiveModel.SetVisibility(false);
  FActiveModel := FActive.Template.Data.AsType<TGorillaModel>();
  FActiveModel.SetVisibility(true);
end;

procedure TForm2.UpgradeWeapon();
var LW : TGorillaInventoryCollectable;
    LMProc : TGorillaInventoryManufacturingProcess;
begin
  // we upgrade weapon4 to weapon5 if it was collected

  // at first we try to find weapon4 - only if we collected it
  // we are able to upgrade
  LW := GorillaInventory1.FindFirstCollectedItem('Weapon4');
  if not Assigned(LW) then
  begin
    ShowMessage('no weapon found to be upgraded');
    Exit;
  end;

  // start upgrading process
  if LW.IsUpgradable() then
  begin
    // toggle weapon if currently selected, otherwise FActive will be invalid
    // afterwards
    if (FActive = LW) then
      ToggleWeapon();

    // start upgrading process (3 sec.)
    LMProc := GorillaInventory1.Upgrade(LW);
    // notify upgrading process
    LMProc.Manufacturer.OnNotify := DoOnWeaponUpgrade;
    // notify when upgrading finished
    LMProc.Manufacturer.OnProduced := DoOnWeaponUpgraded;
    ProgressBar1.Visible := true;
    InventoryFrame1.UpdateInventory();
  end;
end;

procedure TForm2.CollectWeapon();
var LItem : TGorillaInventoryItemTemplate;
    LState : TGorillaInventoryCollectState;
begin
  // collect weapon 4 if close enough
  if not FDoCollect then
    Exit;

  // at first we virtually collect the weapon to put it in our inventory
  LItem := Weapon4.TagObject as TGorillaInventoryItemTemplate;
  GorillaInventory1.Collect(LItem, LState);
  if LState <> TGorillaInventoryCollectState.Collected then
    ShowMessage('failed to collect weapon');

  // afterwards we collect the weapon visually
  Weapon4.SetVisibility(false);
  Weapon4.Scale.Point := Point3D(1, 1, 1);
  Weapon4.Position.Point := TPoint3D.Zero;
  Weapon4.Parent := Weapons;

  // at last we update the inventory frame
  InventoryFrame1.UpdateInventory();
end;

procedure TForm2.AttackWeapon();
begin
  AttackAnimation.Enabled := false;
  AttackAnimation.Enabled := true;
  AttackAnimation.Start();
end;

procedure TForm2.GorillaFirstPersonController1CustomAction(ASender: TObject;
  const AKind: TGorillaCharacterControllerHotKey;
  const AHotKey: TGorillaHotKeyItem;
  const AStates: TGorillaCharacterControllerStates;
  const AMode: TGorillaInputMode);
begin
  case AKind of
    TGorillaCharacterControllerHotKey.KeyboardCustomAction1 :
      begin
        // key "[Q]" was pressed - toggle weapon
        ToggleWeapon();
      end;

    TGorillaCharacterControllerHotKey.KeyboardCustomAction2 :
      begin
        // key "[E]" was pressed - attack
        AttackWeapon();
      end;

    TGorillaCharacterControllerHotKey.KeyboardCustomAction3 :
      begin
        // key "[F]" was pressed - upgrade
        UpgradeWeapon();
      end;

    TGorillaCharacterControllerHotKey.KeyboardCustomAction4 :
      begin
        // key "[C]" was pressed - collect weapon
        CollectWeapon();
      end;
  end;
end;

end.
