unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  System.Generics.Collections,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light,
  Gorilla.Camera, FMX.Objects3D, Gorilla.Viewport, Gorilla.Prefab,
  Gorilla.Control, Gorilla.Transform, Gorilla.Mesh, Gorilla.Cube, Gorilla.SkyBox,
  Gorilla.Group;

type
  TForm1 = class(TForm)
    GorillaPrefabSystem1: TGorillaPrefabSystem;
    GorillaViewport1: TGorillaViewport;
    GorillaSkyBox1: TGorillaSkyBox;
    TopLight: TGorillaLight;
    Prefabs: TGorillaGroup;
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  Gorilla.Plane, Gorilla.Model, Gorilla.Material.Blinn;

procedure TForm1.FormCreate(Sender: TObject);
var LList   : TObjectList<TComponent>;
    LModel  : TGorillaModel;
    LCamera : TGorillaCamera;
begin
  LList := nil;
  GorillaPrefabSystem1.LoadAllPrefabs(LList);

  // let's find the camera from prefab and assign it to the viewport
  // the camera is attached to a dummy with a rotation animation out of the prefab
  LCamera := Self.FindComponentDeepSearchByClass(TGorillaCamera) as TGorillaCamera;
  if Assigned(LCamera) then
  begin
    GorillaViewport1.Camera := LCamera;
    GorillaViewport1.UsingDesignCamera := false;
  end;

  // let's find the animated model and start it
  LModel := Self.FindComponentDeepSearchByClass(TGorillaModel) as TGorillaModel;
  if not Assigned(LModel) then
    Exit;

  LModel.AnimationManager.Current.Start;
end;

end.
