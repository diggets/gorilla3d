unit Unit2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light, Gorilla.DefTypes,
  Gorilla.Camera, FMX.Objects3D, Gorilla.Viewport, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Terrain, FMX.MaterialSources, Gorilla.Material.Default,
  Gorilla.Material.Terrain, Gorilla.Cube, Gorilla.SkyBox, FMX.Objects,
  FMX.Layers3D, FMX.Controls.Presentation, FMX.StdCtrls, Gorilla.Layers,
  Gorilla.Transform;

type
  TForm2 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    Dummy1: TDummy;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaTerrain1: TGorillaTerrain;
    Timer1: TTimer;
    Image1: TImage;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    Image2: TImage;
    GorillaLayer3D1: TGorillaLayer3D;
    GorillaTerrainMaterialSource1: TGorillaTerrainMaterialSource;
    GorillaSkyBox1: TGorillaSkyBox;
    procedure Timer1Timer(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

procedure TForm2.FormCreate(Sender: TObject);
begin
  GorillaTerrain1.SetHitTestValue(false);
end;

procedure TForm2.SpeedButton1Click(Sender: TObject);
begin
  ShowMessage('That�s your job to do ;)');
end;

procedure TForm2.SpeedButton2Click(Sender: TObject);
begin
  ShowMessage('Powered by Gorilla3D');
end;

procedure TForm2.SpeedButton3Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TForm2.Timer1Timer(Sender: TObject);
begin
  Dummy1.RotationAngle.Y := Dummy1.RotationAngle.Y + 1;
end;

end.
