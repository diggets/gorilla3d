unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light, Gorilla.DefTypes,
  Gorilla.Camera, Gorilla.Viewport, Gorilla.Control, Gorilla.Mesh, Gorilla.Plane,
  FMX.MaterialSources, Gorilla.Material.Default, Gorilla.Material.Lambert,
  Gorilla.Controller, Gorilla.Controller.Input, FMX.Objects3D,
  Gorilla.Controller.Input.Character, Gorilla.Controller.Input.FirstPerson,
  FMX.Controls.Presentation, FMX.StdCtrls, Gorilla.Cone, Gorilla.Transform;

type
  TForm2 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaPlane1: TGorillaPlane;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    GorillaInputController1: TGorillaInputController;
    GorillaFirstPersonController1: TGorillaFirstPersonController;
    Label1: TLabel;
    GorillaCone1: TGorillaCone;
    procedure FormShow(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

procedure TForm2.FormShow(Sender: TObject);
begin
  GorillaInputController1.Enabled := true;
end;

end.
