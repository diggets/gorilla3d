{*******************************************************}
{                                                       }
{           diggets.com Gorilla3D Framework             }
{                                                       }
{          Copyright(c) 2017-2018 Eric Berger           }
{              All rights reserved                      }
{                                                       }
{   Copyright and license exceptions noted in source    }
{                                                       }
{*******************************************************}

{*******************************************************}
{                                                       }
{ * SHADER CODE:                                        }
{ * "Seascape" by Alexander Alekseev aka TDM - 2014     }
{ * License Creative Commons Attribution-NonCommercial- }
{ * ShareAlike 3.0 Unported License.                    }
{                                                       }
{ * Contact: tdmaav@gmail.com                           }
{ * Website: https://www.shadertoy.com/view/Ms2SD1      }
{                                                       }
{*******************************************************}

unit Gorilla.Material.Ocean;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Messaging,
  System.Math.Vectors,
  System.Types,
  System.UITypes,
  FMX.Types,
  FMX.Types3D,
  FMX.Graphics,
  FMX.Materials,
  FMX.MaterialSources,
  Gorilla.Controller,
  Gorilla.Context.GLES;

type
  {$M+}
  TGorillaOceanMaterialSource = class;

  /// <summary>
  /// Ocean material - fully-procedural sea surface computing without textures.
  /// Providing configuration possibilities for editing color, wave behaviour
  /// and light effects.
  /// </summary>
  TGorillaOceanMaterial = class(TCustomMaterial)
  private
    FSource       : TGorillaOceanMaterialSource;
    FTimerService : IFMXTimerService;
    FStartTime    : Double;
    FRenderTime   : Double;
    FLastTime     : Double;

    FSeaHeight    : Single;
    FSeaChoppy    : Single;
    FSeaSpeed     : Single;
    FSeaFreq      : Single;
    FSeaBase      : TAlphaColorF;
    FSeaColor     : TAlphaColorF;
    FScale        : TPointF;

    FReflectionPower : Single;
    FRefractionPower : Single;

    [Weak] FReflection : TTexture;
    [Weak] FRefraction : TTexture;

  protected
    procedure SetReflection(const Value: TTexture);
    procedure SetRefraction(const Value: TTexture);

    procedure DoApply(const Context: TContext3D); override;
    procedure DoInitialize(); override;

  public
    constructor Create(const ASource : TGorillaOceanMaterialSource); reintroduce; virtual;
    destructor Destroy(); override;

  published
    /// <summary>
    /// Defines the height of sea waves.
    /// </summary>
    /// <remarks>
    /// Default value is set to 0.6.
    /// </remarks>
    property SeaHeight : Single read FSeaHeight write FSeaHeight;
    /// <summary>
    /// Defines how choppy the sea is displayed. Low value means less movement.
    /// </summary>
    /// <remarks>
    /// Default value is set to 4.
    /// </remarks>
    property SeaChoppy : Single read FSeaChoppy write FSeaChoppy;
    /// <summary>
    /// Defines how fast waves are moving across.
    /// </summary>
    /// <remarks>
    /// Default value is set to 0.8.
    /// </remarks>
    property SeaSpeed : Single read FSeaSpeed write FSeaSpeed;
    /// <summary>
    /// Defines the wave frequency. This has influence on distance between each
    /// wave.
    /// </summary>
    /// <remarks>
    /// Default value is set to 0.16.
    /// </remarks>
    property SeaFreq : Single read FSeaFreq write FSeaFreq;
    /// <summary>
    /// Defines the basic diffuse color of sea water.
    /// </summary>
    /// <remarks>
    /// Default value is set to vec4(0.22, 0.19, 0.1, 1.0).
    /// </remarks>
    property SeaBase : TAlphaColorF read FSeaBase write FSeaBase;
    /// <summary>
    /// Defines an additional specular color which is used for color computation.
    /// </summary>
    /// <remarks>
    /// Default value is set to vec4(0.6, 0.9, 0.8, 1.0).
    /// </remarks>
    property SeaColor : TAlphaColorF read FSeaColor write FSeaColor;
    /// <summary>
    /// The ocean is rendered in dependency of the context resolution. By this
    /// scaling factor you can manipulate those resolution value.
    /// wave.
    /// </summary>
    /// <remarks>
    /// Default value is set to vec2(1.0, 1.0).
    /// </remarks>
    property Scale : TPointF read FScale write FScale;

    property Reflection : TTexture read FReflection write SetReflection;
    property ReflectionPower : Single read FReflectionPower write FReflectionPower;
    property Refraction : TTexture read FRefraction write SetRefraction;
    property RefractionPower : Single read FRefractionPower write FRefractionPower;
  end;

  /// <summary>
  /// Material source with "Seascape" shader by Alexander Alekseev.
  /// This material renders an ocean with waves.
  /// </summary>
  /// <remarks>
  /// License Creative Commons Attribution-NonCommercial-
  /// ShareAlike 3.0 Unported License.
  /// Contact: tdmaav@gmail.com
  /// Website: https://www.shadertoy.com/view/Ms2SD1
  /// </remarks>
  TGorillaOceanMaterialSource = class(TMaterialSource)
  protected
    FReflectionPass : TGorillaRenderPassController; // TGorillaRenderPassReflection;
    FRefractionPass : TGorillaRenderPassController; // TGorillaRenderPassRefraction;

    function GetSeaBase: TAlphaColorF;
    function GetSeaChoppy: Single;
    function GetSeaColor: TAlphaColorF;
    function GetSeaFreq: Single;
    function GetSeaHeight: Single;
    function GetSeaSpeed: Single;
    function GetScale() : TPointF;
    function GetReflectionPower() : Single;
    function GetRefractionPower() : Single;

    procedure SetSeaBase(const Value: TAlphaColorF);
    procedure SetSeaChoppy(const Value: Single);
    procedure SetSeaColor(const Value: TAlphaColorF);
    procedure SetSeaFreq(const Value: Single);
    procedure SetSeaHeight(const Value: Single);
    procedure SetSeaSpeed(const Value: Single);
    procedure SetScale(const Value : TPointF);
    procedure SetReflectionPower(const Value : Single);
    procedure SetRefractionPower(const Value : Single);
    procedure SetReflectionPass(const Value: TGorillaRenderPassController);
    procedure SetRefractionPass(const Value: TGorillaRenderPassController);

    function CreateMaterial() : TMaterial; override;
    procedure DoPassesChanged(Sender: TObject);

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;

  published
    /// <summary>
    /// Defines the height of sea waves.
    /// </summary>
    /// <remarks>
    /// Default value is set to 0.6.
    /// </remarks>
    property SeaHeight : Single read GetSeaHeight write SetSeaHeight;
    /// <summary>
    /// Defines how choppy the sea is displayed. Low value means less movement.
    /// </summary>
    /// <remarks>
    /// Default value is set to 4.
    /// </remarks>
    property SeaChoppy : Single read GetSeaChoppy write SetSeaChoppy;
    /// <summary>
    /// Defines how fast waves are moving across.
    /// </summary>
    /// <remarks>
    /// Default value is set to 0.8.
    /// </remarks>
    property SeaSpeed : Single read GetSeaSpeed write SetSeaSpeed;
    /// <summary>
    /// Defines the wave frequency. This has influence on distance between each
    /// wave.
    /// </summary>
    /// <remarks>
    /// Default value is set to 0.16.
    /// </remarks>
    property SeaFreq : Single read GetSeaFreq write SetSeaFreq;
    /// <summary>
    /// Defines the basic diffuse color of sea water.
    /// </summary>
    /// <remarks>
    /// Default value is set to vec4(0.22, 0.19, 0.1, 1.0).
    /// </remarks>
    property SeaBase : TAlphaColorF read GetSeaBase write SetSeaBase;
    /// <summary>
    /// Defines an additional specular color which is used for color computation.
    /// </summary>
    /// <remarks>
    /// Default value is set to vec4(0.6, 0.9, 0.8, 1.0).
    /// </remarks>
    property SeaColor : TAlphaColorF read GetSeaColor write SetSeaColor;
    /// <summary>
    /// The ocean is rendered in dependency of the context resolution. By this
    /// scaling factor you can manipulate those resolution value.
    /// wave.
    /// </summary>
    /// <remarks>
    /// Default value is set to vec2(1.0, 1.0).
    /// </remarks>
    property Scale : TPointF read GetScale write SetScale;

    /// <summary>
    /// ReflectionPass is experimental! May produce shattered result.
    /// </summary>
    property ReflectionPass : TGorillaRenderPassController read FReflectionPass write SetReflectionPass;
    /// <summary>
    /// ReflectionPass is experimental! May produce shattered result.
    /// </summary>
    property ReflectionPower : Single read GetReflectionPower write SetReflectionPower;
    /// <summary>
    /// RefractionPass is experimental! May produce shattered result.
    /// </summary>
    property RefractionPass : TGorillaRenderPassController read FRefractionPass write SetRefractionPass;
    /// <summary>
    /// RefractionPass is experimental! May produce shattered result.
    /// </summary>
    property RefractionPower : Single read GetRefractionPower write SetRefractionPower;
  end;

  procedure Register();

implementation

uses
  System.Math,
  System.IOUtils,
  FMX.Platform,
  Gorilla.Controller.Passes.Refraction,
  Gorilla.Controller.Passes.Reflection;

  ResourceString GORILLA_OCEAN_VS =
    'attribute vec3 a_Position;' +
    'attribute vec2 a_TexCoord0;' +

    'uniform mat4 _ModelViewProjMatrix;' +
    'uniform mat4 _ModelViewMatrix;' +
    'uniform mat4 _ReflectTextureMatrix;' + // projector view-perspective-bias-matrix for reflection
    'uniform mat4 _RefractTextureMatrix;' + // projector view-perspective-bias-matrix for refraction

    'varying vec2 v_texCoord;' +
    'varying vec4 v_ReflProjTextureCoords;' +
    'varying vec4 v_RefrProjTextureCoords;' +

    'void main(void) {' +
    '  vec4 l_Position = vec4(a_Position, 1.0);' +
    '  gl_Position = _ModelViewProjMatrix * l_Position;' +
    '  v_texCoord = a_TexCoord0;' +

    '  v_ReflProjTextureCoords = _ReflectTextureMatrix * _ModelViewMatrix * l_Position;' +
    '  v_RefrProjTextureCoords = _RefractTextureMatrix * _ModelViewMatrix * l_Position;' +
    '}';

  ResourceString GORILLA_OCEAN_FS_1 =
    'uniform float _Time;' +
    'uniform float _SeaHeight;' +
    'uniform float _SeaChoppy;' +
    'uniform float _SeaSpeed;' +
    'uniform float _SeaFreq;' +
    'uniform vec4 _SeaBase;' +
    'uniform vec4 _SeaColor;' +
    'uniform vec3 _Resolution;' +
    'uniform vec3 _LightDir;' +
    'uniform float _Opacity;' +

    'uniform float _ReflectionPower;' +
    'uniform sampler2D _ReflectionTexture;' +
    'uniform float _RefractionPower;' +
    'uniform sampler2D _RefractionTexture;' +

    'varying vec2 v_texCoord;' +
    'varying vec4 v_ReflProjTextureCoords;' +
    'varying vec4 v_RefrProjTextureCoords;' +

    'vec3 iResolution = _Resolution;' + //vec3(640.0, 480.0, 0.0);' +
    'const vec4 iMouse = vec4(0.0, 0.0, 0.0, 0.0);' +

    /// *
    /// * "Seascape" by Alexander Alekseev aka TDM - 2014
    /// * License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
    /// * Contact: tdmaav@gmail.com
    /// *
    'const int NUM_STEPS = 8;' +
    'const float PI	 	= 3.141592;' +
    'const float EPSILON	= 1e-3;' +

//    '#define EPSILON_NRM (0.1 / iResolution.x)' +
    'float EPSILON_NRM(){' +
    '  return 0.1 / iResolution.x;' +
    '}' +

    // sea
    'const int ITER_GEOMETRY = 3;' +
    'const int ITER_FRAGMENT = 5;' +

    'float SEA_HEIGHT = _SeaHeight;' + //0.6;' +
    'float SEA_CHOPPY = _SeaChoppy;' + //4.0;' +
    'float SEA_SPEED = _SeaSpeed;' + //0.8;' +
    'float SEA_FREQ = _SeaFreq;' + //0.16;' +
    'vec3 SEA_BASE = _SeaBase.rgb;' + //vec3(0.1, 0.19, 0.22);' +
    'vec3 SEA_WATER_COLOR = _SeaColor.rgb;' + //vec3(0.8, 0.9, 0.6);' +

//    '#define SEA_TIME (1.0 + _Time * SEA_SPEED)' +
    'vec2 SEA_TIME(){' +
    '  float lVal = 1.0 + _Time * SEA_SPEED;' +
    '  return vec2(lVal, lVal);' +
    '}' +

    'const mat2 octave_m = mat2(1.6, 1.2, -1.2, 1.6);' +

    // math
    'mat3 fromEuler(vec3 ang){' +
    '  vec2 a1 = vec2(sin(ang.x), cos(ang.x));' +
    '  vec2 a2 = vec2(sin(ang.y), cos(ang.y));' +
    '  vec2 a3 = vec2(sin(ang.z), cos(ang.z));' +
    '  mat3 m;' +
    '  m[0] = vec3(a1.y*a3.y + a1.x*a2.x*a3.x, a1.y*a2.x*a3.x + a3.y*a1.x, -a2.y*a3.x);' +
    '  m[1] = vec3(-a2.y*a1.x, a1.y*a2.y, a2.x);' +
    '  m[2] = vec3(a3.y*a1.x*a2.x + a1.y*a3.x, a1.x*a3.x - a1.y*a3.y*a2.x, a2.y*a3.y);' +
    '  return m;' +
    '}' +

    'float hash(vec2 p){' +
    '  float h = dot(p, vec2(127.1, 311.7));' +
    '    return fract(sin(h) * 43758.5453123);' +
    '}' +

    'float noise(in vec2 p){' +
    '  vec2 i = floor( p );' +
    '  vec2 f = fract( p );' +
    '  vec2 u = f * f * (3.0 - 2.0 * f);' +
    '  return -1.0 + 2.0 * mix( mix( hash( i + vec2(0.0, 0.0) ),' +
    '                     hash( i + vec2(1.0, 0.0) ), u.x),' +
    '                mix( hash( i + vec2(0.0, 1.0) ),' +
    '                     hash( i + vec2(1.0, 1.0) ), u.x), u.y);' +
    '}';

  ResourceString GORILLA_OCEAN_FS_2 =
    // lighting
    'float diffuse(vec3 n, vec3 l, float p){' +
    '  return pow(dot(n, l) * 0.4 + 0.6, p);' +
    '}' +

    'float specular(vec3 n, vec3 l, vec3 e, float s){' +
    '  float nrm = (s + 8.0) / (PI * 8.0);' +
    '  return pow(max(dot(reflect(e, n), l), 0.0), s) * nrm;' +
    '}' +

    // sky
    'vec3 getSkyColor(vec3 e){' +
    '  e.y = max(e.y, 0.0);' +
    '  return vec3(pow(1.0-e.y,2.0), 1.0-e.y, 0.6+(1.0-e.y)*0.4);' +
    '}' +

    // sea
    'float sea_octave(vec2 uv, float choppy){' +
    '  uv += noise(uv);' +
    '  vec2 wv = 1.0 - abs(sin(uv));' +
    '  vec2 swv = abs(cos(uv));' +
    '  wv = mix(wv, swv, wv);' +
    '  return pow(1.0 - pow(wv.x * wv.y,0.65), choppy);' +
    '}' +

    'float map(vec3 p){' +
    '    float freq = SEA_FREQ;' +
    '    float amp = SEA_HEIGHT;' +
    '    float choppy = SEA_CHOPPY;' +
    '    vec2 uv = p.xz;' +
    '    uv.x *= 0.75;' +

    '    float d, h = 0.0;' +
    '    for(int i = 0; i < ITER_GEOMETRY; i++) {' +
    '      d = sea_octave((uv + SEA_TIME()) * freq, choppy);' +
    '      d += sea_octave((uv - SEA_TIME()) * freq, choppy);' +
    '      h += d * amp;' +
    '      uv *= octave_m;' +
    '      freq *= 1.9;' +
    '      amp *= 0.22;' +
    '      choppy = mix(choppy, 1.0, 0.2);' +
    '    }' +
    '    return p.y - h;' +
    '}' +

    'float map_detailed(vec3 p){' +
    '    float freq = SEA_FREQ;' +
    '    float amp = SEA_HEIGHT;' +
    '    float choppy = SEA_CHOPPY;' +
    '    vec2 uv = p.xz;' +
    '    uv.x *= 0.75;' +

    '    float d, h = 0.0;' +
    '    for(int i = 0; i < ITER_FRAGMENT; i++){' +
    '      d = sea_octave((uv + SEA_TIME()) * freq, choppy);' +
    '      d += sea_octave((uv - SEA_TIME()) * freq, choppy);' +
    '      h += d * amp;' +
    '      uv *= octave_m; freq *= 1.9; amp *= 0.22;' +
    '      choppy = mix(choppy,1.0,0.2);' +
    '    }' +
    '    return p.y - h;' +
    '}' +

    'vec3 getSeaColor(vec3 p, vec3 n, vec3 l, vec3 eye, vec3 dist){' +
    '    float fresnel = clamp(1.0 - dot(n, -eye), 0.0, 1.0);' +
    '    fresnel = pow(fresnel,3.0) * 0.65;' +

    '    vec3 reflected = getSkyColor(reflect(eye, n));' +
    '    vec3 refracted = SEA_BASE + diffuse(n, l, 80.0) * SEA_WATER_COLOR * 0.12;' +

    '    vec3 color = mix(refracted, reflected, fresnel);' +

    '    float atten = max(1.0 - dot(dist, dist) * 0.001, 0.0);' +
    '    color += SEA_WATER_COLOR * (p.y - SEA_HEIGHT) * 0.18 * atten;' +

    '    color += vec3(specular(n, l, eye, 60.0));' +

    '    return color;' +
    '}';

  ResourceString GORILLA_OCEAN_FS_3 =
    // tracing
    'vec3 getNormal(vec3 p, float eps){' +
    '    vec3 n;' +
    '    n.y = map_detailed(p);' +
    '    n.x = map_detailed(vec3(p.x + eps, p.y, p.z)) - n.y;' +
    '    n.z = map_detailed(vec3(p.x, p.y, p.z + eps)) - n.y;' +
    '    n.y = eps;' +
    '    return normalize(n);' +
    '}' +

    'float heightMapTracing(vec3 ori, vec3 dir, out vec3 p){' +
    '    float tm = 0.0;' +
    '    float tx = 1000.0;' +
    '    float hx = map(ori + dir * tx);' +
    '    if(hx > 0.0) return tx;' +
    '    float hm = map(ori + dir * tm);' +
    '    float tmid = 0.0;' +
    '    for(int i = 0; i < NUM_STEPS; i++) {' +
    '      tmid = mix(tm,tx, hm/(hm-hx));' +
    '      p = ori + dir * tmid;' +
    '      float hmid = map(p);' +
    '      if(hmid < 0.0) {' +
    '        tx = tmid;' +
    '        hx = hmid;' +
    '      } else {' +
    '        tm = tmid;' +
    '        hm = hmid;' +
    '      }' +
    '    }' +
    '    return tmid;' +
    '}' +

    // main
    'void mainImage( out vec4 fragColor, in vec2 fragCoord ){' +
    {
    '  vec2 uv = fragCoord.xy / iResolution.xy;' +
    '  uv = uv * 2.0 - 1.0;' +
    '  uv.x *= iResolution.x / iResolution.y;' +
    }
    '  vec2 uv = v_texCoord.xy;' +
    '  uv.y = -uv.y;' +
    '  float time = _Time * 0.3 + iMouse.x * 0.01;' +

       // ray
    '  vec3 ang = vec3(sin(time*3.0)*0.1, sin(time)*0.2+0.3, time);' +
    '  vec3 ori = vec3(0.0, 3.5, time * 5.0);' +
    '  vec3 dir = normalize(vec3(uv.xy, -2.0));' +
    '  dir.z += length(uv) * 0.15;' +
    '  dir = normalize(dir) * fromEuler(ang);' +

       // tracing
    '  vec3 p;' +
    '  heightMapTracing(ori, dir, p);' +
    '  vec3 dist = p - ori;' +
    '  float l_eps = dot(dist, dist) * EPSILON_NRM();' +
    '  vec3 n = getNormal(p, l_eps);' +
//    '  vec3 light = normalize(vec3(0.0, 1.0, 0.8));' +
    '  vec3 light = _LightDir;' +

       // color
    '  vec3 color = mix(' +
    '    getSkyColor(dir),' +
    '    getSeaColor(p, n, light, dir, dist),' +
    '    pow(smoothstep(0.0, -0.05, dir.y), 0.3));' +

        // post
    '  fragColor = vec4(pow(color, vec3(0.75)), _SeaBase.a);' +
    '}' +

    'void main(){' +
    '  vec4 l_Color;' +
    '  mainImage(l_Color, gl_FragCoord.xy);' +

    // add reflection
    '  if(_ReflectionPower > 0.0){' +
    '    vec4 l_ReflTex = textureProj(_ReflectionTexture, v_ReflProjTextureCoords);' +
    '    l_Color = mix(l_Color, l_ReflTex, _ReflectionPower);' +
    '  }' +

    // add refraction
    '  if(_RefractionPower > 0.0){' +
    '    vec4 l_RefrTex = textureProj(_RefractionTexture, v_RefrProjTextureCoords);' +
    '    l_Color = mix(l_Color, l_RefrTex, _RefractionPower);' +
    '  }' +

    // transparency
    '  if(_Opacity < 0.99999){' +
    '    l_Color = vec4(l_Color.rgb, l_Color.a * _Opacity);' +
    '  }' +

    '  gl_FragColor = l_Color;' +
    '}';

procedure Register();
begin
//  RegisterComponents('Gorilla3D', [TGorillaOceanMaterialSource]);
end;

{ TOceanMaterial }

constructor TGorillaOceanMaterial.Create(const ASource : TGorillaOceanMaterialSource);
begin
  inherited Create();

  FSource := ASource;

  if FTimerService = nil then
    if not TPlatformServices.Current.SupportsPlatformService(IFMXTimerService, FTimerService) then
      raise EUnsupportedPlatformService.Create('IFMXTimerService');

  FLastTime   := FTimerService.GetTick();
  FRenderTime := FLastTime;
  FStartTime  := FLastTime;

  FScale        := TPointF.Create(1, 1);
  FSeaHeight    := 0.6;
  FSeaChoppy    := 4;
  FSeaSpeed     := 0.8;
  FSeaFreq      := 0.16;
  FSeaBase      := TAlphaColorF.Create(0.22, 0.19, 0.1, 1.0);
  FSeaColor     := TAlphaColorF.Create(0.6, 0.9, 0.8, 1.0);

  // by default no reflection and refraction applied
  FReflectionPower := 0;
  FRefractionPower := 0;
end;

destructor TGorillaOceanMaterial.Destroy();
begin
  inherited;
end;

procedure TGorillaOceanMaterial.DoInitialize();
var LOGLBytes : TArray<Byte>;
begin
  ///
  /// VertexShader
  ///
  LOGLBytes := TEncoding.ASCII.GetBytes(GORILLA_OCEAN_VS);
  FVertexShader := TShaderManager.RegisterShaderFromData('ocean.fvs', TContextShaderKind.VertexShader, '', [
    TContextShaderSource.Create(TContextShaderArch.GLSL,
      LOGLBytes,
      [
      TContextShaderVariable.Create('ModelViewProjMatrix', TContextShaderVariableKind.Matrix, 0, 4),
      TContextShaderVariable.Create('ModelViewMatrix', TContextShaderVariableKind.Matrix, 1, 4),
      TContextShaderVariable.Create('ReflectTextureMatrix', TContextShaderVariableKind.Matrix, 2, 4),
      TContextShaderVariable.Create('RefractTextureMatrix', TContextShaderVariableKind.Matrix, 3, 4)
      ]
    )
  ]);


  ///
  /// PixelShader / FragmentShader
  ///
  LOGLBytes := TEncoding.ASCII.GetBytes(GORILLA_OCEAN_FS_1 + GORILLA_OCEAN_FS_2 + GORILLA_OCEAN_FS_3);
  FPixelShader := TShaderManager.RegisterShaderFromData('ocean.fps', TContextShaderKind.PixelShader, '', [
    TContextShaderSource.Create(TContextShaderArch.GLSL,
      LOGLBytes,
      [
      TContextShaderVariable.Create('Time', TContextShaderVariableKind.Float, 0, 1),
      TContextShaderVariable.Create('SeaHeight', TContextShaderVariableKind.Float, 1, 1),
      TContextShaderVariable.Create('SeaChoppy', TContextShaderVariableKind.Float, 2, 1),
      TContextShaderVariable.Create('SeaSpeed', TContextShaderVariableKind.Float, 3, 1),
      TContextShaderVariable.Create('SeaFreq', TContextShaderVariableKind.Float, 4, 1),
      TContextShaderVariable.Create('SeaBase', TContextShaderVariableKind.Vector, 5, 1),
      TContextShaderVariable.Create('SeaColor', TContextShaderVariableKind.Vector, 6, 1),
      TContextShaderVariable.Create('Resolution', TContextShaderVariableKind.Float3, 7, 1),
      TContextShaderVariable.Create('LightDir', TContextShaderVariableKind.Float3, 8, 1),
      TContextShaderVariable.Create('Opacity', TContextShaderVariableKind.Float, 9, 1),
      TContextShaderVariable.Create('ReflectionPower', TContextShaderVariableKind.Float, 10, 1),
      TContextShaderVariable.Create('ReflectionTexture', TContextShaderVariableKind.Texture, 11, 0),
      TContextShaderVariable.Create('RefractionPower', TContextShaderVariableKind.Float, 12, 1),
      TContextShaderVariable.Create('RefractionTexture', TContextShaderVariableKind.Texture, 13, 0)
      ]
    )
  ]);
end;

procedure TGorillaOceanMaterial.DoApply(const Context: TContext3D);
var
  MaxLightCount : Integer;
  I             : Integer;
  CLight        : integer;
  LLight        : TLightDescription;
  LLightPos     : TVector3D;
  LCtx          : TCustomContextOpenGL;
  LMat          : TMatrix3D;
  LTime         : Single;
  LLightDir     : TPoint3D;
  LResolution   : TPointF;
  LReflPow,
  LRefrPow      : Single;
  LRenderPassChg: Boolean;
begin
  LCtx := TCustomContextOpenGL(Context);

  LResolution.X := Context.Width;
  LResolution.Y := Context.Height;
  LResolution := LResolution * FScale;

  FLastTime   := FRenderTime;
  FRenderTime := FTimerService.GetTick();

  // lights
  CLight := 0;
  for I := 0 to LCtx.Lights.Count - 1 do
    if LCtx.Lights[i].Enabled then
      Inc(CLight);
  CLight := Min(TContext3D.MaxLightCount, CLight);

  // set shaders
  LCtx.SetShaders(FVertexShader, FPixelShader);

  TCustomContextOpenGL(Context).SetShaderVariable('ModelViewProjMatrix', LCtx.CurrentModelViewProjectionMatrix, true);
  TCustomContextOpenGL(Context).SetShaderVariable('ModelViewMatrix', LCtx.CurrentMatrix, true);

  case CLight of
    0 : MaxLightCount := 0;

    1 : if TContext3D.MaxLightCount > 4 then MaxLightCount := 2
                                        else MaxLightCount := 1;

    2 : MaxLightCount := 2;

    3,
    4 : MaxLightCount := 4;

    else MaxLightCount := 8;
  end;

  LTime := (FRenderTime - FStartTime) * 0.01;
  Context.SetShaderVariable('Time', [Vector3D(LTime, 0, 0, 0)]);

  Context.SetShaderVariable('SeaHeight', [Vector3D(FSeaHeight, 0, 0, 0)]);
  Context.SetShaderVariable('SeaChoppy', [Vector3D(FSeaChoppy, 0, 0, 0)]);
  Context.SetShaderVariable('SeaSpeed', [Vector3D(FSeaSpeed, 0, 0, 0)]);
  Context.SetShaderVariable('SeaFreq', [Vector3D(FSeaFreq, 0, 0, 0)]);
  Context.SetShaderVariable('SeaBase', FSeaBase.ToAlphaColor());
  Context.SetShaderVariable('SeaColor', FSeaColor.ToAlphaColor());
  Context.SetShaderVariable('Resolution', [Vector3D(LResolution.X, LResolution.Y, 0, 0)]);
  Context.SetShaderVariable('Opacity', [Vector3D(Context.CurrentOpacity, 0, 0, 0)]);

  // lights (single light supported)
  LLightDir := Point3D(0.0, 1.0, 0.8);
  if MaxLightCount > 0 then
  begin
    for I := 0 to LCtx.Lights.Count - 1 do
    begin
      LLight := LCtx.Lights[I];
      if LLight.Enabled then
      begin
        LLightPos := Vector3D(LLight.Position, 1);
        LLightDir := (TPoint3D.Zero - LLightPos).Normalize();
        break;
      end;
    end;
  end;

  Context.SetShaderVariable('LightDir', [Vector3D(LLightDir.X, LLightDir.Y, LLightDir.Z, 1)]);

  // reflection matrix
  if Assigned(FSource.FReflectionPass) then
  begin
    LMat := TGorillaRenderPassReflection(FSource.FReflectionPass).GetTextureMatrix(Context);
    LReflPow := FReflectionPower;
  end
  else
  begin
    LMat := TMatrix3D.Identity;
    LReflPow := 0;
  end;

  TCustomContextOpenGL(Context).SetShaderVariable('ReflectTextureMatrix', LMat, true);

  // refraction matrix
  if Assigned(FSource.FRefractionPass) then
  begin
    LMat := TGorillaRenderPassRefraction(FSource.FRefractionPass).GetTextureMatrix(Context);
    LRefrPow := FRefractionPower;
  end
  else
  begin
    LMat := TMatrix3D.Identity;
    LRefrPow := 0;
  end;

  TCustomContextOpenGL(Context).SetShaderVariable('RefractTextureMatrix', LMat, true);

  // reflection and refraction render passes request
  if Assigned(FSource) then
  begin
    LRenderPassChg := false;
    // check for changes
    if Assigned(FSource.FReflectionPass) then
    begin
      LRenderPassChg := (FSource.FReflectionPass.OutputTexture <> FReflection);
    end;

    if not LRenderPassChg then
    begin
      if Assigned(FSource.FRefractionPass) then
        LRenderPassChg := (FSource.FRefractionPass.OutputTexture <> FRefraction);
    end;

    if LRenderPassChg then
      FSource.DoPassesChanged(FSource);
  end;

  Context.SetShaderVariable('ReflectionPower', [Vector3D(LReflPow, 0, 0, 0)]);
  Context.SetShaderVariable('ReflectionTexture', Self.FReflection);
  Context.SetShaderVariable('RefractionPower', [Vector3D(LRefrPow, 0, 0, 0)]);
  Context.SetShaderVariable('RefractionTexture', Self.FRefraction);
end;

procedure TGorillaOceanMaterial.SetReflection(const Value: TTexture);
begin
  if FReflection <> Value then
  begin
    FReflection := Value;
  end;

  // The change notification is performed outside the if, because the content of texture could be changed
  DoChange();
end;

procedure TGorillaOceanMaterial.SetRefraction(const Value: TTexture);
begin
  if FRefraction <> Value then
  begin
    FRefraction := Value;
  end;

  // The change notification is performed outside the if, because the content of texture could be changed
  DoChange();
end;











{ TOceanMaterialSource }

constructor TGorillaOceanMaterialSource.Create(AOwner: TComponent);
begin
  inherited;
end;

destructor TGorillaOceanMaterialSource.Destroy();
begin
  inherited;
end;

procedure TGorillaOceanMaterialSource.DoPassesChanged(Sender: TObject);
begin
  if assigned(FReflectionPass) then
    TGorillaOceanMaterial(Material).Reflection := FReflectionPass.OutputTexture;

  if assigned(FRefractionPass) then
    TGorillaOceanMaterial(Material).Refraction := FRefractionPass.OutputTexture;
end;

function TGorillaOceanMaterialSource.GetSeaBase() : TAlphaColorF;
begin
  Result := TGorillaOceanMaterial(Material).SeaBase;
end;

function TGorillaOceanMaterialSource.GetSeaChoppy() : Single;
begin
  Result := TGorillaOceanMaterial(Material).SeaChoppy;
end;

function TGorillaOceanMaterialSource.GetSeaColor() : TAlphaColorF;
begin
  Result := TGorillaOceanMaterial(Material).SeaColor;
end;

function TGorillaOceanMaterialSource.GetSeaFreq() : Single;
begin
  Result := TGorillaOceanMaterial(Material).SeaFreq;
end;

function TGorillaOceanMaterialSource.GetSeaHeight() : Single;
begin
  Result := TGorillaOceanMaterial(Material).SeaHeight;
end;

function TGorillaOceanMaterialSource.GetSeaSpeed() : Single;
begin
  Result := TGorillaOceanMaterial(Material).SeaSpeed;
end;

function TGorillaOceanMaterialSource.GetScale() : TPointF;
begin
  Result := TGorillaOceanMaterial(Material).Scale;
end;

function TGorillaOceanMaterialSource.GetReflectionPower() : Single;
begin
  result := TGorillaOceanMaterial(Material).ReflectionPower;
end;

function TGorillaOceanMaterialSource.GetRefractionPower() : Single;
begin
  result := TGorillaOceanMaterial(Material).RefractionPower;
end;

procedure TGorillaOceanMaterialSource.SetSeaBase(const Value: TAlphaColorF);
begin
  TGorillaOceanMaterial(Material).SeaBase := Value;
end;

procedure TGorillaOceanMaterialSource.SetSeaChoppy(const Value: Single);
begin
  TGorillaOceanMaterial(Material).SeaChoppy := Value;
end;

procedure TGorillaOceanMaterialSource.SetSeaColor(const Value: TAlphaColorF);
begin
  TGorillaOceanMaterial(Material).SeaColor := Value;
end;

procedure TGorillaOceanMaterialSource.SetSeaFreq(const Value: Single);
begin
  TGorillaOceanMaterial(Material).SeaFreq := Value;
end;

procedure TGorillaOceanMaterialSource.SetSeaHeight(const Value: Single);
begin
  TGorillaOceanMaterial(Material).SeaHeight := Value;
end;

procedure TGorillaOceanMaterialSource.SetSeaSpeed(const Value: Single);
begin
  TGorillaOceanMaterial(Material).SeaSpeed := Value;
end;

procedure TGorillaOceanMaterialSource.SetScale(const Value: TPointF);
begin
  TGorillaOceanMaterial(Material).Scale := Value;
end;

procedure TGorillaOceanMaterialSource.SetReflectionPower(const Value : Single);
begin
  TGorillaOceanMaterial(Material).ReflectionPower := Value;
end;

procedure TGorillaOceanMaterialSource.SetRefractionPower(const Value : Single);
begin
  TGorillaOceanMaterial(Material).RefractionPower := Value;
end;

procedure TGorillaOceanMaterialSource.SetReflectionPass(
  const Value: TGorillaRenderPassController);
begin
  if (FReflectionPass <> Value) then
  begin
    FReflectionPass := Value;
    DoPassesChanged(Self);
  end;
end;

procedure TGorillaOceanMaterialSource.SetRefractionPass(
  const Value: TGorillaRenderPassController);
begin
  if (FRefractionPass <> Value) then
  begin
    FRefractionPass := Value;
    DoPassesChanged(Self);
  end;
end;

function TGorillaOceanMaterialSource.CreateMaterial() : TMaterial;
begin
  Result := TGorillaOceanMaterial.Create(Self);
end;

initialization
  RegisterFmxClasses([TGorillaOceanMaterialSource]);

end.