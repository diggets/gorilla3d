program unittest_loops;

uses
	System.SysUtils;
	
	procedure Main();
	var LInt : Integer;
		LVal : Int32;
	begin
		System.WriteLn('--- UNITTEST - LOOPS ------------------------------------------');
		
		///
		/// for loops
		///
		System.WriteLn('for-loop should compute 9');
		LVal := -1;
		for LInt := 0 to 9 do
			LVal := LInt;
		System.WriteLn(LVal.ToString());
		
		System.WriteLn('for-loop should compute -1 (empty loop)');
		LVal := -1;
		for LInt := 0 to 9 do
			;
		System.WriteLn(LVal.ToString());
		
		System.WriteLn('for-loop with complex condition should compute -1');
		LVal := -1;
		for LInt := 0 to ((2+10)/2*3) do
			System.WriteLn(LInt.ToString() + ': loop');
		System.WriteLn(LVal.ToString());
		
		System.WriteLn('for-loop with break should compute 5');
		LVal := 0;
		for LInt := 0 to 9 do
			if (LInt = 5) then
			begin
			  Break;
			end
			else LVal := LVal + 1;
		System.WriteLn(LVal.ToString());
		
		System.WriteLn('for-loop with continue should compute 9');
		LVal := 0;
		for LInt := 0 to 9 do
			if (LInt = 5) then
			begin
			  Continue;
			end
			else LVal := LVal + 1;
		System.WriteLn(LVal.ToString());
		
		///
		/// while loops
		///
		
		System.WriteLn('while-loop should compute 10');
		LInt := 0;
		while (LInt < 10) do
		begin
			LInt := LInt + 1;
		end;
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('while-loop should compute -1 (empty loop)');
		LInt := -1;
		while (LInt > -1) do
			;
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('while-loop without brackets should compute 10');
		LInt := 0;
		while LInt < 10 do
		begin
			LInt := LInt + 1;
		end;
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('while-loop with multiple condition should compute 6');
		LInt := 0;
		while (LInt < 10) and (LInt < 5) or (LInt = 5) do
		begin
			LInt := LInt + 1;
		end;
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('while-loop with break should compute 6');
		LInt := 0;
		while (LInt < 10) do
		begin
			if (LInt = 6) then
				Break;
				
			LInt := LInt + 1;
		end;
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('while-loop with continue should compute 9');
		LInt := 0;
		LVal := 0;
		while (LInt < 10) do
		begin
			LInt := LInt + 1;
			if (LInt = 6) then
				Continue;
				
			LVal := LVal + 1;
		end;
		System.WriteLn(LVal.ToString());
		
		///
		/// repeat loops
		///
		
		System.WriteLn('repeat-loop should compute 9');
		LInt := 10;
		repeat
			LInt := LInt - 1;
		until (LInt >= 0);
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('repeat-loop should compute -1 (empty loop)');
		LInt := -1;
		repeat until (LInt < 0);
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('repeat-loop without brackets should compute 10');
		LInt := 0;
		repeat
			LInt := LInt + 1;
		until LInt >= 10;
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('repeat-loop with multiple condition should compute 5');
		LInt := 0;
		repeat
			LInt := LInt + 1;
		until (LInt >= 10) and (LInt > 5) or (LInt = 5);
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('repeat-loop with break should compute 6');
		LInt := 0;
		repeat
			if (LInt = 6) then
				Break;
				
			LInt := LInt + 1;
		until (LInt >= 10);
		System.WriteLn(LInt.ToString());
		
		System.WriteLn('repeat-loop with continue should compute 9');
		LInt := 0;
		LVal := 0;
		repeat
			LInt := LInt + 1;
			if (LInt = 6) then
				Continue;
				
			LVal := LVal + 1;
		until (LInt >= 10);
		System.WriteLn(LVal.ToString());
		
		System.WriteLn('---------------------------------------------------------------');
	end;
	
begin
	Main();  
end.