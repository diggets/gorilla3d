unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Types3D,
  System.Math.Vectors, FMX.Controls3D, Gorilla.Light, Gorilla.Viewport,
  FMX.MaterialSources, Gorilla.Material.Default, Gorilla.Material.Lambert,
  Gorilla.Control, Gorilla.Transform, Gorilla.Mesh, Gorilla.Terrain,
  Gorilla.Model, Gorilla.Material.Behaviour, Gorilla.Material.Behaviour.Wind,
  FMX.Objects, FMX.Controls.Presentation, FMX.Edit, FMX.EditBox, FMX.NumberBox,
  FMX.StdCtrls, FMX.ListBox, FMX.Colors;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaLight1: TGorillaLight;
    GorillaTerrain1: TGorillaTerrain;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    GrassTemplate: TGorillaModel;
    PalmsModel: TGorillaModel;
    GorillaMaterialWindBehaviour1: TGorillaMaterialWindBehaviour;
    DinosaurModel: TGorillaModel;
    Rectangle1: TRectangle;
    NumberBox1: TNumberBox;
    NumberBox2: TNumberBox;
    NumberBox3: TNumberBox;
    Label1: TLabel;
    Label2: TLabel;
    NumberBox4: TNumberBox;
    NumberBox5: TNumberBox;
    NumberBox6: TNumberBox;
    Label3: TLabel;
    NumberBox7: TNumberBox;
    NumberBox8: TNumberBox;
    NumberBox9: TNumberBox;
    Label4: TLabel;
    NumberBox10: TNumberBox;
    Label5: TLabel;
    NumberBox11: TNumberBox;
    Label6: TLabel;
    NumberBox12: TNumberBox;
    CheckBox1: TCheckBox;
    ColorComboBox1: TColorComboBox;
    Label7: TLabel;
    NumberBox13: TNumberBox;
    Label8: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure NumberBox1Change(Sender: TObject);
    procedure NumberBox2Change(Sender: TObject);
    procedure NumberBox3Change(Sender: TObject);
    procedure NumberBox4Change(Sender: TObject);
    procedure NumberBox5Change(Sender: TObject);
    procedure NumberBox6Change(Sender: TObject);
    procedure NumberBox7Change(Sender: TObject);
    procedure NumberBox8Change(Sender: TObject);
    procedure NumberBox9Change(Sender: TObject);
    procedure NumberBox10Change(Sender: TObject);
    procedure NumberBox11Change(Sender: TObject);
    procedure NumberBox12Change(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure ColorComboBox1Change(Sender: TObject);
    procedure NumberBox13Change(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  Gorilla.FBX.Loader;

procedure TForm1.FormCreate(Sender: TObject);
begin
  // Attach wind to grass
  GrassTemplate.AttachMaterialBehaviour(GorillaMaterialWindBehaviour1);

  // Attach wind to palms
  PalmsModel.AttachMaterialBehaviour(GorillaMaterialWindBehaviour1);

  // Setup dinosaur
  DinosaurModel.LoadFromFile(nil, '.\assets\raptor-dinosaur-indoraptor\raptor.fbx', []);
  DinosaurModel.AnimationManager.Current.Start();
end;

procedure TForm1.NumberBox1Change(Sender: TObject);
begin
  GorillaMaterialWindBehaviour1.WindDir.X := NumberBox1.Value;
end;

procedure TForm1.NumberBox2Change(Sender: TObject);
begin
  GorillaMaterialWindBehaviour1.WindDir.Y := NumberBox2.Value;
end;

procedure TForm1.NumberBox3Change(Sender: TObject);
begin
  GorillaMaterialWindBehaviour1.WindDir.Z := NumberBox3.Value;
end;

procedure TForm1.NumberBox4Change(Sender: TObject);
begin
  GorillaMaterialWindBehaviour1.WindWidth := NumberBox4.Value;
end;

procedure TForm1.NumberBox5Change(Sender: TObject);
begin
  GorillaMaterialWindBehaviour1.WindHeight := NumberBox5.Value;
end;

procedure TForm1.NumberBox6Change(Sender: TObject);
begin
  GorillaMaterialWindBehaviour1.WindDistance := NumberBox6.Value;
end;

procedure TForm1.NumberBox7Change(Sender: TObject);
begin
  GorillaMaterialWindBehaviour1.WindSource.X := NumberBox7.Value;
end;

procedure TForm1.NumberBox8Change(Sender: TObject);
begin
  GorillaMaterialWindBehaviour1.WindSource.Y := NumberBox8.Value;
end;

procedure TForm1.NumberBox9Change(Sender: TObject);
begin
  GorillaMaterialWindBehaviour1.WindSource.Z := NumberBox9.Value;
end;

procedure TForm1.NumberBox10Change(Sender: TObject);
begin
  GorillaMaterialWindBehaviour1.WindPower := NumberBox10.Value;
end;

procedure TForm1.NumberBox11Change(Sender: TObject);
begin
  GorillaMaterialWindBehaviour1.WindWaving := NumberBox11.Value;
end;

procedure TForm1.NumberBox12Change(Sender: TObject);
begin
  GorillaMaterialWindBehaviour1.WindDecay := NumberBox12.Value;
end;

procedure TForm1.NumberBox13Change(Sender: TObject);
begin
  GorillaMaterialWindBehaviour1.WindSpeed := NumberBox13.Value;
end;

procedure TForm1.CheckBox1Change(Sender: TObject);
begin
  GorillaMaterialWindBehaviour1.ShowVisualizer := CheckBox1.IsChecked;
end;

procedure TForm1.ColorComboBox1Change(Sender: TObject);
begin
  GorillaMaterialWindBehaviour1.WindColor := ColorComboBox1.Color;
end;

end.
