unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light, Gorilla.DefTypes,
  Gorilla.Camera, Gorilla.Viewport, Gorilla.Control, Gorilla.Mesh,
  Gorilla.Terrain, Gorilla.Controller.Input.Character,
  Gorilla.Controller.Input.FirstPerson, Gorilla.Controller.Input.ThirdPerson,
  FMX.Objects3D, Gorilla.PhysicsCharacterController, Gorilla.Physics,
  Gorilla.Capsule, Gorilla.Controller, Gorilla.Controller.Input,
  FMX.MaterialSources, Gorilla.Material.Default, Gorilla.Material.Terrain,
  Gorilla.Material.Lambert, Gorilla.Physics.Q3.Scene, Gorilla.Controller.Input.Types,
  Gorilla.Physics.Q3.Body, Gorilla.Model, Gorilla.AssetsManager,
  Gorilla.Material.Phong, Gorilla.Sphere, Gorilla.Animation.Controller,
  FMX.Controls.Presentation, FMX.StdCtrls, Gorilla.Transform;

const
  CHARACTER_MODEL = 'c.dae';
  CHARACTER_IDLE = 'c-idle.dae';
  CHARACTER_RUN = 'c-run.dae';

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaTerrain1: TGorillaTerrain;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    Timer1: TTimer;
    GorillaAssetsManager1: TGorillaAssetsManager;
    GorillaPhongMaterialSource1: TGorillaPhongMaterialSource;
    GorillaSphere1: TGorillaSphere;
    GorillaPhysicsSystem1: TGorillaPhysicsSystem;
    GorillaInputController1: TGorillaInputController;
    GorillaModel1: TGorillaModel;
    GorillaThirdPersonController1: TGorillaThirdPersonController;
    GorillaPhysicsCharacterController1: TGorillaPhysicsCharacterController;
    GorillaAnimationController1: TGorillaAnimationController;
    CheckBox1: TCheckBox;
    Label1: TLabel;
    Timer2: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure GorillaPhysicsCharacterController1Update(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure GorillaThirdPersonController1CustomAction(ASender: TObject;
      const AKind: TGorillaCharacterControllerHotKey;
      const AHotKey: TGorillaHotKeyItem;
      const AStates: TGorillaCharacterControllerStates;
      const AMode: TGorillaInputMode);
    procedure Timer2Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FPackage    : TGorillaAssetsPackage;
    FPhysThread : TQ3SimulationThread;

    function GetAssetsPath() : String;
    procedure DoCreateCharacter();
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.IOUtils,
  Gorilla.DAE.Loader,
  Gorilla.Animation;

procedure TForm1.FormCreate(Sender: TObject);
begin
  // load the character model and animation
  // setup character / animation control
  DoCreateCharacter();

  // finally activate physics computation
  GorillaPhysicsSystem1.Active := true;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  GorillaPhysicsSystem1.Active := false;
{$IFDEF MSWINDOWS}
  Application.ProcessMessages();
{$ENDIF}
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  Timer2.Enabled := true;
  GorillaCamera1.Position.Y := -7.5;
  GorillaCamera1.Target := GorillaModel1;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var LPos : TPoint3D;
begin
  LPos := TPoint3D(GorillaModel1.AbsolutePosition);
  Self.Caption := Format('Character = (%n; %n; %n) @%n FPS',
    [LPos.X, LPos.Y, LPos.Z, GorillaViewport1.FPS]);

  GorillaViewport1.Invalidate();
end;

procedure TForm1.Timer2Timer(Sender: TObject);
begin
  Timer2.Enabled := false;
  Label1.Text := '[W,A,S,D] - Move, [SHIFT] - Run';
  GorillaInputController1.Enabled := true;
end;

procedure TForm1.CheckBox1Change(Sender: TObject);
begin
  GorillaPhysicsSystem1.RenderColliders := CheckBox1.IsChecked;
end;

procedure TForm1.GorillaPhysicsCharacterController1Update(Sender: TObject);
begin
  GorillaViewport1.Invalidate();
end;

procedure TForm1.GorillaThirdPersonController1CustomAction(ASender: TObject;
  const AKind: TGorillaCharacterControllerHotKey;
  const AHotKey: TGorillaHotKeyItem;
  const AStates: TGorillaCharacterControllerStates;
  const AMode: TGorillaInputMode);
begin
  case AKind of
    // Check if key [E] was pressed to hide or show colliders
    TGorillaCharacterControllerHotKey.KeyboardCustomAction2 :
      begin
        CheckBox1.IsChecked := not CheckBox1.IsChecked;
      end;
  end;
end;

function TForm1.GetAssetsPath() : String;
begin
{$IFDEF MSWINDOWS}
  Result := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0)));
{$ENDIF}
{$IFDEF ANDROID}
  Result := IncludeTrailingPathDelimiter(TPath.GetHomePath());
{$ENDIF}

  Result := Result + IncludeTrailingPathDelimiter('assets');
end;

procedure TForm1.DoCreateCharacter();
var LPath : String;
    LAnim : TGorillaAnimation;
    LLay  : TGorillaAnimationTransitionLayer;
    LAnimTr : TGorillaAnimationTransition;
begin
  // create a dummy package for model and textures
  FPackage := GorillaAssetsManager1.AddEmptyPackage('Cache');

  // set character material to color-only
  GorillaPhongMaterialSource1.UseTexturing := false;

  // load character model from DAE file
  LPath := GetAssetsPath();
  GorillaModel1.LoadFromFile(FPackage, LPath + CHARACTER_MODEL, []);
  LAnim := GorillaModel1.AddAnimationFromFile(LPath + CHARACTER_IDLE, []);
  GorillaModel1.AddAnimationFromFile(LPath + CHARACTER_RUN, []);

  // set a material, because this model doesn't has one
  GorillaModel1.Meshes[0].MaterialSource := GorillaPhongMaterialSource1;
  GorillaModel1.SetHitTestValue(false);
  // we always want to see our character, so we disable frustum culling check
  // nevertheless this should not be necessary
  GorillaModel1.FrustumCullingCheck := false;

  // start the idle animation
  LAnim.Start();

  /// setup animation controller with layers
  GorillaAnimationController1.DefaultAnimation := CHARACTER_IDLE;

  /// transitions for automatically switching to running animation
  LLay := GorillaAnimationController1.AddLayer('Layer0');
  LLay.AddTransition('idle > run-forward', CHARACTER_IDLE, CHARACTER_RUN, true,
    TGorillaCharacterControllerHotKey.KeyboardMoveForward,
    [TGorillaCharacterControllerState.fpMoving],
    TGorillaAnimationTransitionMode.tmImmediatly);
  LLay.AddTransition('idle > run-backward', CHARACTER_IDLE, CHARACTER_RUN, true,
    TGorillaCharacterControllerHotKey.KeyboardMoveBackward,
    [TGorillaCharacterControllerState.fpMoving],
    TGorillaAnimationTransitionMode.tmImmediatly);
  LLay.AddTransition('idle > run-left', CHARACTER_IDLE, CHARACTER_RUN, true,
    TGorillaCharacterControllerHotKey.KeyboardMoveLeft,
    [TGorillaCharacterControllerState.fpMoving],
    TGorillaAnimationTransitionMode.tmImmediatly);
  LLay.AddTransition('idle > run-right', CHARACTER_IDLE, CHARACTER_RUN, true,
    TGorillaCharacterControllerHotKey.KeyboardMoveRight,
    [TGorillaCharacterControllerState.fpMoving],
    TGorillaAnimationTransitionMode.tmImmediatly);

  /// transitions for automatically switching back to idle animation
  LLay := GorillaAnimationController1.AddLayer('Layer1');
  LLay.AddTransition('run-forward > idle', CHARACTER_RUN, CHARACTER_IDLE, true,
    TGorillaCharacterControllerHotKey.UnknownHotKey,
    [TGorillaCharacterControllerState.fpIdle],
    TGorillaAnimationTransitionMode.tmImmediatly);
  LLay.AddTransition('run-backward > idle', CHARACTER_RUN, CHARACTER_IDLE, true,
    TGorillaCharacterControllerHotKey.UnknownHotKey,
    [TGorillaCharacterControllerState.fpIdle],
    TGorillaAnimationTransitionMode.tmImmediatly);
  LLay.AddTransition('run-left > idle', CHARACTER_RUN, CHARACTER_IDLE, true,
    TGorillaCharacterControllerHotKey.UnknownHotKey,
    [TGorillaCharacterControllerState.fpIdle],
    TGorillaAnimationTransitionMode.tmImmediatly);
  LLay.AddTransition('run-right > idle', CHARACTER_RUN, CHARACTER_IDLE, true,
    TGorillaCharacterControllerHotKey.UnknownHotKey,
    [TGorillaCharacterControllerState.fpIdle],
    TGorillaAnimationTransitionMode.tmImmediatly);
end;

end.
