unit Unit3;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Types3D,
  System.Math.Vectors, FMX.Controls3D, Gorilla.Light, FMX.MaterialSources,
  Gorilla.Material.Default, Gorilla.Material.Lambert, Gorilla.Plane,
  Gorilla.Control, Gorilla.Transform, Gorilla.Mesh, Gorilla.Cube,
  Gorilla.SkyBox, Gorilla.Viewport, Gorilla.Material.Particle,
  Gorilla.Particle.Influencer, Gorilla.Particle.Emitter;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaSkyBox1: TGorillaSkyBox;
    GorillaPlane1: TGorillaPlane;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    GorillaLight1: TGorillaLight;
    GorillaParticleEmitter1: TGorillaParticleEmitter;
    GorillaParticleMaterialSource1: TGorillaParticleMaterialSource;
    procedure FormShow(Sender: TObject);
    procedure GorillaParticleEmitter1Process(ASender: TObject;
      const AElapsedTime: Int64; const ADeltaTime: Double);
  private
    FPath : TGorillaTrailedParticleInfluencer;
    FRandomColor : TGorillaRandomColorParticleInfluencer;
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.FormShow(Sender: TObject);
const
  PathSize = 5;
  YMove = -0.3;
  Wideness = 0.4;

var LLevels : Integer;
    LScale : Single;
begin
  // Optimize scene navigation
  with Gorillaviewport1.GetDesignCameraController() do
  begin
    Interval := 1;
    IntensityZoom := 10;
    ShiftIntensity := 5;
    IntensityRotate := 10;
  end;

  // Activate emissive color computation
  GorillaViewport1.EmissiveBlur := 3;

  // Particles are spawn randomly
  // Create a path the particles are following from their origin
  FPath := TGorillaTrailedParticleInfluencer.Create(GorillaParticleEmitter1);
  FPath.Emitter := GorillaParticleEmitter1;
  FPath.Path.Clear();
  FPath.Path.MoveTo(TPoint3D.Create(0, 0, 0));

  // Create a spiral path particles are moving along
  LScale := Wideness;
  for LLevels := 1 to 5 do
  begin
    if LLevels = 1 then
    begin
      FPath.Path.LineToRel(Point3D(0, YMove, LScale * PathSize));
      FPath.Path.LineToRel(Point3D(-LScale * PathSize, YMove, 0));
      LScale := LScale + Wideness;
    end
    else
    begin
      LScale := LScale + Wideness;
      FPath.Path.LineToRel(Point3D(0, YMove, LScale * PathSize));
      FPath.Path.LineToRel(Point3D(-LScale * PathSize, YMove, 0));
      LScale := LScale + Wideness;
    end;

    FPath.Path.LineToRel(Point3D(0, YMove, -LScale * PathSize));
    FPath.Path.LineToRel(Point3D(LScale * PathSize, YMove, 0));
  end;

  FPath.Enabled := true;

  // Set the particle origin to the start of the path
  GorillaParticleEmitter1.ParticlePosition.RangeX.Preset(0, 0, 1, false);
  GorillaParticleEmitter1.ParticlePosition.RangeZ.Preset(0, 0, 1, false);
  GorillaParticleEmitter1.ParticlePosition.Direction.Point := Point3D(1, 1, -1);

  // Set the particle lifetime to a suitable value (10 sec.) to control the speed.
  // After they expired, they will be respawn
  GorillaParticleEmitter1.ParticleLifeTime.Preset(30, 30, 1, false);

  // Link the design time camera to the emitter for correct particle sorting
  GorillaParticleEmitter1.Camera := GorillaViewport1.GetDesignCamera();

  // Activate the particle emitter
  GorillaParticleEmitter1.Start();
end;

procedure TForm1.GorillaParticleEmitter1Process(ASender: TObject;
  const AElapsedTime: Int64; const ADeltaTime: Double);
begin
  Self.Caption := Format('Gorilla3D - Emissive Particles (FPS: %n, Particles: %d)',
    [GorillaViewport1.FPS, GorillaParticleEmitter1.Emitted]);
end;

end.
