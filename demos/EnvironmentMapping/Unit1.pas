unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Gorilla.Viewport, FMX.Types3D, System.Math.Vectors, FMX.Controls3D,
  Gorilla.Light, Gorilla.Control, Gorilla.Transform, Gorilla.Mesh, Gorilla.Cube,
  FMX.MaterialSources, Gorilla.Material.Default, Gorilla.Material.Blinn,
  Gorilla.SkyBox, Gorilla.Cone, Gorilla.Controller,
  Gorilla.Controller.Passes.Environment, FMX.Ani, Gorilla.Material.Phong,
  Gorilla.Sphere, Gorilla.Material.Lambert, Gorilla.Torus, Gorilla.Capsule,
  Gorilla.Material.RimLighting;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaCube1: TGorillaCube;
    GorillaLight1: TGorillaLight;
    GorillaBlinnMaterialSource1: TGorillaBlinnMaterialSource;
    GorillaCone1: TGorillaCone;
    GorillaBlinnMaterialSource2: TGorillaBlinnMaterialSource;
    GorillaRenderPassEnvironment1: TGorillaRenderPassEnvironment;
    FloatAnimation1: TFloatAnimation;
    GorillaSphere1: TGorillaSphere;
    GorillaPhongMaterialSource1: TGorillaPhongMaterialSource;
    FloatAnimation2: TFloatAnimation;
    GorillaTorus1: TGorillaTorus;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    FloatAnimation4: TFloatAnimation;
    GorillaCapsule1: TGorillaCapsule;
    GorillaRimLightingMaterialSource1: TGorillaRimLightingMaterialSource;
    FloatAnimation3: TFloatAnimation;
    GorillaSkyBox1: TGorillaSkyBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.FormCreate(Sender: TObject);
begin
  // attach light source to camera, to always have light
  GorillaLight1.Parent := GorillaViewport1.GetDesignCamera();

  // some designtime settings still getting resetted on loading at runtime
  // we increase the environment map for better quality
  GorillaRenderPassEnvironment1.MapSize := 512;

  // because we have moving objects in our scene, we can not only render the
  // environment once, we have to update the map on each render cycle
  GorillaRenderPassEnvironment1.IsDynamic := true;

  // the reflecting cube has a size of 2.5, we want to render environment
  // for each side not at the center of the cube, but from side-offset
//  GorillaRenderPassEnvironment1.SetCenterOffsets(1.25);

  // link renderpass to the reflecting cube
  GorillaRenderPassEnvironment1.Target := GorillaCube1;

  // finally enable the environment render pass
  GorillaRenderPassEnvironment1.Enabled := true;
end;

end.
