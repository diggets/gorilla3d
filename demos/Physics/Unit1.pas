unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Gorilla.Viewport, System.Math.Vectors, FMX.Types3D, FMX.Objects3D,
  FMX.Controls3D, Gorilla.Control, Gorilla.Mesh, Gorilla.Plane, Gorilla.Cube,
  Gorilla.Sphere, Gorilla.Material.Phong, Gorilla.Material.Default,
  Gorilla.Material.Lambert, FMX.MaterialSources, Gorilla.Physics, Gorilla.DefTypes,
  FMX.Controls.Presentation, FMX.StdCtrls, Gorilla.Transform,
  Gorilla.Material.POM, Gorilla.Material.PBR, Gorilla.Material.Blinn;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    Light1: TLight;
    GorillaSphere1: TGorillaSphere;
    GorillaCube1: TGorillaCube;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    GorillaPhysicsSystem1: TGorillaPhysicsSystem;
    Timer1: TTimer;
    Label1: TLabel;
    Button1: TButton;
    GorillaBlinnMaterialSource1: TGorillaBlinnMaterialSource;
    GorillaCube2: TGorillaCube;
    GorillaBlinnMaterialSource2: TGorillaBlinnMaterialSource;
    procedure Timer1Timer(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.Button1Click(Sender: TObject);
begin
  // we activate the physics system
  GorillaPhysicsSystem1.Active := true;
  // and we need to activate the physics timer for constant updates
  Timer1.Enabled := true;
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word; var KeyChar: Char;
  Shift: TShiftState);
var LImpulse : TPoint3D;
begin
  case Key of
    vkLeft  : LImpulse := Point3D(-1, 0, 0);
    vkRight : LImpulse := Point3D(1, 0, 0);
    vkUp    : LImpulse := Point3D(0, -10, 0);
    vkDown  : LImpulse := Point3D(0, 5, 0);
    else LImpulse := TPoint3D.Zero;
  end;

  GorillaPhysicsSystem1.RemoteBodyImpulse(GorillaSphere1, LImpulse);
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  if not GorillaPhysicsSystem1.Async then
    GorillaPhysicsSystem1.Step();

  GorillaViewport1.Invalidate();
end;

end.
