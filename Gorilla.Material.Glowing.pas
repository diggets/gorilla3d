{*******************************************************}
{                                                       }
{           diggets.com Gorilla3D Framework             }
{                                                       }
{          Copyright(c) 2017-2018 Eric Berger           }
{              All rights reserved                      }
{                                                       }
{   Copyright and license exceptions noted in source    }
{                                                       }
{*******************************************************}

{*******************************************************}
{                                                       }
{  SHADER CODE:                                         }
{  Seven am 2013-04-26                                  }
{  https://www.shadertoy.com/view/lsXGWn                }
{                                                       }
{*******************************************************}

unit Gorilla.Material.Glowing;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Messaging,
  System.Math.Vectors,
  System.Types,
  System.UITypes,
  FMX.Types,
  FMX.Types3D,
  FMX.Graphics,
  FMX.Materials,
  FMX.MaterialSources,
  Gorilla.Controller,
  Gorilla.Context.GLES;

type
  {$M+}
  TGorillaGlowingMaterialSource = class;

  /// <summary>
  /// Custom glowing material.
  /// </summary>
  /// <remarks>
  /// Shader code by : Seven am 2013-04-26
  /// https://www.shadertoy.com/view/lsXGWn
  /// </remarks>
  TGorillaGlowingMaterial = class(TCustomMaterial)
  private
    FSource       : TGorillaGlowingMaterialSource;
    FTimerService : IFMXTimerService;
    FStartTime    : Double;
    FRenderTime   : Double;
    FLastTime     : Double;

    [Weak] FTexture : TTexture;

    procedure SetTexture(const Value: TTexture);

  protected

    procedure DoApply(const Context: TContext3D); override;
    procedure DoInitialize(); override;

  public
    constructor Create(const ASource : TGorillaGlowingMaterialSource); reintroduce; virtual;
    destructor Destroy(); override;

  published
    /// <summary>
    /// A main texture used during material computation.
    /// </summary>
    /// <remarks>
    /// By default the texture is not set.
    /// </remarks>
    property Texture : TTexture read FTexture write SetTexture;
  end;

  /// <summary>
  /// Glowing texture material source, which simulates a glowing effect.
  /// This material source ignores light sources and shading.
  /// As input a texture is needed.
  /// </summary>
  /// <remarks>
  /// Shader code by : Seven am 2013-04-26
  /// https://www.shadertoy.com/view/lsXGWn
  /// </remarks>
  TGorillaGlowingMaterialSource = class(TMaterialSource)
  protected
    FTexture : TBitmap;
    FContextResetOvrId : Integer;

    function CreateMaterial() : TMaterial; override;
    procedure DoTextureChanged(Sender: TObject);
    procedure ContextResetHandlerOverride(const Sender: TObject;
      const Msg: TMessage);

    procedure SetTexture(const Value: TBitmap);

  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;

  published
    /// <summary>
    /// Get or set render texture from / to material.
    /// </summary>
    /// <remarks>
    /// By default the texture is not set.
    /// </remarks>
    property Texture : TBitmap read FTexture write SetTexture;
  end;

implementation

uses
  System.Math,
  System.IOUtils,
  FMX.Platform;

  ResourceString GORILLA_GLOWING_VS =
    'uniform mat4 _ModelViewProjMatrix;' +

    'attribute vec3 a_Position;' +
    'attribute vec2 a_TexCoord0;' +

    'varying vec4 v_VertexPosition;' +
    'varying vec2 v_TextureCoord;' +

    'void main( void ){' +
    '  v_TextureCoord = a_TexCoord0;' +
    '  v_VertexPosition = _ModelViewProjMatrix * vec4(a_Position, 1.0);' +

    '  gl_Position = v_VertexPosition;' +

    '}';

  ResourceString GORILLA_GLOWING_FS =
    'uniform sampler2D _texture0;' +
    'uniform float _Time;' +

    'varying vec4 v_VertexPosition;' +
    'varying vec2 v_TextureCoord;' +

    'const float blurSize = 1.0 / 512.0;' +
    'const float intensity = 0.35;' +

    'void main(){' +
    '  vec4 sum = vec4(0);' +
    '  vec2 fragCoord = gl_FragCoord.xy;' +
    '  vec2 texcoord = v_TextureCoord;' +
    '  int j;' +
    '  int i;' +

    '  sum += texture(_texture0, vec2(texcoord.x - 4.0*blurSize, texcoord.y)) * 0.05;' +
    '  sum += texture(_texture0, vec2(texcoord.x - 3.0*blurSize, texcoord.y)) * 0.09;' +
    '  sum += texture(_texture0, vec2(texcoord.x - 2.0*blurSize, texcoord.y)) * 0.12;' +
    '  sum += texture(_texture0, vec2(texcoord.x - blurSize, texcoord.y)) * 0.15;' +
    '  sum += texture(_texture0, vec2(texcoord.x, texcoord.y)) * 0.16;' +
    '  sum += texture(_texture0, vec2(texcoord.x + blurSize, texcoord.y)) * 0.15;' +
    '  sum += texture(_texture0, vec2(texcoord.x + 2.0*blurSize, texcoord.y)) * 0.12;' +
    '  sum += texture(_texture0, vec2(texcoord.x + 3.0*blurSize, texcoord.y)) * 0.09;' +
    '  sum += texture(_texture0, vec2(texcoord.x + 4.0*blurSize, texcoord.y)) * 0.05;' +

      // blur in y (vertical)
      // take nine samples, with the distance blurSize between them
    '  sum += texture(_texture0, vec2(texcoord.x, texcoord.y - 4.0*blurSize)) * 0.05;' +
    '  sum += texture(_texture0, vec2(texcoord.x, texcoord.y - 3.0*blurSize)) * 0.09;' +
    '  sum += texture(_texture0, vec2(texcoord.x, texcoord.y - 2.0*blurSize)) * 0.12;' +
    '  sum += texture(_texture0, vec2(texcoord.x, texcoord.y - blurSize)) * 0.15;' +
    '  sum += texture(_texture0, vec2(texcoord.x, texcoord.y)) * 0.16;' +
    '  sum += texture(_texture0, vec2(texcoord.x, texcoord.y + blurSize)) * 0.15;' +
    '  sum += texture(_texture0, vec2(texcoord.x, texcoord.y + 2.0*blurSize)) * 0.12;' +
    '  sum += texture(_texture0, vec2(texcoord.x, texcoord.y + 3.0*blurSize)) * 0.09;' +
    '  sum += texture(_texture0, vec2(texcoord.x, texcoord.y + 4.0*blurSize)) * 0.05;' +

      // increase blur with intensity!
    '  if(sin(_Time) > 0.0)' +
    '    gl_FragColor = sum * sin(_Time)+ texture(_texture0, texcoord);' +
    '  else' +
    '    gl_FragColor = sum * -sin(_Time)+ texture(_texture0, texcoord);' +
    '}';

{ TGorillaGlowingMaterial }

constructor TGorillaGlowingMaterial.Create(const ASource : TGorillaGlowingMaterialSource);
begin
  inherited Create();

  FSource := ASource;

  if FTimerService = nil then
    if not TPlatformServices.Current.SupportsPlatformService(IFMXTimerService, FTimerService) then
      raise EUnsupportedPlatformService.Create('IFMXTimerService');

  FLastTime   := FTimerService.GetTick();
  FRenderTime := FLastTime;
  FStartTime  := FLastTime;
end;

destructor TGorillaGlowingMaterial.Destroy();
begin
  inherited;
end;

procedure TGorillaGlowingMaterial.DoInitialize();
var LOGLBytes : TArray<Byte>;
begin
  ///
  /// VertexShader
  ///
  LOGLBytes := TEncoding.ASCII.GetBytes(GORILLA_GLOWING_VS);
  FVertexShader := TShaderManager.RegisterShaderFromData('glowing.fvs', TContextShaderKind.VertexShader, '', [
    TContextShaderSource.Create(TContextShaderArch.GLSL,
      LOGLBytes,
      [
      TContextShaderVariable.Create('ModelViewProjMatrix', TContextShaderVariableKind.Matrix, 0, 4),
      TContextShaderVariable.Create('ModelViewMatrix', TContextShaderVariableKind.Matrix, 1, 4)
      ]
    )
  ]);


  ///
  /// PixelShader / FragmentShader
  ///
  LOGLBytes := TEncoding.ASCII.GetBytes(GORILLA_GLOWING_FS);
  FPixelShader := TShaderManager.RegisterShaderFromData('glowing.fps', TContextShaderKind.PixelShader, '', [
    TContextShaderSource.Create(TContextShaderArch.GLSL,
      LOGLBytes,
      [
      TContextShaderVariable.Create('texture0', TContextShaderVariableKind.Texture, 0, 0),
      TContextShaderVariable.Create('Time', TContextShaderVariableKind.Float, 1, 1)
      ]
    )
  ]);
end;

procedure TGorillaGlowingMaterial.SetTexture(const Value: TTexture);
begin
  if FTexture <> Value then
  begin
    FTexture := Value;
  end;
  // The change notification is performed outside the if, because the content of texture could be changed
  DoChange;
end;


procedure TGorillaGlowingMaterial.DoApply(const Context: TContext3D);
var
  LCtx        : TCustomContextOpenGL;
  LTime       : Single;
  LResolution : TPointF;
begin
  LCtx := TCustomContextOpenGL(Context);

  LResolution.X := Context.Width;
  LResolution.Y := Context.Height;

  FLastTime   := FRenderTime;
  FRenderTime := FTimerService.GetTick();

  // set shaders
  LCtx.SetShaders(FVertexShader, FPixelShader);

  // textures
  Context.SetShaderVariable('texture0', FTexture);

  TCustomContextOpenGL(Context).SetShaderVariable('ModelViewProjMatrix', LCtx.CurrentModelViewProjectionMatrix, true);
  TCustomContextOpenGL(Context).SetShaderVariable('ModelViewMatrix', LCtx.CurrentMatrix, true);

  LTime := (FRenderTime - FStartTime);

  Context.SetShaderVariable('Time', [Vector3D(LTime, 0, 0, 0)]);
  Context.SetShaderVariable('Resolution', [Vector3D(LResolution.X, LResolution.Y, 0, 0)]);
end;











{ TGorillaGlowingMaterialSource }

constructor TGorillaGlowingMaterialSource.Create(AOwner: TComponent);
begin
  inherited;

  FTexture := TTextureBitmap.Create();
  FTexture.OnChange := DoTextureChanged;

  FContextResetOvrId := TMessageManager.DefaultManager.SubscribeToMessage(TContextResetMessage, ContextResetHandlerOverride);
end;

destructor TGorillaGlowingMaterialSource.Destroy();
begin
  TMessageManager.DefaultManager.Unsubscribe(TContextResetMessage, FContextResetOvrId);

  FreeAndNil(FTexture);

  inherited;
end;

procedure TGorillaGlowingMaterialSource.ContextResetHandlerOverride(
  const Sender: TObject; const Msg: TMessage);
begin
  DoTextureChanged(Self);
end;

procedure TGorillaGlowingMaterialSource.SetTexture(const Value: TBitmap);
begin
  FTexture.Assign(Value);
end;


procedure TGorillaGlowingMaterialSource.DoTextureChanged(Sender: TObject);
begin
  if not FTexture.IsEmpty then
    TGorillaGlowingMaterial(Material).Texture := TTextureBitmap(FTexture).Texture;
end;

function TGorillaGlowingMaterialSource.CreateMaterial() : TMaterial;
begin
  Result := TGorillaGlowingMaterial.Create(Self);
end;

initialization
  RegisterFmxClasses([TGorillaGlowingMaterialSource]);

end.