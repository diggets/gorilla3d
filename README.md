## Gorilla3D (Developer-Edition) Addon Framework for Delphi Firemonkey 3D

---

## Features
By default Delphi Firemonkey 3D library is very limited. By Gorilla3D you can extend the 3D functionality.

We provide a lot of features and components:

* Skin-, Skeleton- und Vertexanimations for 3D Meshes (Key-Animations)
* Integrated and fast Q3 Physics Engine (with collision detection)
* Materials: textures, normal-mapping, vertex-color, phong-, blinn-, lambert-materials, layered materials, splatmaps, terrain-material, parallax occlusion mapping (POM), physically based rendering (PBR)
* Materials based on a expandable DefaultMaterial
* Weighted blended order-independent transparency rendering
* Global-Illumination rendering with shadowing, reflections and light-scattering (Beta)
* Emissive Blurring / Bloom effect
* Environment mapping component to combine with default materials
* Logical model management (independent from FireMonkey tree), to manage a large amount of meshes and to instanciate separatly
* Flexible particle-system with influencer-classes
* Component based render pass implementation
* AssetsManager with packaging system
* FMOD AudioManager for professional audio playback
* Scripting in Delphi syntax
* Prefab management
* Design-Time editors: Package-Manager, Dialogue-Designer, Inventory-Manager, Animation-Transition Manager, Sketchfab-Plugin, Terrain-Generation + Planting, ...
* AssetsStore with easy & free embeddable prefabs
* SkyBox-support with CubeMap
* Volume rendering with raytracing
* Fog rendering (linear, exp, exp2)
* Water Surface rendering
* Bokeh/Depth-Of-Field
* Shadow- and Variance-Shadow-Mapping (experimental)
* Terrain rendering: from height-maps and procedural algorithms (Diamond-Square, Perlin-Noise, ...)
* New Point3D-, Quaternion-, Transformationsmatrix- und VertexKey-Animationen
* Wide range of components for: particles, physics, audio, terrain, skybox, characters, dialogue-creation, inventory, input controlling, first- & third-person controllers, ...
* Compatible with existing FireMonkey components
* OpenGL 4.3+ / OpenGLES 3.1+
* Platform-independent: WIN32, WIN64, ANDROID, ANDROID64 [currently not supported: iOS, MACOS, LINUX]
* Formate: DAE, FBX, GLTF/GLB, OBJ, STL, PLY, SKP, X3D, Babylon, G3D

---
## Developer Edition Download

Because we are also a small company, we know how hard and expensive it is to develop an application,
without knowing if it's ever going to be published.
Therefor we provide the free downloadable developer edition here:
https://gorilla3d.de/files/?dir=packages

Installation-Guide: https://docs.gorilla3d.de/1.1.0/installation

Perfectly suited for small indie developers at the beginning of their project. Start to develop your game or app.

When you're ready to publish/release, you need to buy a commercial license!

* visual and hardcoded watermarks
* max. 5 developers in your team
* allowed only for development purposes
* not allowed to release or publish anything
* no limitation to the number of projects

---

## Install
* https://docs.gorilla3d.de/1.1.0/installation

---

## Purchase a license

Visit our online shop further details: https://www.gorilla3d.de/purchase/

And read more about our licensing here: https://docs.gorilla3d.de/1.1.0/pricing

If you have any further questions, feel free to contact us: support@gorilla3d.de

---

## Links
* Visit us at: https://gorilla3d.de/
* Report bugs at: https://dev.gorilla3d.de/
* Online documentation: https://docs.gorilla3d.de/

---
## Social Media

Follow us at:
1. Instagram: https://www.instagram.com/gorilla3dfmx/

2. Facebook: https://www.facebook.com/gorilla3d

3. Youtube: https://www.youtube.com/@gorilla3dfmx

4. Telegram-Stream: https://t.me/gorilla3d

5. Telegram-Chat: https://t.me/Gorilla3dGroup

6. Discord: https://discord.gg/VC2mEU8aGY
