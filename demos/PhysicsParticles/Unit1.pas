unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light, Gorilla.DefTypes,
  Gorilla.Camera, Gorilla.Viewport, FMX.Objects3D, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Terrain, FMX.MaterialSources, Gorilla.Material.Default,
  Gorilla.Material.Lambert, Gorilla.Particle.Influencer,
  Gorilla.Particle.Emitter, Gorilla.Material.Particle, Gorilla.Physics,
  FMX.Controls.Presentation, FMX.StdCtrls, Gorilla.Cube, Gorilla.Sphere,
  Gorilla.Material.Blinn, Gorilla.Transform;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    Dummy1: TDummy;
    GorillaTerrain1: TGorillaTerrain;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    Timer1: TTimer;
    GorillaParticleEmitter1: TGorillaParticleEmitter;
    GorillaParticleMaterialSource1: TGorillaParticleMaterialSource;
    GorillaPhysicsSystem1: TGorillaPhysicsSystem;
    Button1: TButton;
    GorillaPhysicsInfluencer1: TGorillaPhysicsInfluencer;
    GorillaSphere1: TGorillaSphere;
    GorillaColoredParticleInfluencer1: TGorillaColoredParticleInfluencer;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.TypInfo, Gorilla.Physics.Types;

procedure TForm1.Button1Click(Sender: TObject);
begin
  if GorillaParticleEmitter1.Active then
  begin
    // deactivate particles and physics
    Timer1.Enabled := false;
    GorillaPhysicsSystem1.Active := false;
    GorillaParticleEmitter1.Stop();
    GorillaParticleEmitter1.Clear();
    Button1.Text := 'Start';
  end
  else
  begin
    // activate particles and physics
    GorillaPhysicsSystem1.Active := true;
    Timer1.Enabled := true;
    GorillaParticleEmitter1.Start();
    Button1.Text := 'Stop';
  end;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // stop physics particles computation (making problems sometimes!)
  Timer1.Enabled := false;

  // stop emitter
  GorillaParticleEmitter1.Stop();
  GorillaParticleEmitter1.Clear();

  // finally stop physics engine
  GorillaPhysicsSystem1.Active := false;
end;

procedure TForm1.FormCreate(Sender: TObject);
var LPrefab : TGorillaColliderSettings;
    LVecPre : TParticleVectorPreset;
begin
  Randomize();

  ///
  /// TERRAIN
  ///

  // disable mouse interaction for terrain
  GorillaTerrain1.SetHitTestValue(false);

  // add a physics collider for terrain
  LPrefab := TGorillaColliderSettings.Create(TGorillaPhysicsBodyType.eStaticBody);
  GorillaPhysicsSystem1.AddTerrainCollider(GorillaTerrain1, LPrefab);

  ///
  /// SPHERE
  ///

  // add a collider for the sphere
  if GorillaSphere1.Visible then
  begin
    LPrefab := TGorillaColliderSettings.Create(TGorillaPhysicsBodyType.eStaticBody);
    GorillaPhysicsSystem1.AddSphereCollider(GorillaSphere1, LPrefab);
  end;

  ///
  /// PARTICLE EMITTER
  ///

  // setup particle emitter (position, velocity, lifetime, size, ... of particles)
  LVecPre := GorillaParticleEmitter1.ParticlePosition;
  LVecPre.RangeX.Preset(0, 25, 10, false);
  LVecPre.RangeY.Preset(25, 25, 1, false);
  LVecPre.RangeZ.Preset(0, 25, 10, false);
  LVecPre.Direction.Point := Point3D(1, -1, 1);
  LVecPre.Axis := [TAllowedAxis.axX, TAllowedAxis.axY, TAllowedAxis.axZ];

  LVecPre := GorillaParticleEmitter1.ParticleVelocity;
  LVecPre.RangeX.Preset(0, 0, 1, false);
  LVecPre.RangeY.Preset(100, 1000, 1000, false);
  LVecPre.RangeZ.Preset(0, 0, 1, false);
  LVecPre.Direction.Point := Point3D(0, 9.81, 0);
  LVecPre.Axis := [TAllowedAxis.axX, TAllowedAxis.axY, TAllowedAxis.axZ];

  GorillaParticleEmitter1.ParticleLifeTime.Preset(15, 100, 10, false);
  GorillaParticleEmitter1.ParticleSize.Preset(2, 4, 1, false);
  GorillaParticleEmitter1.ParticleWeight.Preset(1, 1, 1, false);

  // disable default linear influencer (this added to an emitter by default)
  GorillaParticleEmitter1.Influencers[0].Enabled := false;

  // configure the colored influencer for particles
  GorillaColoredParticleInfluencer1.StartColor :=
    TAlphaColorF.Create(Random(255)/255, Random(255)/255, Random(255)/255, 1).ToAlphaColor();
  GorillaColoredParticleInfluencer1.EndColor :=
    TAlphaColorF.Create(Random(255)/255, Random(255)/255, Random(255)/255, 1).ToAlphaColor();
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  GorillaViewport1.BeginUpdate();
  try
    // update render info in header
    Self.Caption := Format('Gorilla 3D - Physics Particle Demo - %n FPS (Particles: %d)',
      [GorillaViewport1.FPS, GorillaParticleEmitter1.Emitted]);

    // rotate camera around center of the scene
    Dummy1.RotationAngle.Y := Dummy1.RotationAngle.Y + 1;

    // update physics
    if not GorillaPhysicsSystem1.Async then
      GorillaPhysicsSystem1.Step();
  finally
    GorillaViewport1.EndUpdate();
  end;
end;

end.
