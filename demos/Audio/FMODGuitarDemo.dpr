program FMODGuitarDemo;

uses
  System.StartUpCopy,
  FMX.Forms,
  Gorilla.Audio.FMOD.System,
  Unit2 in 'Unit2.pas' {Form4};

{$R *.res}

begin
  TGorillaFMODSystem.MaxSoftwareChannels := 1024;

  Application.Initialize;
  Application.CreateForm(TForm4, Form4);
  Application.Run;
end.
