unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Gorilla.Controller, Gorilla.Controller.Input, Gorilla.Viewport, Gorilla.DefTypes,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light,
  Gorilla.Camera, Gorilla.Control, Gorilla.Mesh, Gorilla.Plane,
  FMX.MaterialSources, Gorilla.Material.Default, Gorilla.Material.Lambert,
  Gorilla.Cube, FMX.Objects3D, Gorilla.Controller.Input.Character,
  Gorilla.Controller.Input.FirstPerson, Gorilla.Controller.Input.ThirdPerson,
  FMX.Controls.Presentation, FMX.StdCtrls, Gorilla.Transform;

type
  TForm2 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaInputController1: TGorillaInputController;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaPlane1: TGorillaPlane;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    GorillaThirdPersonController1: TGorillaThirdPersonController;
    GorillaCube1: TGorillaCube;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure GorillaThirdPersonController1Rotate(ASender: TObject;
      ACurrentRotation, ANewRotation: TQuaternion3D; ADelta: TPointF);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

procedure TForm2.FormShow(Sender: TObject);
begin
  GorillaInputController1.Enabled := true;
end;

procedure TForm2.GorillaThirdPersonController1Rotate(ASender: TObject;
  ACurrentRotation, ANewRotation: TQuaternion3D; ADelta: TPointF);
begin
  GorillaViewport1.Invalidate;
end;

end.
