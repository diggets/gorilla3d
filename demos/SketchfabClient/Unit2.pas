unit Unit2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.Edit, FMX.StdCtrls;

const
  INI_FILE  = 'credentials.ini';
  INI_LOGIN = 'LOGIN';

type
  TForm2 = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    Button1: TButton;
    PasswordEditButton1: TPasswordEditButton;
    Edit3: TEdit;
    Panel1: TPanel;
    Panel2: TPanel;
    Edit4: TEdit;
    PasswordEditButton2: TPasswordEditButton;
    Label1: TLabel;
    Label2: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    function GetConfigFilePath() : String;
    procedure SaveCredentials(AFile : String);
    procedure LoadCredentials(AFile : String);
    procedure ApplyCredentials(ADoSave : Boolean);
  public
    { Public-Deklarationen }
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

uses
  Unit1,
  System.IOUtils,
  System.IniFiles,
  Gorilla.Sketchfab.Loader;

procedure TForm2.FormCreate(Sender: TObject);
begin
  LoadCredentials(GetConfigFilePath());
end;

procedure TForm2.Button1Click(Sender: TObject);
begin
  ApplyCredentials(true);
{$IFDEF MSWINDOWS}
  ModalResult := mrOk;
{$ENDIF}
{$IFDEF ANDROID}
  Close;
{$ENDIF}
end;

procedure TForm2.ApplyCredentials(ADoSave : Boolean);
begin
  Form1.Username := Edit1.Text;
  Form1.Password := Edit2.Text;
  Form1.ClientId := Edit3.Text;
  Form1.ClientSecret := Edit4.Text;

  SaveCredentials(GetConfigFilePath());
end;

function TForm2.GetConfigFilePath() : String;
begin
{$IFDEF MSWINDOWS}
  Result := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0)));
{$ENDIF}
{$IFDEF ANDROID}
  Result := IncludeTrailingPathDelimiter(TPath.GetHomePath());
{$ENDIF}

  Result := Result + INI_FILE;
end;

procedure TForm2.LoadCredentials(AFile : String);
var LIni : TMemIniFile;
begin
  if not FileExists(AFile) then
    Exit;

  LIni := TMemIniFile.Create(AFile);
  try
    Edit1.Text := LIni.ReadString(INI_LOGIN, 'Username', '');
    Edit2.Text := LIni.ReadString(INI_LOGIN, 'Password', '');
    Edit3.Text := LIni.ReadString(INI_LOGIN, 'ClientId', '');
    Edit4.Text := LIni.ReadString(INI_LOGIN, 'ClientSecret', '');
  finally
    FreeAndNil(LIni);
  end;

  ApplyCredentials(false);
end;

procedure TForm2.SaveCredentials(AFile : String);
var LIni : TMemIniFile;
begin
  LIni := TMemIniFile.Create(AFile);
  try
    LIni.WriteString(INI_LOGIN, 'Username', Edit1.Text);
    LIni.WriteString(INI_LOGIN, 'Password', Edit2.Text);
    LIni.WriteString(INI_LOGIN, 'ClientId', Edit3.Text);
    LIni.WriteString(INI_LOGIN, 'ClientSecret', Edit4.Text);
    LIni.UpdateFile();
  finally
    FreeAndNil(LIni);
  end;
end;

end.
