unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Controls3D,
  Gorilla.Control, Gorilla.Transform, Gorilla.Mesh, Gorilla.Cube,
  Gorilla.SkyBox, Gorilla.Viewport, Gorilla.Model, FMX.Controls.Presentation,
  FMX.StdCtrls, FMX.MaterialSources, Gorilla.Material.Default,
  Gorilla.Material.Blinn, FMX.Colors, FMX.Types3D, System.Math.Vectors,
  Gorilla.Light;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaModel1: TGorillaModel;
    TrackBar1: TTrackBar;
    GorillaBlinnMaterialSource1: TGorillaBlinnMaterialSource;
    Label1: TLabel;
    TrackBar2: TTrackBar;
    Label2: TLabel;
    ComboColorBox1: TComboColorBox;
    GorillaLight1: TGorillaLight;
    GorillaSkyBox1: TGorillaSkyBox;
    procedure FormCreate(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure TrackBar2Change(Sender: TObject);
    procedure ComboColorBox1Change(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.Math;

procedure TForm1.FormCreate(Sender: TObject);
begin
  // activate emissive blurring for bloom effect and emissive color in our material
  TrackBar1.Value := 3;
  TrackBar2.Value := 0.25;

  // link our emissive material to all meshes inside the suzanne model
  GorillaModel1.ApplyMaterialSource(GorillaBlinnMaterialSource1);
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  // change the bloom effect intensity
  GorillaViewport1.EmissiveBlur := Floor(TrackBar1.Value);
end;

procedure TForm1.ComboColorBox1Change(Sender: TObject);
var LColor : TAlphaColorF;
begin
  // change the emissive color and intensity
  LColor := TAlphaColorF.Create(ComboColorBox1.Color);
  GorillaBlinnMaterialSource1.EmissiveF := TAlphaColorF.Create(
    LColor.R, LColor.G, LColor.B, // emissive color
    TrackBar2.Value // intensity of bloom
    );
end;

procedure TForm1.TrackBar2Change(Sender: TObject);
begin
  // update the emissive color intensity
  ComboColorBox1Change(ComboColorBox1);
end;

end.
