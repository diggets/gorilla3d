unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light,
  Gorilla.Camera, FMX.Objects3D, Gorilla.Viewport, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Plane, FMX.MaterialSources, Gorilla.Material.Default,
  Gorilla.Material.SplatMap, Gorilla.Prefab, Gorilla.Model, Gorilla.Transform,
  Gorilla.Group, FMX.Ani;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    Dummy1: TDummy;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaPlane1: TGorillaPlane;
    GorillaSplatMapMaterialSource1: TGorillaSplatMapMaterialSource;
    GrassTemplate: TGorillaModel;
    FloatAnimation1: TFloatAnimation;
    procedure FormCreate(Sender: TObject);
    procedure GorillaViewport1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure GorillaViewport1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure GorillaViewport1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Single);
  private
    FMove : Boolean;
    FLastPos : TPointF;
  public
    function SetupInstances() : Integer;
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.Math,
  Gorilla.Material.Types;

procedure TForm1.FormCreate(Sender: TObject);
var LTexture : Gorilla.Material.Types.TGorillaBitmapPoolEntry;
    LCount : Integer;
begin
  /// block plane and grass from mouse interaction
  GorillaPlane1.SetHitTestValue(false);
  GrassTemplate.SetHitTestValue(false);

  /// the model is not static by default: we can gain more performance by
  /// making it static and pushing vertexdata once to the gpu
  GrassTemplate.IsStatic := true;

  /// BUG: designtime tiling settings not available anymore at runtime
  /// This bug was fixed in 0.8.4.2284
//  LTexture := GorillaSplatMapMaterialSource1.Bitmaps.Items[0] as Gorilla.Material.Types.TGorillaBitmapPoolEntry;
//  LTexture.Tiling := PointF(4, 4);
//  LTexture := GorillaSplatMapMaterialSource1.Bitmaps.Items[1] as Gorilla.Material.Types.TGorillaBitmapPoolEntry;
//  LTexture.Tiling := PointF(8, 8);
//  LTexture := GorillaSplatMapMaterialSource1.Bitmaps.Items[2] as Gorilla.Material.Types.TGorillaBitmapPoolEntry;
//  LTexture.Tiling := PointF(1, 1);
//  LTexture := GorillaSplatMapMaterialSource1.Bitmaps.Items[3] as Gorilla.Material.Types.TGorillaBitmapPoolEntry;
//  LTexture.Tiling := PointF(1, 1);
//  LTexture := GorillaSplatMapMaterialSource1.Bitmaps.Items[4] as Gorilla.Material.Types.TGorillaBitmapPoolEntry;
//  LTexture.Tiling := PointF(1, 1);

  /// setting up instaces for grass
  LCount := SetupInstances();
  Self.Caption := Self.Caption + Format(' [Instances = %d]', [LCount]);
end;

//// INSTANCING ////////////////////////////////////////////////////////////////

function TForm1.SetupInstances() : Integer;
const GRASS_INV_SIZE = 10;

var LBmp       : TBitmap;
    LData      : TBitmapData;
    X, Z       : Integer;
    LMapInvSize: Single;
    LHalfX,
    LHalfZ     : Single;
    LPosX,
    LPosZ      : Single;
    LSize      : Single;
    LBaseTransf,
    LRotMat,
    LTransf    : TMatrix3D;
    LGrassMesh : TGorillaMesh;
    LPxClr     : TAlphaColor;
begin
  Result := 0;

  /// we generate instances for grass by the splatmap inside the plane material
  /// only where is red color we place a grass instance randomly
  LBmp := GorillaSplatMapMaterialSource1.Texture;
  if LBmp.Map(TMapAccess.Read, LData) then
  begin
    try
      LHalfX := LData.Width / 2;
      LHalfZ := LData.Height / 2;

      /// get the relation between texture-size and plane-size for correct
      /// adjustment
      LMapInvSize := LData.Width / GorillaPlane1.Width;

      /// get the mesh itself, of which we create instances
      LGrassMesh := GrassTemplate.Meshes[0];

      /// setup a general matrix transformation as basis for all instances
      LBaseTransf := TMatrix3D.Identity;

      for X := 0 to LData.Width - 1 do
      begin
        if X mod 8 <> 0 then
          Continue;

        for Z := 0 to LData.Height - 1 do
        begin
          if Z mod 8 <> 0 then
            Continue;

          /// check if pixel color is red and the intensity is between 51-255
          /// only then we want to display grass instances
          LPxClr := LData.GetPixel(X, LData.Height - Z);
          if TAlphaColorRec(LPxClr).R < 25 then
            Continue;

          /// compute the position of the current instance
          LPosX   := (-LHalfX + X) / LMapInvSize;
          LPosZ   := (LHalfZ - Z) / LMapInvSize;

          /// add a random x,z offset for a more natural look
          LPosX := LPosX + ((-15 + Random(30)) / GRASS_INV_SIZE);
          LPosZ := LPosZ + ((-15 + Random(30)) / GRASS_INV_SIZE);

          /// start setting up instance transformation matrix
          LTransf := LBaseTransf;

          /// generate random size
          if Random(20) = 0 then
          begin
            /// a few grass instances should be much larger for a more natural
            /// look
            LSize := 25 + Random(25);
          end
          else
          begin
            /// at the walkway, grass need to get smaller
            LSize := Random(TAlphaColorRec(LPxClr).R) / GRASS_INV_SIZE;
            if LSize < 5 then LSize := LSize + 3;
          end;
          
          LTransf := LTransf * TMatrix3D.CreateScaling(Point3D(LSize, LSize, LSize));

          /// generate rotation, remark: for this grass model, we have to adjust
          /// the basis rotation, because it lays on the side (Y=-90�)
          LRotMat := TMatrix3D.CreateRotationYawPitchRoll(DegToRad(0),
            DegToRad(90), DegToRad(0));
          /// add a random rotation on y-axis
          LRotMat := LRotMat * TMatrix3D.CreateRotationY(DegToRad(Random(180)));
          LTransf := LTransf * LRotMat;

          /// set the position of the specific grass instance
          LTransf := LTransf * TMatrix3D.CreateTranslation(Point3D(LPosX, 0, LPosZ));
          LGrassMesh.AddInstance(LTransf);
          Inc(Result);
        end;
      end;
    finally
      LBmp.Unmap(LData);
    end;
  end;
end;

//// MOUSE INTERACTION /////////////////////////////////////////////////////////

procedure TForm1.GorillaViewport1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  FMove := true;
  FLastPos := PointF(X, Y);
end;

procedure TForm1.GorillaViewport1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Single);
var LDiff : TPointF;
begin
  if not FMove then
  begin
    FLastPos := PointF(X, Y);
    Exit;
  end;

  LDiff := PointF(X, Y) - FLastPos;
  if ssLeft in Shift then
  begin
    GorillaCamera1.Position.Z := GorillaCamera1.Position.Z + LDiff.Y * 0.5;
  end
  else if ssRight in Shift then
  begin
    GorillaCamera1.Position.Y := GorillaCamera1.Position.Y + LDiff.Y;
  end;

  FLastPos := PointF(X, Y);
end;

procedure TForm1.GorillaViewport1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  FMove := false;
end;

end.
