unit Unit4;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Types3D,
  System.Math.Vectors, FMX.Controls3D, Gorilla.Light, Gorilla.Viewport,
  Gorilla.Control, Gorilla.Transform, Gorilla.Mesh, Gorilla.Plane,
  FMX.MaterialSources, Gorilla.Material.Default, Gorilla.Material.Lambert,
  Gorilla.Cube, Gorilla.SkyBox, FMX.Media, Gorilla.Cylinder, Gorilla.Model;

type
  TForm4 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaLight1: TGorillaLight;
    GorillaSkyBox1: TGorillaSkyBox;
    CameraComponent1: TCameraComponent;
    GorillaModel1: TGorillaModel;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    procedure CameraComponent1SampleBufferReady(Sender: TObject;
      const ATime: TMediaTime);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FFrameCount : Int64;
  public
    { Public-Deklarationen }
  end;

var
  Form4: TForm4;

implementation

{$R *.fmx}

procedure TForm4.CameraComponent1SampleBufferReady(Sender: TObject;
  const ATime: TMediaTime);
var LBmp : FMX.Graphics.TBitmap;
begin
  Inc(FFrameCount);
  Self.Caption := Format('Gorilla3D - Camera Image [FPS=%n, Frames=%d]',
    [GorillaViewport1.FPS, FFrameCount]);

  LBmp := FMX.Graphics.TBitmap.Create(Self.Width, Self.Height);
  try
    CameraComponent1.SampleBufferToBitmap(LBmp, true);
    GorillaLambertMaterialSource1.Texture.Assign(LBmp);
  finally
    LBmp.Free();
  end;
end;

procedure TForm4.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // Stop camera detection
  CameraComponent1.Active := false;
end;

procedure TForm4.FormShow(Sender: TObject);
begin
  // Attach the light source to the design camera
  GorillaLight1.Parent := GorillaViewport1.GetDesignCamera();

  // We apply our video material to the sub-mesh for the tv-screen
  GorillaModel1.Meshes[2].MaterialSource := GorillaLambertMaterialSource1;

  // Start camera detection
  CameraComponent1.CaptureSetting := TVideoCaptureSetting.Create(640, 480, 24);
  CameraComponent1.Active := true;
end;

end.
