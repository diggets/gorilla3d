unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light, Gorilla.DefTypes,
  Gorilla.Camera, FMX.Objects3D, Gorilla.Viewport, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Plane, FMX.MaterialSources, Gorilla.Material.Default,
  Gorilla.Material.Phong, Gorilla.Layers, FMX.Layers3D;

type
  TForm2 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    Dummy1: TDummy;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaPlane1: TGorillaPlane;
    GorillaPhongMaterialSource1: TGorillaPhongMaterialSource;
    GorillaTextLayer3D1: TGorillaTextLayer3D;
    GorillaTextLayer3D2: TGorillaTextLayer3D;
    GorillaTextLayer3D3: TGorillaTextLayer3D;
    Timer1: TTimer;
    GorillaTextLayer3D4: TGorillaTextLayer3D;
    GorillaTextLayer3D5: TGorillaTextLayer3D;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

procedure TForm2.FormCreate(Sender: TObject);
begin
  GorillaTextLayer3D1.UpdateBuffer();
  GorillaTextLayer3D2.UpdateBuffer();
  GorillaTextLayer3D3.UpdateBuffer();
  GorillaTextLayer3D4.UpdateBuffer();
  GorillaTextLayer3D5.UpdateBuffer();
end;

procedure TForm2.Timer1Timer(Sender: TObject);
begin
  Dummy1.RotationAngle.Y := Dummy1.RotationAngle.Y + 1;
end;

end.
