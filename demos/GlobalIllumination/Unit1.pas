unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Controls3D,
  Gorilla.Control, Gorilla.Transform, Gorilla.Mesh, Gorilla.Cube,
  Gorilla.SkyBox, Gorilla.Viewport, FMX.MaterialSources,
  Gorilla.Material.Default, Gorilla.Material.Phong, FMX.Types3D,
  System.Math.Vectors, Gorilla.Light, Gorilla.Material.Blinn, Gorilla.Torus,
  FMX.Ani, Gorilla.Model, FMX.StdCtrls, FMX.Controls.Presentation;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaSkyBox1: TGorillaSkyBox;
    GorillaCube1: TGorillaCube;
    GorillaPhongMaterialSource1: TGorillaPhongMaterialSource;
    GorillaCube2: TGorillaCube;
    GorillaLight1: TGorillaLight;
    GorillaCube3: TGorillaCube;
    GorillaCube4: TGorillaCube;
    GorillaTorus1: TGorillaTorus;
    GorillaBlinnMaterialSource1: TGorillaBlinnMaterialSource;
    FloatAnimation1: TFloatAnimation;
    FloatAnimation2: TFloatAnimation;
    GorillaModel1: TGorillaModel;
    GorillaBlinnMaterialSource2: TGorillaBlinnMaterialSource;
    TrackBar1: TTrackBar;
    Panel1: TPanel;
    Label1: TLabel;
    TrackBar2: TTrackBar;
    Label2: TLabel;
    TrackBar3: TTrackBar;
    Label3: TLabel;
    Label4: TLabel;
    TrackBar4: TTrackBar;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    TrackBar5: TTrackBar;
    Label9: TLabel;
    Label10: TLabel;
    CheckBox1: TCheckBox;
    FloatAnimation3: TFloatAnimation;
    CheckBox2: TCheckBox;
    GorillaLight2: TGorillaLight;
    FloatAnimation4: TFloatAnimation;
    procedure FormCreate(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure TrackBar2Change(Sender: TObject);
    procedure TrackBar3Change(Sender: TObject);
    procedure TrackBar4Change(Sender: TObject);
    procedure TrackBar5Change(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure CheckBox2Change(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.Math;

procedure TForm1.CheckBox1Change(Sender: TObject);
begin
  // enable / disable moving light source
  FloatAnimation3.Enabled := CheckBox1.IsChecked;
end;

procedure TForm1.CheckBox2Change(Sender: TObject);
begin
  // enable / disable 2nd light source
  GorillaLight2.Enabled := CheckBox2.IsChecked;
  FloatAnimation4.Enabled := CheckBox2.IsChecked;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  // apply material to suzanne model meshes
  GorillaModel1.ApplyMaterialSource(GorillaBlinnMaterialSource2);
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  // change global illumination quality / disable/enable GI
  GorillaViewport1.GlobalIllumDetail := Floor(TrackBar1.Value);
  Label5.Text := IntToStr(Floor(TrackBar1.Value));
end;

procedure TForm1.TrackBar2Change(Sender: TObject);
begin
  // change the softness of global illumination rendering
  GorillaViewport1.GlobalIllumSoftness := Floor(TrackBar2.Value);
  Label6.Text := IntToStr(Floor(TrackBar2.Value));
end;

procedure TForm1.TrackBar3Change(Sender: TObject);
begin
  // change the intensity of shadows
  GorillaViewport1.ShadowStrength := TrackBar3.Value;
  Label7.Text := FormatFloat('0.00', TrackBar3.Value);
end;

procedure TForm1.TrackBar4Change(Sender: TObject);
begin
  // change the intensity of reflections
  GorillaViewport1.ReflectionStrength := TrackBar4.Value;
  Label8.Text := FormatFloat('0.00', TrackBar4.Value);
end;

procedure TForm1.TrackBar5Change(Sender: TObject);
begin
  // change light scattering intensity
  GorillaLight1.ScatteringIntensity := TrackBar5.Value;
  Label10.Text := FormatFloat('0.000', TrackBar5.Value);
end;

end.
