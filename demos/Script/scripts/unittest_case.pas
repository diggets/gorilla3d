program unittest_case;

uses
	System.SysUtils;
	
	procedure Main();
	var LInt : Integer;
		LStr : String;
		LFlt : Double;
		i    : Integer;
	begin
		System.WriteLn('--- UNITTEST - CASE-STATEMENTS --------------------------------');

		System.WriteLn('"case LInt of" should return : case #3');		
		LInt := 5;
		case LInt of
			0 : System.WriteLn('case #1');			
			1 : System.WriteLn('case #2');
			5 : System.WriteLn('case #3');
			else System.WriteLn('case else');
		end;
		
		System.WriteLn('"case LInt of" should return : case else');		
		LInt := 2;
		case LInt of
			0 : System.WriteLn('case #1');			
			1 : System.WriteLn('case #2');
			5 : System.WriteLn('case #3');
			else System.WriteLn('case else');
		end;
		
		System.WriteLn('"case LInt of" should return : case #3');		
		LInt := 8;
		case LInt of
			0    : System.WriteLn('case #1');			
			1..2 : System.WriteLn('case #2');
						
			3..4, 
			5,
			6..9 : begin
					System.WriteLn('case #3');
				   end;
			
			else System.WriteLn('case else');
		end;
		
		System.WriteLn('"case LFlt of" should return : case #3');		
		LFlt := 1225.75;
		case LFlt of
			1225.70 : System.WriteLn('case #1');			
			1225.77 : System.WriteLn('case #2');
			
			1225.749..1225.7501 : System.WriteLn('case #3');
			
			else System.WriteLn('case else');
		end;
		
		System.WriteLn('"case LStr of" should return : case #2');		
		LStr := 'Hello';
		case LStr of
			'Hellooo' : System.WriteLn('case #1');			
			'Hallo'..'Hulu' : System.WriteLn('case #2');
			
			else System.WriteLn('case else');
		end;
		
		System.WriteLn('multiple statement should return : case #2 + more output');		
		LStr := 'Hello';
		case LStr of
			'Hellooo' : System.WriteLn('case #1');			
			'Hello' : begin
						System.WriteLn('case #2');
						for i := 0 to 9 do
						begin
						  System.WriteLn('iteration #i = ' + i);
						  System.WriteLn(#13#10);
						end;
					  end;
			
			else System.WriteLn('case else');
		end;
	end;
	
begin
	Main();  
end.