unit Unit2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.Edit, FMX.EditBox, FMX.NumberBox, FMX.ListBox,
  Gorilla.Volumetric.Mesh;

type
  TForm2 = class(TForm)
    Edt_Width: TNumberBox;
    Label1: TLabel;
    Edt_Height: TNumberBox;
    Label2: TLabel;
    Edt_Slices: TNumberBox;
    Label3: TLabel;
    Btn_OK: TButton;
    Edt_Details: TNumberBox;
    Label4: TLabel;
    Edt_Path: TEdit;
    Label5: TLabel;
    EllipsesEditButton1: TEllipsesEditButton;
    OpenDialog1: TOpenDialog;
    CmbBx_DataType: TComboBox;
    Label6: TLabel;
    procedure EllipsesEditButton1Click(Sender: TObject);
    procedure Btn_OKClick(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    Header   : TNRRDHeader;
    FileName : String;
    Details  : Single;
    IsNRRD   : Boolean;

    function Execute() : Boolean;
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

function TForm2.Execute() : Boolean;
begin
  Header   := TNRRDHeader.Create();
  FileName := '';
  IsNRRD   := false;

  Edt_Width.Value  := 256;
  Edt_Height.Value := 256;
  Edt_Slices.Value := 256;
  CmbBx_DataType.ItemIndex := 0;
  Edt_Details.Value := 1;

  ShowModal();

  Result := (ModalResult = mrOK);
end;

procedure TForm2.Btn_OKClick(Sender: TObject);
begin
  Header.Sizes.Width  := Round(Edt_Width.Value);
  Header.Sizes.Height := Round(Edt_Height.Value);
  Header.Sizes.Depth  := Round(Edt_Slices.Value);
  Header._Type := TGorillaVolumetricMeshDataType(CmbBx_DataType.ItemIndex);
  Details := Edt_Details.Value;

  ModalResult := mrOk;
end;

procedure TForm2.EllipsesEditButton1Click(Sender: TObject);
var LStream : TFileStream;
begin
  if not OpenDialog1.Execute() then
    Exit;

  FileName := OpenDialog1.FileName;
  Edt_Path.Text := FileName;

  // if NRRD file - then try to read texture format
  IsNRRD := ExtractFileExt(FileName).ToLower().Equals('.nrrd');
  if IsNRRD then
  begin
    try
      LStream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyNone);
      try
        TGorillaVolumetricMesh.TryReadNRRDHeader(LStream, Header);
        Edt_Width.Value  := Header.Sizes.Width;
        Edt_Height.Value := Header.Sizes.Height;
        Edt_Slices.Value := Header.Sizes.Depth;
        CmbBx_DataType.ItemIndex := Ord(Header._Type);
      finally
        FreeAndNil(LStream);
      end;
    except
      // ignore open errors here
    end;
  end;
end;

end.
