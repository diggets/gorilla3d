unit Unit2;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Types3D,
  System.Math.Vectors, FMX.Controls3D, Gorilla.Light, Gorilla.Viewport,
  FMX.MaterialSources, Gorilla.Material.Default, Gorilla.Material.Lambert,
  Gorilla.Plane, Gorilla.Control, Gorilla.Transform, Gorilla.Mesh, Gorilla.Cube,
  Gorilla.SkyBox, Gorilla.Particle.Influencer, Gorilla.Particle, Gorilla.Material.Particle,
  Gorilla.Particle.Emitter, Gorilla.Particle.Snow, FMX.ListBox, Gorilla.Particle.Rain,
  Gorilla.Particle.Explosion, Gorilla.Particle.Eruption,
  Gorilla.Particle.Waterfall, FMX.Controls.Presentation, FMX.StdCtrls,
  FMX.Objects3D;

type
  TForm2 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaLight1: TGorillaLight;
    GorillaSkyBox1: TGorillaSkyBox;
    GorillaPlane1: TGorillaPlane;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    GorillaSnowParticleEmitter1: TGorillaSnowParticleEmitter;
    ComboBox1: TComboBox;
    GorillaRainParticleEmitter1: TGorillaRainParticleEmitter;
    GorillaExplosionParticleEmitter1: TGorillaExplosionParticleEmitter;
    GorillaEruptionParticleEmitter1: TGorillaEruptionParticleEmitter;
    GorillaWaterfallParticleEmitter1: TGorillaWaterfallParticleEmitter;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    Sphere1: TSphere;
    GorillaLambertMaterialSource2: TGorillaLambertMaterialSource;
    Timer1: TTimer;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure CheckBox2Change(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure CheckBox3Change(Sender: TObject);
    procedure CheckBox4Change(Sender: TObject);
  private
    FCurrent : TGorillaParticleEmitter;

    FTraktor : TGorillaTraktorInfluencer;
    FRandomColor : TGorillaRandomColorParticleInfluencer;
    FMoveVal : Single;

    procedure SetupCurrentEmitter(AMaterial : TGorillaParticleMaterialSource;
      AUseTexture : Boolean);
    procedure AddDynamicInfluencers();
    procedure RemoveDynamicInfluencers();
  public
    { Public-Deklarationen }
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

procedure TForm2.FormCreate(Sender: TObject);
var LCamera : TCamera;
begin
  Randomize();
  LCamera := GorillaViewport1.GetDesignCamera();

  // Attach light source to camera
  GorillaLight1.Parent := LCamera;

  Sphere1.Visible := false;
  FCurrent := nil;
  ComboBox1.ItemIndex := 0;
  SetupCurrentEmitter(nil, true);
end;

procedure TForm2.SetupCurrentEmitter(AMaterial : TGorillaParticleMaterialSource;
  AUseTexture : Boolean);
var LCamera : TCamera;
begin
  // Here we load textures for default emitters
  LCamera := GorillaViewport1.GetDesignCamera();

  if FCurrent = GorillaRainParticleEmitter1 then
  begin
    GorillaRainParticleEmitter1.Camera := LCamera;
  end
  else if FCurrent = GorillaSnowParticleEmitter1 then
  begin
    GorillaSnowParticleEmitter1.Camera := LCamera;
    if AUseTexture then
    begin
      GorillaSnowParticleEmitter1.LoadTexture('.\snowflake-icon-size_128.png', 8, true, 1, 1);
      if Assigned(AMaterial) then
        AMaterial.UseTexture := AUseTexture;
    end
    else
    begin
      GorillaSnowParticleEmitter1.ParticleSize.Preset(1, 4, 1, false);
      if Assigned(AMaterial) then
        AMaterial.UseTexture := AUseTexture;
    end;
  end
  else if FCurrent = GorillaExplosionParticleEmitter1 then
  begin
    GorillaExplosionParticleEmitter1.Camera := LCamera;
    if AUseTexture then
    begin
      GorillaExplosionParticleEmitter1.LoadTexture('.\explosion.png', 16, true, 8, 8);
      if Assigned(AMaterial) then
        AMaterial.UseTexture := AUseTexture;
    end
    else
    begin
      GorillaExplosionParticleEmitter1.ParticleSize.Preset(1, 4, 1, false);
      if Assigned(AMaterial) then
        AMaterial.UseTexture := AUseTexture;
    end;
  end
  else if FCurrent = GorillaEruptionParticleEmitter1 then
  begin
    GorillaEruptionParticleEmitter1.Camera := LCamera;
  end
  else if FCurrent = GorillaWaterfallParticleEmitter1 then
  begin
    GorillaWaterfallParticleEmitter1.Camera := LCamera;
  end;
end;

procedure TForm2.Timer1Timer(Sender: TObject);
begin
  // Move the traktor influencer
  if not Assigned(FTraktor) then
    Exit;
  if not FTraktor.Enabled then
    Exit;

  FMoveVal := FMoveVal + 0.025;
  Sphere1.AbsolutePosition := Vector3D(Sin(FMoveVal) * 5, 0, Cos(FMoveVal) * 5, 1);
  FTraktor.Center := TPoint3D(Sphere1.AbsolutePosition);
end;

procedure TForm2.CheckBox1Change(Sender: TObject);
begin
  // Show / hide bounds
  if not Assigned(FCurrent) then
    Exit;

  FCurrent.ShowBounds := CheckBox1.IsChecked;
end;

procedure TForm2.CheckBox2Change(Sender: TObject);
begin
  // Activate / Deactivate traktor influencer
  if CheckBox2.IsChecked then
    AddDynamicInfluencers()
  else
    RemoveDynamicInfluencers();
end;

procedure TForm2.CheckBox3Change(Sender: TObject);
var LMaterial : TGorillaParticleMaterialSource;
begin
  // Activate / Deactivate random-color influencer
  if CheckBox3.IsChecked then
    AddDynamicInfluencers()
  else
    RemoveDynamicInfluencers();

  // To allow the random particle-color to be rendered, we need to activate
  // "UseVertexColor" property of the material. This can be modifying "UseTexture".
  if Assigned(FCurrent) then
  begin
    LMaterial := FCurrent.FindComponentDeepSearchByClass(TGorillaParticleMaterialSource) as TGorillaParticleMaterialSource;
    if Assigned(LMaterial) then
    begin
      FCurrent.Clear();
      FCurrent.Stop();

      // For switching back to texture mode, we should reload emitter textures
      SetupCurrentEmitter(LMaterial, not CheckBox3.IsChecked);
      FCurrent.Start();
    end;
  end;
end;

procedure TForm2.CheckBox4Change(Sender: TObject);
var LMaterial : TGorillaParticleMaterialSource;
begin
  // Activate / Deactivate material lighting
  if Assigned(FCurrent) then
  begin
    LMaterial := FCurrent.FindComponentDeepSearchByClass(TGorillaParticleMaterialSource) as TGorillaParticleMaterialSource;
    if Assigned(LMaterial) then
      LMaterial.UseLighting := CheckBox4.IsChecked;
  end;
end;

procedure TForm2.ComboBox1Change(Sender: TObject);
begin
  if Assigned(FCurrent) then
  begin
    FCurrent.Stop();
    FCurrent.Clear();
    RemoveDynamicInfluencers();
  end;

  case ComboBox1.ItemIndex of
    1 : FCurrent := GorillaSnowParticleEmitter1;
    2 : FCurrent := GorillaExplosionParticleEmitter1;
    3 : FCurrent := GorillaEruptionParticleEmitter1;
    4 : FCurrent := GorillaWaterfallParticleEmitter1;

    else FCurrent := GorillaRainParticleEmitter1;
  end;

  // Apply current show-bounds settings
  FCurrent.ShowBounds := CheckBox1.IsChecked;
  // Apply current lighting settings
  CheckBox4Change(nil);
  // Apply render-color material settings
  CheckBox3Change(nil);
  // Apply traktor and/or random-color influencers
  AddDynamicInfluencers();
  // Run the new particle emitter
  FCurrent.Start();

  // Bugfix: Rain particle emitter with splashes produces large black artifacts!
  if (FCurrent = GorillaRainParticleEmitter1) then
    GorillaRainParticleEmitter1.Splashes.Stop();
end;

procedure TForm2.AddDynamicInfluencers();
var I : Integer;
begin
  if CheckBox2.IsChecked then
  begin
    FTraktor := TGorillaTraktorInfluencer.Create(Self);
    FTraktor.Radius  := 7.5;
    FTraktor.Acceleration := 5;
    FTraktor.Emitter := FCurrent;
    FTraktor.Enabled := CheckBox2.IsChecked;

    Sphere1.Visible := CheckBox2.IsChecked;
    FTraktor.Center := TPoint3D(Sphere1.AbsolutePosition);
  end;

  if CheckBox3.IsChecked then
  begin
    // Some emitters already contain a color-influencer by default
    // We should remove that, to prevent from overwriting ours
    if Assigned(FCurrent) then
    begin
      I := 0;
      while I < FCurrent.Influencers.Count do
      begin
        {if FCurrent.Influencers[I] is TGorillaColoredParticleInfluencer then
          FCurrent.RemoveInfluencer(FCurrent.Influencers[I])
        else }
        if FCurrent.Influencers[I] is TGorillaRandomColorParticleInfluencer then
          FCurrent.RemoveInfluencer(FCurrent.Influencers[I])
        else
          Inc(I);
      end;
    end;

    // Add our random color influencer
    FRandomColor := TGorillaRandomColorParticleInfluencer.Create(Self);
    FRandomColor.Emitter := FCurrent;
    FRandomColor.Enabled := CheckBox3.IsChecked;
  end;
end;

procedure TForm2.RemoveDynamicInfluencers();
begin
  if Assigned(FTraktor) then
  begin
    FTraktor.Enabled := false;
    if Assigned(FTraktor.Emitter) then
    begin
      FCurrent.RemoveInfluencer(FTraktor);
      FTraktor := nil;
      Sphere1.Visible := CheckBox2.IsChecked;
    end;
  end;

  if Assigned(FRandomColor) then
  begin
    FRandomColor.Enabled := false;
    if Assigned(FRandomColor.Emitter) then
    begin
      FCurrent.RemoveInfluencer(FRandomColor);
      FRandomColor := nil;
    end;
  end;
end;

end.
