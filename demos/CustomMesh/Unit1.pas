unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Types3D,
  System.Math.Vectors, Gorilla.Control, Gorilla.Transform, Gorilla.Mesh,
  Gorilla.Cube, Gorilla.SkyBox, FMX.Controls3D, Gorilla.Light, Gorilla.Viewport,
  Gorilla.Model, FMX.MaterialSources, Gorilla.Material.Default,
  Gorilla.Material.Lambert;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaLight1: TGorillaLight;
    GorillaSkyBox1: TGorillaSkyBox;
    procedure FormCreate(Sender: TObject);
  private
    FModel : TGorillaModel;
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.Generics.Collections, System.Generics.Defaults,
  Gorilla.DefTypes, Gorilla.Loader, Gorilla.AssetsManager;

type
  TUserModelBuilder = class
    protected
      class function DoBuildMesh(AOwner : TMeshDef) : TMeshDef;

    public
      class function Build() : TModelDef;

      class function AddMaterial(AModel : TModelDef; AMesh : TMeshDef;
        AId : String; AFileName : String) : TMaterialDef;
      class function AddArmatureAndController(AModel : TModelDef;
        AMesh : TMeshDef) : TArmatureDef;
      class function AddSkinAnimation(AModel : TModelDef;
        AArmature : TArmatureDef; AId : String) : TAnimationDef;
  end;

{ TUserModelBuilder }

class function TUserModelBuilder.Build() : TModelDef;
var LMesh : TMeshDef;
    LArma : TArmatureDef;
begin
  // Create the model container
  Result := TModelDef.Create(nil, nil);
  Result.Id := 'CustomModel1';

  // Create and add all meshes to it
  LMesh := DoBuildMesh(Result);

  // We generate texture coordinates, tangents and binormals automatically
  LMesh.CalcTextureCoordinates(TTextureMappingMethod.tmCubic);
  LMesh.CalcTangentBinormals();

  // Add a material with texture
  AddMaterial(Result, LMesh, 'CustomMaterial1', 'brick-c.jpg');

  // Add an armature and skin controller
  LArma := AddArmatureAndController(Result, LMesh);

  // Add skin animation
  AddSkinAnimation(Result, LArma, 'Animation1');
end;

class function TUserModelBuilder.DoBuildMesh(AOwner : TMeshDef) : TMeshDef;

  function GetVertexID(const APos, ANormal, ATangent, AColor : Integer) : TVertexID;
  begin
    FillChar(Result, SizeOf(Result), 0);

    Result.Position := APos;
    Result.Normal := ANormal;
    Result.Tangent := ATangent;
    Result.Colors.Count := 1;
    Result.Colors.Indices[0] := AColor;
  end;

var LCache      : TVertexCache;
    LVertIdxOfs : Integer;
    // In our example we have 3 quads
    LPolys      : Array[0..2] of TPolygonID;
    I           : Integer;
begin
   // Create the mesh object and add it to the supplied owner
  Result := TMeshDef.Create(AOwner);
  Result.Id := 'CustomMesh1';
  Result.FaceOrientation := TFaceOrientation.TwoFace;
  AOwner.AddMesh(Result);

  // Setup a vertex cache
  LCache := TVertexCache.Create();
  try
    LCache.UseBuffersAsArray := true;

    // Start pushing vertex data into
    LCache.SetPositionSource([
        // quad #1 (vertex#0, vertex#1, vertex#2, vertex#3)
        -0.5, -0.5, 0, 0.5, -0.5, 0, 0.5, 0.5, 0, -0.5, 0.5, 0,
        // quad #2 (vertex#4, vertex#5, vertex#6, vertex#7)
        -0.5, -1.5, 0, 0.5, -1.5, 0, 0.5, -0.75, 0, -0.5, -0.75, 0,
        // quad #3 (vertex#8, vertex#9, vertex#10, vertex#11)
        -1.5, -1.5, 0, -0.75, -1.5, 0, -0.75, -0.75, 0, -1.5, -0.75, 0
        ]);

    // All vertices should have the same normal and tangent vector
    LCache.SetNormalSource([0, 0, -1]);
    LCache.SetNormalSource([1, 0, 0]);

    // Each triangle should have its own color
    LCache.SetColorSource(0, [1, 0, 0, 1,   0, 1, 0, 1,   0, 0, 1, 1]);

    // You can work with offsets on the cache - very useful for meshes using the same vertex data
    LVertIdxOfs := 0;

    // Prepare vertex data indices (from buffers) to add quads
    // At first let's clear the triangle vertex data index structures
    System.SetLength(LPolys[0], 4);
    System.SetLength(LPolys[1], 4);
    System.SetLength(LPolys[2], 4);

    // Polygon/Quad #1 - Vertex #0, #1, #2, #3 (with its own color)
    LPolys[0][0] := GetVertexID(0, 0, 0, 0);
    LPolys[0][1] := GetVertexID(1, 0, 0, 0);
    LPolys[0][2] := GetVertexID(2, 0, 0, 0);
    LPolys[0][3] := GetVertexID(3, 0, 0, 0);

    // Polygon/Quad #2 - Vertex #4, #5, #6, #7 (with its own color)
    LPolys[1][0] := GetVertexID(4, 0, 0, 1);
    LPolys[1][1] := GetVertexID(5, 0, 0, 1);
    LPolys[1][2] := GetVertexID(6, 0, 0, 1);
    LPolys[1][3] := GetVertexID(7, 0, 0, 1);

    // Polygon/Quad #3 - Vertex #8, #9, #10, #11 (with its own color)
    LPolys[2][0] := GetVertexID(8, 0, 0, 2);
    LPolys[2][1] := GetVertexID(9, 0, 0, 2);
    LPolys[2][2] := GetVertexID(10, 0, 0, 2);
    LPolys[2][3] := GetVertexID(11, 0, 0, 2);

    // Add the polygons one by one - with automatic triangulation
    for I := Low(LPolys) to High(LPolys) do
      LVertIdxOfs := LCache.AddPolygon(LVertIdxOfs, LPolys[I],
        LCache.Vertices, LCache.Indices);

    // Load the TMeshDef (Result) from the cache
    TGorillaLoader.LoadDefFromData(Result, LCache);

    // When vertex data is added from cache, the vertex index map may be also produced automatically
    // We should add it to the mesh for correct vertex mapping during animations
    Result.AddVertexIndexMap(LCache.IndexMap);
  finally
    FreeAndNil(LCache);
  end;
end;

class function TUserModelBuilder.AddMaterial(AModel : TModelDef; AMesh : TMeshDef;
  AId : String; AFileName : String) : TMaterialDef;
var LAsset : TGorillaTextureAsset;
begin
  // Materials are attached to the model to reuse them in multiple meshes
  Result := TMaterialDef.Create(AModel, AId);
  Result.Kind := TMaterialDefKind.mkLambert;

  // Create a texture for the material
  // 1) Create a standalone asset (possible to request one from an assets package too)
  LAsset := TGorillaTextureAsset.Create(nil);
  LAsset.ImportFromFile(AFileName);

  // 2) Add a diffuse texture in the material
  Result.AddTexture(TColorChannelDef.Diffuse, LAsset, true);

  // Finally we need to link it to our mesh
  AMesh.SetMaterialReference(Result);
end;

class function TUserModelBuilder.AddArmatureAndController(AModel : TModelDef;
  AMesh : TMeshDef) : TArmatureDef;
var LJt1,
    LJt2   : TJointDef;
    LCtrl  : TControllerDef;
    LJtRef : TJointRefDef;
    LRes   : Integer;
begin
  //
  // Create an armature definition and link it to the model
  //
  Result := TArmatureDef.Create(AModel);
  Result.Id := 'Armature1';
  AModel.AddArmature(Result);

  // Afterwards you can add joints / bones for a hierarchy
  // Starting with the root joint
  Result.Root := TJointDef.Create(Result);
  Result.Root.Id := 'SkeletonRoot';

  // Then add the first child node with position offset
  LJt1 := TJointDef.Create(Result);
  LJt1.Id := 'Bone1';
  LJt1.Transform := TMatrix3D.CreateTranslation(Point3D(0, -1, 0));
  Result.Root.AddJoint(LJt1);

  // And another child node with position offset
  LJt2 := TJointDef.Create(Result);
  LJt2.Id := 'Bone2';
  LJt2.Transform := TMatrix3D.CreateTranslation(Point3D(-1, 0, 0));
  LJt1.AddJoint(LJt2);

  //
  // After we have setup the armature with a bone hierarchy, we can create a skin-controller
  //
  LCtrl := TControllerDef.Create(AMesh);
  LCtrl.Id := 'CustomMesh1Controller';

  // Add the controller to the model controllers list
  AModel.AddController(LCtrl);

  // Link the controller also to the armature
  Result.AddController(LCtrl);

  // Add a skin to our controller -  capable to manage deformation data depending on an armature hierarchy
  LCtrl.Skin := TSkinDef.Create(LCtrl);

  // Add the skeleton root reference joint (standalone reference)
  LJtRef := TJointRefDef.Create(Result.Root, LCtrl, true);
  LCtrl.Skin.Skeletons.Add(LJtRef);

  // We have to link all relevant joint to the skin and add weights and joint-indices
  LJtRef := LCtrl.Skin.LinkJoint(Result.Root);
  LJtRef.Weights := [0.75, 0.75, 0.75, 0.75, 0.25, 0.25, 0.25, 0.25, 0, 0, 0, 0];
  LJtRef.Indices := [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

  LJtRef := LCtrl.Skin.LinkJoint(LJt1);
  LJtRef.Weights := [0.25, 0.25, 0.25, 0.25, 0.75, 0.75, 0.75, 0.75, 0.25, 0.25, 0.25, 0.25];
  LJtRef.Indices := [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

  LJtRef := LCtrl.Skin.LinkJoint(LJt2);
  LJtRef.Weights := [0, 0, 0, 0, 0.25, 0.25, 0.25, 0.25, 0.75, 0.75, 0.75, 0.75];
  LJtRef.Indices := [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];

  // Verify the setup
  LRes := LCtrl.Skin.CheckMeshVerticesToBeAttachedCorrectly();
  case LRes of
    0 : ; // everything is okey
    1 : raise Exception.Create('skin vertex not attached');
    2 : raise Exception.Create('skin vertex not attached completely, meaning that total weight value is not equal to 1.0');
    3 : raise Exception.Create('no mesh linked');
    4 : raise Exception.Create('mesh vertex list is empty');
    5 : raise Exception.Create('no skeleton root found');
    6 : raise Exception.Create('no joints linked');
    else raise Exception.Create('unexpected failure');
  end;
end;

class function TUserModelBuilder.AddSkinAnimation(AModel : TModelDef;
  AArmature : TArmatureDef; AId : String) : TAnimationDef;
var LStage : TAnimationStageDef;
    LInterp : TPoint3DInterpolatorDef;
begin
  // Let's start with the animation main object
  Result := TAnimationDef.Create(AModel);
  Result.Id := 'CustomAnimation1';
  AModel.AddAnimation(Result);

  // ROOT JOINT
  // Each animation should contain at least one stage definition
  LStage := TAnimationStageDef.Create(Result);
  LStage.Id := 'RootBoneStage';
  LStage.Reference := AArmature.Root;
  Result.AddStage(LStage);

  // In our example we simply want to move the mesh
  // So we need to create a Point3D interpolator in the stage
  LInterp := TPoint3DInterpolatorDef.Create(LStage);
  LInterp.Id := 'RootBonePosition';
  LStage.AddInterpolator(LInterp);

  // Configure the animation
  LInterp.Path := 'Position.Point';
  LInterp.Loop := true;
  LInterp.Duration := 3.0; // 3 seconds

  // To move the mesh, we need to add keys to the interpolator
  LInterp.AddKey(0, Point3D(0, 0, 0), TAnimationKeyInterpolationType.Linear);
  LInterp.AddKey(0.75, Point3D(0, -0.75, 0), TAnimationKeyInterpolationType.Linear);
  LInterp.AddKey(1, Point3D(0, -1, 0), TAnimationKeyInterpolationType.Linear);

  // JOINT1
  LStage := TAnimationStageDef.Create(Result);
  LStage.Id := 'Bone1Stage';
  LStage.Reference := AArmature.Root.FindJointById('Bone1');
  Result.AddStage(LStage);

  // In our example we simply want to move the mesh
  // So we need to create a Point3D interpolator in the stage
  LInterp := TPoint3DInterpolatorDef.Create(LStage);
  LInterp.Id := 'Bone1Rotation';
  LStage.AddInterpolator(LInterp);

  // Configure the animation
  LInterp.Path := 'RotationAngle.Point';
  LInterp.Loop := true;
  LInterp.Duration := 3.0; // 3 seconds

  // To move the mesh, we need to add keys to the interpolator
  LInterp.AddKey(0, Point3D(0, 0, 0), TAnimationKeyInterpolationType.Linear);
  LInterp.AddKey(0.75, Point3D(0, 90, 0), TAnimationKeyInterpolationType.Linear);
  LInterp.AddKey(1, Point3D(0, 180, 0), TAnimationKeyInterpolationType.Linear);

  // JOINT2
  LStage := TAnimationStageDef.Create(Result);
  LStage.Id := 'Bone2Stage';
  LStage.Reference := AArmature.Root.FindJointById('Bone2');
  Result.AddStage(LStage);

  // In our example we simply want to move the mesh
  // So we need to create a Point3D interpolator in the stage
  LInterp := TPoint3DInterpolatorDef.Create(LStage);
  LInterp.Id := 'Bone2Rotation';
  LStage.AddInterpolator(LInterp);

  // Configure the animation
  LInterp.Path := 'RotationAngle.Point';
  LInterp.Loop := true;
  LInterp.Duration := 3.0; // 3 seconds

  // To move the mesh, we need to add keys to the interpolator
  LInterp.AddKey(0, Point3D(0, 0, 0), TAnimationKeyInterpolationType.Linear);
  LInterp.AddKey(0.75, Point3D(0, 0, -90), TAnimationKeyInterpolationType.Linear);
  LInterp.AddKey(1, Point3D(0, 0, -180), TAnimationKeyInterpolationType.Linear);
end;

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
var LModelDef : TModelDef;
begin
  // Create abstract model data
  LModelDef := TUserModelBuilder.Build();

  // Create visual component for the abstract model data
  FModel := TGorillaModel.LoadNewModelFromDef(GorillaViewport1, LModelDef, []);
  FModel.Parent := GorillaViewport1;

  // For debugging purposes show the bones
  FModel.ShowJoints := true;

  FModel.AnimationManager.Current.Start;
end;

end.
