unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Gorilla.Viewport, FMX.Types3D, System.Math.Vectors, FMX.Controls3D,
  Gorilla.Light, Gorilla.Control, Gorilla.Transform, Gorilla.Mesh, Gorilla.Cube,
  FMX.MaterialSources, Gorilla.Material.Default, Gorilla.Material.Lambert,
  Gorilla.Material.Blinn, Gorilla.Sphere, Gorilla.Physics,
  FMX.Controls.Presentation, FMX.StdCtrls, Gorilla.Capsule, FMX.Objects3D,
  FMX.Layers3D, Gorilla.Layers, Gorilla.SkyBox, Gorilla.Cylinder;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaLight1: TGorillaLight;
    Joint3_Cube1: TGorillaCube;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    Joint3_Cube2: TGorillaCube;
    Joint1_Sphere1: TGorillaSphere;
    GorillaBlinnMaterialSource1: TGorillaBlinnMaterialSource;
    Joint1_Sphere2: TGorillaSphere;
    GorillaPhysicsSystem1: TGorillaPhysicsSystem;
    Button1: TButton;
    Joint3_Cube3: TGorillaCube;
    GorillaLambertMaterialSource2: TGorillaLambertMaterialSource;
    Joint2_Cube: TGorillaCube;
    Joint2_Capsule: TGorillaCapsule;
    Joint1_Text: TGorillaTextLayer3D;
    GorillaSkyBox1: TGorillaSkyBox;
    Joint2_Text: TGorillaTextLayer3D;
    Joint3_Text: TGorillaTextLayer3D;
    Timer1: TTimer;
    Joint4_Text: TGorillaTextLayer3D;
    Joint4_Door: TGorillaCube;
    Joint4_Post: TGorillaCylinder;
    Joint5_Text: TGorillaTextLayer3D;
    Joint5_Cube1: TGorillaCube;
    Joint5_Cube2: TGorillaCube;
    Joint6_Cube1: TGorillaCube;
    Joint6_Text: TGorillaTextLayer3D;
    Joint6_Cube2: TGorillaCube;
    Joint7_Cube1: TGorillaCube;
    Joint7_Text: TGorillaTextLayer3D;
    Joint7_Cube2: TGorillaCube;
    Joint7_Cube3: TGorillaCube;
    Joint7_Sphere1: TGorillaSphere;
    Joint2_Sphere: TGorillaSphere;
    procedure Button1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private-Deklarationen }
    FTimeValue : Single;
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  Gorilla.Physics.Q3.Renderer;

var SHOW_PHYSICS_COLLIDERS : Boolean = true;

procedure TForm1.Button1Click(Sender: TObject);
begin
  GorillaPhysicsSystem1.Active := Button1.IsPressed;
  Timer1.Enabled := Button1.IsPressed;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  // Move magnet to test the magnetic joint
  FTimeValue := FTimeValue + 0.25;
  GorillaPhysicsSystem1.RemoteBodyVelocity(
    Joint3_Cube2,
    Point3D(Sin(FTimeValue) * 2, 0, Cos(FTimeValue) * 2)
    );
end;

end.
