unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light, Gorilla.DefTypes,
  Gorilla.Camera, FMX.Objects3D, Gorilla.Viewport, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Plane, FMX.MaterialSources, Gorilla.Material.Default,
  Gorilla.Material.SplatMap, FMX.Objects, FMX.Controls.Presentation,
  FMX.StdCtrls, FMX.ListBox, System.Diagnostics;

type
  TForm2 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    Dummy1: TDummy;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaPlane1: TGorillaPlane;
    GorillaSplatMapMaterialSource1: TGorillaSplatMapMaterialSource;
    Timer1: TTimer;
    Panel1: TPanel;
    ComboBox1: TComboBox;
    Label1: TLabel;
    PaintBox1: TImage;
    Button1: TButton;
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PaintBox1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure PaintBox1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure PaintBox1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Single);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    FMove    : Boolean;
    FLastPos : TPointF;
    FTicker  : TStopWatch;

    procedure ClearSplatMap();
    procedure ApplySplatMap();
  public
    { Public-Deklarationen }
  end;

var
  Form2: TForm2;

implementation

{$R *.fmx}

procedure TForm2.FormCreate(Sender: TObject);
begin
  FTicker := TStopWatch.Create();
  FTicker.StartNew();
end;

procedure TForm2.FormShow(Sender: TObject);
begin
  ClearSplatMap();
end;

procedure TForm2.Button1Click(Sender: TObject);
begin
  ClearSplatMap();
end;

procedure TForm2.PaintBox1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  FMove := true;
  FLastPos := PointF(X, Y);
  FTicker.Start;
end;

procedure TForm2.PaintBox1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Single);
var LCurr  : TPointF;
    LBmp   : TBitmap;
    LBrush : TStrokeBrush;
    LClr   : TAlphaColor;
begin

  if not FMove then
  begin
    FLastPos := PointF(X, Y);
    Exit;
  end;

  if not (ssLeft in Shift) then
    Exit;

  LCurr := PointF(X, Y); // + PaintBox1.AbsoluteRect.TopLeft;

  case Combobox1.ItemIndex of
    0 : LClr := TAlphaColorRec.Red;
    1 : LClr := TAlphaColorRec.Green;
    2 : LClr := TAlphaColorRec.Blue;
    else LClr := TAlphaColorRec.Black;
  end;

  LBrush := TStrokeBrush.Create(TBrushKind.Solid, LClr);
  LBrush.Thickness := 10;

  LBmp := PaintBox1.Bitmap;
  LBmp.Canvas.BeginScene();
  try
    LBmp.Canvas.Fill.Color := LClr;
    LBmp.Canvas.FillRect(
      RectF(LCurr.X - 8, LCurr.Y - 8, LCurr.X + 8, LCurr.Y + 8),
      4, 4,
      [TCorner.TopLeft, TCorner.TopRight, TCorner.BottomLeft, TCorner.BottomRight],
      1
      );
  finally
    LBmp.Canvas.EndScene();
  end;

  FLastPos := PointF(X, Y);

  if FTicker.ElapsedMilliseconds > 250 then
    ApplySplatMap();
end;

procedure TForm2.PaintBox1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  FTicker.Stop;
  FMove := false;
  ApplySplatMap();
end;

procedure TForm2.Timer1Timer(Sender: TObject);
begin
  Dummy1.RotationAngle.Y := Dummy1.RotationAngle.Y + 1;
end;

procedure TForm2.ClearSplatMap();
var LBmp : TBitmap;
begin
  LBmp := TBitmap.Create(320, 320);
  try
    LBmp.Canvas.BeginScene();
    try
      LBmp.Canvas.Fill.Color := TAlphaColorRec.Black;
      LBmp.Canvas.FillRect(PaintBox1.ClipRect, 1);
    finally
      LBmp.Canvas.EndScene();
    end;

    PaintBox1.Bitmap.Assign(LBmp);
  finally
    LBmp.Free();
  end;

  ApplySplatMap();
end;

procedure TForm2.ApplySplatMap();
begin
  GorillaSplatMapMaterialSource1.Texture.Assign(PaintBox1.Bitmap);
end;

end.
