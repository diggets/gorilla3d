unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light,
  Gorilla.Camera, Gorilla.Viewport, Gorilla.AssetsManager,
  FMX.Controls.Presentation, FMX.Edit, FMX.StdCtrls, Gorilla.Model,
  Gorilla.Control, Gorilla.Mesh, Gorilla.Plane, FMX.Objects3D, FMX.Objects;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaAssetsManager1: TGorillaAssetsManager;
    Edit1: TEdit;
    EditButton1: TEditButton;
    Button1: TButton;
    Grid3D1: TGrid3D;
    LoadingPanel: TPanel;
    Image1: TImage;
    Label1: TLabel;
    Dummy1: TDummy;
    procedure EditButton1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure GorillaViewport1MouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure GorillaViewport1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure GorillaViewport1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Single);
    procedure GorillaViewport1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
  private
    FPackage  : TGorillaAssetsPackage;
    FUsername : String;
    FPassword : String;
    FClientId : String;
    FClientSecret : String;

    FUID      : String;
    FModel    : TGorillaModel;

    FMove     : Boolean;
    FLastPoint : TPointF;

    function GetExtractPath() : String;
  public
    property Username : String read FUsername write FUsername;
    property Password : String read FPassword write FPassword;
    property ClientId : String read FClientId write FClientId;
    property ClientSecret : String read FClientSecret write FClientSecret;

    procedure SetModelUID(AID : String);
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  Unit2,
  System.IOUtils,
  Gorilla.DefTypes,
  Gorilla.Sketchfab.Loader;

function TForm1.GetExtractPath() : String;
begin
{$IFDEF MSWINDOWS}
  Result := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0)));
{$ENDIF}
{$IFDEF ANDROID}
  Result := IncludeTrailingPathDelimiter(TPath.GetPublicPath());
{$ENDIF}
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  // open login screen
{$IFDEF MSWINDOWS}
  Form2.ShowModal();
{$ENDIF}
{$IFDEF ANDROID}
  Form2.Show();
{$ENDIF}
end;

procedure TForm1.SetModelUID(AID : String);
begin
  Edit1.Text := AID;
  FUID := Edit1.Text;
end;

procedure TForm1.EditButton1Click(Sender: TObject);
var LOpts : TGorillaLoadOptions;
    LExPathCached,
    LExPath : String;
begin
  if FUsername.IsEmpty then
  begin
    ShowMessage('Username not set');
    Exit;
  end;

  if FPassword.IsEmpty then
  begin
    ShowMessage('Password not set');
    Exit;
  end;

  if FClientId.IsEmpty then
  begin
    ShowMessage('ClientId not set');
    Exit;
  end;

  if FClientSecret.IsEmpty then
  begin
    ShowMessage('ClientSecret not set');
    Exit;
  end;

  FUID := Edit1.Text;

  if FUID.IsEmpty then
  begin
    ShowMessage('Model UID not set');
    Exit;
  end;

  try
    // clear the previously created model
    if Assigned(FModel) then
      FreeAndNil(FModel);

    // create a new package if not exists
    if not Assigned(FPackage) then
      FPackage := GorillaAssetsManager1.AddEmptyPackage('SketchfabCache');

    // get download destination path depending on platform
    LExPath := GetExtractPath();

    // we have to check if uid path exists on android platform
    // because GetFiles throws an exception
    LExPathCached := LExPath + IncludeTrailingPathDelimiter(FUID);
    if not DirectoryExists(LExPathCached) then
      System.IOUtils.TDirectory.CreateDirectory(LExPathCached);
    if not DirectoryExists(LExPathCached) then
      raise EAbort.CreateFmt('failed to create download directory: %s', [LExPathCached]);

    // setup loading options to connect to sketchfab
    LOpts := TGorillaLoadOptions.Create(FUID, '.sketchfab', FPackage);
    LOpts.FileInfo.OpenFormat := TGorillaFileOpenFormat.fofDownload;

    LOpts.FileInfo.URL := Gorilla.Sketchfab.Loader.GORILLA_SKETCHFAB_URL;
    LOpts.FileInfo.ClientId := FClientId;
    LOpts.FileInfo.ClientSecret := FClientSecret;
    LOpts.FileInfo.Username := FUsername;
    LOpts.FileInfo.Password := FPassword;
    // get download destination depending on the platform
    LOpts.ExtractDirectory := LExPath;

    LoadingPanel.Visible := true;
    LoadingPanel.BringToFront;
    GorillaViewport1.Enabled := false;
  {$IFDEF MSWINDOWS}
    Application.ProcessMessages();
  {$ENDIF}
    try
      // try loading sketchfab model
      FModel := TGorillaModel.LoadNewModelFromFile(GorillaViewport1, LOpts);
      if Assigned(FModel) then
        FModel.Parent := GorillaViewport1;

      FModel.SetHitTestValue(false);
    finally
      GorillaViewport1.Enabled := true;
      LoadingPanel.Visible := false;
    end;
  except
    on E:Exception do
    begin
      ShowMessage('Error: ' + E.Message);
    end;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  LoadingPanel.Visible := false;
end;

procedure TForm1.GorillaViewport1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  FMove := true;
end;

procedure TForm1.GorillaViewport1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Single);
var LCurr, LDiff : TPointF;
begin
  LCurr := PointF(X, Y);
  if not FMove then
  begin
    FLastPoint := LCurr;
    Exit;
  end;

  LDiff := LCurr - FLastPoint;
  if (ssLeft in Shift) then
  begin
    Dummy1.RotationAngle.Y := Dummy1.RotationAngle.Y + LDiff.X;
  end
  else if (ssRight in Shift) then
  begin
    Dummy1.RotationAngle.X := Dummy1.RotationAngle.X + LDiff.Y;
  end;

  FLastPoint := LCurr;
end;

procedure TForm1.GorillaViewport1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  FMove := true;
end;

procedure TForm1.GorillaViewport1MouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; var Handled: Boolean);
begin
  if WheelDelta < 0 then
    GorillaCamera1.Position.Z := GorillaCamera1.Position.Z - 1
  else
    GorillaCamera1.Position.Z := GorillaCamera1.Position.Z + 1;
end;

end.
