unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Gorilla.Controller, Gorilla.Controller.Passes.PostFX, FMX.Controls3D,
  Gorilla.Control, Gorilla.Transform, Gorilla.Mesh, Gorilla.Cube,
  Gorilla.SkyBox, Gorilla.Viewport, System.Math.Vectors, FMX.MaterialSources,
  Gorilla.Material.Default, Gorilla.Material.Lambert, FMX.Objects3D,
  FMX.Types3D, Gorilla.Light;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaSkyBox1: TGorillaSkyBox;
    GorillaRenderPassPostFX1: TGorillaRenderPassPostFX;
    RoundCube1: TRoundCube;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    GorillaLight1: TGorillaLight;
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

end.
