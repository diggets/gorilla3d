unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light, Gorilla.DefTypes,
  Gorilla.Camera, Gorilla.Viewport, Gorilla.Control, Gorilla.Mesh, Gorilla.Plane,
  FMX.MaterialSources, Gorilla.Material.Default, Gorilla.Material.Lambert,
  Gorilla.Cube, Gorilla.Material.Shared, Gorilla.Material.Atlas,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Objects3D, Gorilla.Transform,
  Gorilla.Controller, Gorilla.Controller.Passes.Reflection;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaPlane1: TGorillaPlane;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    GorillaCube1: TGorillaCube;
    GorillaCube2: TGorillaCube;
    GorillaCube3: TGorillaCube;
    GorillaAtlasMaterialSource1: TGorillaAtlasMaterialSource;
    GorillaAtlasMaterialSource2: TGorillaAtlasMaterialSource;
    GorillaAtlasMaterialSource3: TGorillaAtlasMaterialSource;
    Button1: TButton;
    Timer1: TTimer;
    Dummy1: TDummy;
    GorillaSharedAtlasMaterialSource1: TGorillaSharedAtlasMaterialSource;
    GorillaRenderPassReflection1: TGorillaRenderPassReflection;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private-Deklarationen }
  public
    SharedAtlasSource : TGorillaSharedAtlasMaterialSource;
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.IOUtils;

procedure TForm1.Button1Click(Sender: TObject);
begin
  // randomly select a texture for each cube
  GorillaViewport1.BeginUpdate();
  try
    GorillaAtlasMaterialSource1.TextureIndex := Random(64);
    GorillaAtlasMaterialSource2.TextureIndex := Random(64);
    GorillaAtlasMaterialSource3.TextureIndex := Random(64);
  finally
    GorillaViewport1.EndUpdate();
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Randomize();

  Timer1.Enabled := true;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  Dummy1.RotationAngle.Y := Dummy1.RotationAngle.Y + 1;
end;

end.
