unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  Gorilla.Viewport, FMX.Controls3D, Gorilla.Control, Gorilla.Transform,
  Gorilla.Mesh, Gorilla.Cube, Gorilla.SkyBox, Gorilla.Terrain, FMX.Types3D,
  System.Math.Vectors, Gorilla.Light, FMX.MaterialSources,
  Gorilla.Material.Default, Gorilla.Material.Lambert, Gorilla.Plane,
  Gorilla.Material.Blinn, Gorilla.Controller,
  Gorilla.Controller.Passes.Reflection, Gorilla.Model, FMX.Ani,
  FMX.Controls.Presentation, FMX.StdCtrls, Gorilla.Material.POM,
  Gorilla.Material.PBR;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaSkyBox1: TGorillaSkyBox;
    GorillaTerrain1: TGorillaTerrain;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    GorillaLight1: TGorillaLight;
    GorillaRenderPassReflection1: TGorillaRenderPassReflection;
    GorillaModel1: TGorillaModel;
    FloatAnimation1: TFloatAnimation;
    MirrorPlane2: TGorillaPlane;
    GorillaBlinnMaterialSource2: TGorillaBlinnMaterialSource;
    MirrorPlane3: TGorillaPlane;
    GorillaBlinnMaterialSource3: TGorillaBlinnMaterialSource;
    GorillaRenderPassReflection2: TGorillaRenderPassReflection;
    GorillaRenderPassReflection3: TGorillaRenderPassReflection;
    MirrorPlane1: TGorillaPlane;
    GorillaBlinnMaterialSource1: TGorillaBlinnMaterialSource;
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

end.
