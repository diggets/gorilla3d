unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Types3D,
  System.Math.Vectors, FMX.Objects3D, FMX.Controls3D, Gorilla.Camera,
  Gorilla.Light, Gorilla.Viewport, Gorilla.AssetsManager, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Model, Gorilla.Utils.Path, FMX.Ani, Gorilla.Bokeh,
  Gorilla.Particle.Influencer, Gorilla.Particle.Emitter, FMX.MaterialSources,
  Gorilla.Material.Default, Gorilla.Material.Particle, Gorilla.Material.Lambert,
  Gorilla.Sphere, Gorilla.Transform, Gorilla.Cube, Gorilla.SkyBox,
  Gorilla.Material.Behaviour, Gorilla.Material.Behaviour.Caustics,
  FMX.Controls.Presentation, FMX.StdCtrls;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    UnderwaterModel: TGorillaModel;
    GorillaLight1: TGorillaLight;
    DiverModel: TGorillaModel;
    GorillaSkyBox1: TGorillaSkyBox;
    GorillaMaterialCausticsBehaviour1: TGorillaMaterialCausticsBehaviour;
    BubbleEmitter: TGorillaParticleEmitter;
    GorillaParticleMaterialSource1: TGorillaParticleMaterialSource;
    FishGroupModel: TGorillaModel;
    GorillaBokeh1: TGorillaBokeh;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: WideChar;
      Shift: TShiftState);
  private
    { Private-Deklarationen }
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: WideChar; Shift: TShiftState);
begin
  case KeyChar of
    'D',
    'd' : GorillaViewport1.DiagnosticsActive := not GorillaViewport1.DiagnosticsActive;

    'A',
    'a' : GorillaBokeh1.AutoFocus := not GorillaBokeh1.AutoFocus;

    'F',
    'f' : begin
            GorillaBokeh1.FocalDepth := GorillaViewport1.GetDesignCamera.AbsolutePosition.Distance(DiverModel.AbsolutePosition) * 2;
          end;

    'B',
    'b' : GorillaBokeh1.Enabled := not GorillaBokeh1.Enabled;

    'R',
    'r' : if GorillaViewport1.GlobalIllumDetail > 0 then
          begin
            GorillaViewport1.GlobalIllumDetail := 0;
          end
          else
          begin
            GorillaViewport1.GlobalIllumDetail := 5;
            GorillaViewport1.GlobalIllumSoftness := 3;
            GorillaViewport1.ShadowStrength := 1;
          end;
  end;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  // For better bubble particle result, we disable depth peeling on transparency computation
  GorillaViewport1.FragmentDepthRendering := false;

  // Apply caustics underwater effect
  UnderwaterModel.AttachMaterialBehaviour(GorillaMaterialCausticsBehaviour1);
  DiverModel.AttachMaterialBehaviour(GorillaMaterialCausticsBehaviour1);
  FishGroupModel.AttachMaterialBehaviour(GorillaMaterialCausticsBehaviour1);

  // Run the divers animation
  DiverModel.AnimationManager.Current.Start();
  DiverModel.SetHitTestValue(false);

  // Run the fish group animation
  FishGroupModel.AnimationManager.Current.Start();
  FishGroupModel.SetHitTestValue(false);

  // For better particle sorting, we apply the viewers camera
  BubbleEmitter.Camera := GorillaViewport1.GetDesignCamera();

  // Load our bubbles
  BubbleEmitter.LoadTexture('.\assets\bubble.png', 1, true, 2, 2, 64, 64);

  // Do not let light affect our bubbles
  GorillaParticleMaterialSource1.UseLighting := false;

  // Run the bubble particles
  BubbleEmitter.Start();
end;

end.
