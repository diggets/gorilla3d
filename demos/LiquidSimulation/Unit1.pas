unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light, Gorilla.DefTypes,
  Gorilla.Camera, Gorilla.Viewport, FMX.Objects3D, Gorilla.Control,
  Gorilla.Mesh, Gorilla.Terrain, FMX.MaterialSources, Gorilla.Material.Default,
  Gorilla.Material.Lambert, Gorilla.Particle.Influencer, Gorilla.Physics.Q3.Body,
  Gorilla.Particle.Emitter, Gorilla.Material.Particle, Gorilla.Physics,
  FMX.Controls.Presentation, FMX.StdCtrls, Gorilla.Cube, Gorilla.Sphere,
  Gorilla.Material.Blinn, Gorilla.Transform, Gorilla.Group, Gorilla.Particle;

type
  /// <summary>
  /// We overwrite the physics influencer class to set collision detection
  /// for all particles. By default collision detection between particles
  /// is deactivated due to a focus on performance.
  /// </summary>
  TGorillaPhysicsLiquidInfluencer = class(TGorillaPhysicsInfluencer)
    protected
      procedure DoOnEmitParticle(const AParticle : PGorillaParticle); override;
  end;

  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    Dummy1: TDummy;
    Timer1: TTimer;
    GorillaParticleEmitter1: TGorillaParticleEmitter;
    GorillaParticleMaterialSource1: TGorillaParticleMaterialSource;
    GorillaPhysicsSystem1: TGorillaPhysicsSystem;
    Button1: TButton;
    GorillaColoredParticleInfluencer1: TGorillaColoredParticleInfluencer;
    GorillaCube1: TGorillaCube;
    GorillaGroup1: TGorillaGroup;
    GorillaLambertMaterialSource1: TGorillaLambertMaterialSource;
    GorillaCube2: TGorillaCube;
    GorillaCube3: TGorillaCube;
    GorillaCube4: TGorillaCube;
    GorillaCube5: TGorillaCube;
    GorillaCube6: TGorillaCube;
    Dummy2: TDummy;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure GorillaViewport1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure GorillaViewport1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure GorillaViewport1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Single);
    procedure GorillaViewport1MouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; var Handled: Boolean);
  private
    FLiquidInfluencer : TGorillaPhysicsLiquidInfluencer;
    FIsMoving  : Boolean;
    FLastPoint : TPointF;
    FShowColliders : Boolean;
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  System.TypInfo, Gorilla.Physics.Q3.Renderer,
  Gorilla.Physics.Q3.Collider.Particle;

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
  if GorillaParticleEmitter1.Active then
  begin
    // deactivate particles and physics
    Timer1.Enabled := false;

    GorillaPhysicsSystem1.Active := false;
    GorillaParticleEmitter1.Stop();
    GorillaParticleEmitter1.Clear();

    Button1.Text := 'Start';
  end
  else
  begin
    // activate particles and physics
    GorillaPhysicsSystem1.Active := true;

    GorillaParticleEmitter1.Start();

    Timer1.Enabled := true;
    Button1.Text := 'Stop';
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Randomize();

  // enable or disable rendering of physics colliders (Caution: very slow!)
  FShowColliders := true;

  // need to be disabled, otherwise mouse interaction will not work
  GorillaPhysicsSystem1.HitTest := false;

  ///
  /// PARTICLE EMITTER
  ///

  /// create our project specific liquid influencer with particle collision
  /// detection
  FLiquidInfluencer := TGorillaPhysicsLiquidInfluencer.Create(GorillaParticleEmitter1);
  FLiquidInfluencer.Parent  := GorillaParticleEmitter1;
  FLiquidInfluencer.Emitter := GorillaParticleEmitter1;
  FLiquidInfluencer.Physics := GorillaPhysicsSystem1;
  FLiquidInfluencer.Enabled := true;

  // setup particle emitter (position, velocity, lifetime, size, ... of particles)
  GorillaParticleEmitter1.ParticlePosition.RangeX.Preset(500, 700, 100, false);
  GorillaParticleEmitter1.ParticlePosition.RangeY.Preset(25, 25, 1, false);
  GorillaParticleEmitter1.ParticlePosition.RangeZ.Preset(0, 10, 10, true);
  GorillaParticleEmitter1.ParticlePosition.Direction.Point := Point3D(-1, -1, 1);
  GorillaParticleEmitter1.ParticlePosition.Axis := [TAllowedAxis.axX, TAllowedAxis.axY, TAllowedAxis.axZ];

  // we don't want particles to be moved by influencer (no velocity), just a movement by physics
  GorillaParticleEmitter1.ParticleVelocity.RangeX.Preset(0, 0, 1, false);
  GorillaParticleEmitter1.ParticleVelocity.RangeY.Preset(0, 0, 1, false);
  GorillaParticleEmitter1.ParticleVelocity.RangeZ.Preset(0, 0, 1, false);
  GorillaParticleEmitter1.ParticleVelocity.Direction.Point := Point3D(0, 0, 0);
  GorillaParticleEmitter1.ParticleVelocity.Axis := [TAllowedAxis.axX, TAllowedAxis.axY, TAllowedAxis.axZ];

  // because fluid particles should not disappear too fast, we set the lifetime
  // to 10.000 seconds
  GorillaParticleEmitter1.ParticleLifeTime := TParticlePreset.Create(10000, 10000, 1, false);

  // here we set the fixed rendered particle size of 0.50
  GorillaParticleEmitter1.ParticleSize := TParticlePreset.Create(50, 50, 100, false);

  // particle weight defines the particle collider size
  // this setting will produce colliders with a fixed sphere size of 0.5
  // NOTE: size of particle should be larger than Q3_BAUMGARTE (= 0.3)
  GorillaParticleEmitter1.ParticleWeight := TParticlePreset.Create(50, 50, 100, false);

  // disable default linear influencer (this added to an emitter by default)
  GorillaParticleEmitter1.Influencers[0].Enabled := false;

  // configure the colored influencer for particles to have a blueish color
  GorillaColoredParticleInfluencer1.StartColorF :=
    TAlphaColorF.Create(0, Random(156)/255, Random(255)/255, 1);
  GorillaColoredParticleInfluencer1.EndColorF :=
    TAlphaColorF.Create(0, 0, Random(255)/255, 1);
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  // stop physics particles computation (making problems sometimes!)
  Timer1.Enabled := false;

  // stop emitter
  GorillaParticleEmitter1.Stop();
  GorillaParticleEmitter1.Clear();

  // finally stop physics engine
  GorillaPhysicsSystem1.Active := false;
end;

procedure TForm1.GorillaViewport1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  FIsMoving  := true;
  FLastPoint := PointF(X, Y);
end;

procedure TForm1.GorillaViewport1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Single);
var LDiff : TPointF;
begin
  if FIsMoving then
  begin
    if (ssLeft in Shift) then
    begin
      LDiff := PointF(X, Y) - FLastPoint;
      Dummy1.RotationAngle.Y := Dummy1.RotationAngle.Y + LDiff.X;
    end;

    FLastPoint := PointF(X, Y);
  end;
end;

procedure TForm1.GorillaViewport1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  FIsMoving := false;
end;

procedure TForm1.GorillaViewport1MouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; var Handled: Boolean);
begin
  if WheelDelta < 0 then
    GorillaCamera1.Position.Z := GorillaCamera1.Position.Z - 0.2
  else
    GorillaCamera1.Position.Z := GorillaCamera1.Position.Z + 0.2;

  Handled := true;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  GorillaViewport1.BeginUpdate();
  try
    // update render info in header
    Self.Caption := Format('Gorilla 3D - Liquid Simulation - %n FPS (Particles: %d)',
      [GorillaViewport1.FPS, GorillaParticleEmitter1.Emitted]);

    // physics computation inside of the main thread needs explicit updating
    GorillaPhysicsSystem1.Step();
  finally
    GorillaViewport1.EndUpdate();
  end;
end;

{ TGorillaPhysicsLiquidInfluencer }

procedure TGorillaPhysicsLiquidInfluencer.DoOnEmitParticle(
  const AParticle: PGorillaParticle);
var LColl : TQ3ParticleCollider;
begin
  inherited;

  if not Assigned(AParticle^.Data) then
    Exit;

  LColl := TQ3Body(AParticle^.Data).GetFirstCollider() as TQ3ParticleCollider;
  LColl.CollisionDetection := true;
end;

end.
