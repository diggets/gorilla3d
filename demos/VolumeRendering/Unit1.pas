unit Unit1;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Math.Vectors, FMX.Types3D, FMX.Controls3D, Gorilla.Light,
  Gorilla.Camera, Gorilla.Viewport, Gorilla.Control, Gorilla.Mesh,
  Gorilla.Volumetric.Mesh, Gorilla.Controller.Passes.Face,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.ListBox,
  FMX.Edit, FMX.EditBox, FMX.NumberBox, FMX.Objects3D, FMX.Styles.Objects,
  FMX.TabControl, Gorilla.Transform;

type
  TForm1 = class(TForm)
    GorillaViewport1: TGorillaViewport;
    GorillaCamera1: TGorillaCamera;
    GorillaLight1: TGorillaLight;
    GorillaVolumetricMesh1: TGorillaVolumetricMesh;
    ToolBar1: TToolBar;
    Button1: TButton;
    ComboBox2: TComboBox;
    Label2: TLabel;
    Dummy1: TDummy;
    TabStyleObject1: TTabStyleObject;
    TabControl1: TTabControl;
    TabItem1: TTabItem;
    CheckBox3: TCheckBox;
    ComboBox5: TComboBox;
    NumberBox5: TNumberBox;
    Label7: TLabel;
    NumberBox8: TNumberBox;
    Label9: TLabel;
    NumberBox9: TNumberBox;
    Label10: TLabel;
    NumberBox10: TNumberBox;
    Label11: TLabel;
    NumberBox11: TNumberBox;
    Label12: TLabel;
    NumberBox12: TNumberBox;
    Label13: TLabel;
    TabItem2: TTabItem;
    ComboBox6: TComboBox;
    NumberBox4: TNumberBox;
    Label8: TLabel;
    NumberBox6: TNumberBox;
    Label14: TLabel;
    NumberBox7: TNumberBox;
    Label15: TLabel;
    TabItem3: TTabItem;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    TabItem4: TTabItem;
    CmbBx_Shape: TComboBox;
    Edt_Details: TNumberBox;
    Label17: TLabel;
    NumberBox14: TNumberBox;
    Label18: TLabel;
    NumberBox15: TNumberBox;
    Label19: TLabel;
    NumberBox16: TNumberBox;
    Label20: TLabel;
    TrackBar1: TTrackBar;
    TabItem5: TTabItem;
    TrackBar2: TTrackBar;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    TrackBar3: TTrackBar;
    Label5: TLabel;
    TrackBar4: TTrackBar;
    TrackBar5: TTrackBar;
    Label6: TLabel;
    TrackBar6: TTrackBar;
    Label16: TLabel;
    Label21: TLabel;
    TrackBar7: TTrackBar;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    TrackBar8: TTrackBar;
    Label25: TLabel;
    TrackBar9: TTrackBar;
    Label26: TLabel;
    TrackBar10: TTrackBar;
    procedure Button1Click(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GorillaViewport1MouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; var Handled: Boolean);
    procedure GorillaViewport1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure GorillaViewport1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure GorillaViewport1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Single);
    procedure ComboBox6Change(Sender: TObject);
    procedure CheckBox3Change(Sender: TObject);
    procedure ComboBox5Change(Sender: TObject);
    procedure NumberBox5Change(Sender: TObject);
    procedure NumberBox8Change(Sender: TObject);
    procedure NumberBox9Change(Sender: TObject);
    procedure NumberBox10Change(Sender: TObject);
    procedure NumberBox11Change(Sender: TObject);
    procedure NumberBox12Change(Sender: TObject);
    procedure NumberBox4Change(Sender: TObject);
    procedure NumberBox6Change(Sender: TObject);
    procedure NumberBox7Change(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure CheckBox2Change(Sender: TObject);
    procedure CmbBx_ShapeChange(Sender: TObject);
    procedure Edt_DetailsChange(Sender: TObject);
    procedure NumberBox14Change(Sender: TObject);
    procedure NumberBox15Change(Sender: TObject);
    procedure NumberBox16Change(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure TrackBar2Change(Sender: TObject);
    procedure TrackBar3Change(Sender: TObject);
    procedure TrackBar4Change(Sender: TObject);
    procedure TrackBar6Change(Sender: TObject);
    procedure TrackBar5Change(Sender: TObject);
    procedure TrackBar7Change(Sender: TObject);
    procedure TrackBar8Change(Sender: TObject);
    procedure TrackBar9Change(Sender: TObject);
    procedure TrackBar10Change(Sender: TObject);
  private
    FMove : Boolean;
    FLastPos : TPointF;

    FFrontFaceRenderPass : TGorillaRenderPassFace;
    FBackFaceRenderPass  : TGorillaRenderPassFace;
  public
    procedure AdjustCamera(AView : Integer);
    procedure ActivateClipping(AView : Integer);
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses
  Unit2,
  System.IOUtils,
  Gorilla.DefTypes,
  Gorilla.Context.Types;

procedure TForm1.FormCreate(Sender: TObject);
begin
  /// for volume rendering previous steps are necessary
  /// front and back vertices have to be rendered from current view
  /// so we simple create two render passes doing that job for us

  /// a pre render-pass to get all vertex positions of front faces
  FFrontFaceRenderPass := TGorillaRenderPassFace.Create(GorillaViewport1, 'FrontFace');
  FFrontFaceRenderPass.FaceKind := TFaceKind.FrontFace;
  FFrontFaceRenderPass.Viewport := GorillaViewport1;
  FFrontFaceRenderPass.Enabled := true;
  /// the render-pass should only render this volume
  FFrontFaceRenderPass.AllowControl(GorillaVolumetricMesh1);

  /// a pre render-pass to get all vertex positions of back faces
  FBackFaceRenderPass := TGorillaRenderPassFace.Create(GorillaViewport1, 'BackFace');
  FBackFaceRenderPass.FaceKind := TFaceKind.BackFace;
  FBackFaceRenderPass.Viewport := GorillaViewport1;
  FBackFaceRenderPass.Enabled := true;
  /// the render-pass should only render this volume
  FBackFaceRenderPass.AllowControl(GorillaVolumetricMesh1);

  GorillaVolumetricMesh1.FrontFaceRenderPass := FFrontFaceRenderPass;
  GorillaVolumetricMesh1.BackFaceRenderPass  := FBackFaceRenderPass;

  GorillaVolumetricMesh1.GamutMode := TGorillaVolumetricGamutMode.GamutNone;

//{$IFDEF ANDROID}
//  GorillaVolumetricMesh1.LoadFromNRRDFile(
//    IncludeTrailingPathDelimiter(System.IOUtils.TPath.GetHomePath()) +
//    'skull.nrrd'
//    )
//{$ENDIF}
end;

procedure TForm1.GorillaViewport1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  FMove := true;
end;

procedure TForm1.GorillaViewport1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Single);
var LDiff : TPointF;
begin
  if not FMove then
  begin
    FLastPos := PointF(X, Y);
    Exit;
  end;

  if (ssLeft in Shift) then
  begin
    LDiff := PointF(X, Y) - FLastPos;

    Dummy1.RotationAngle.Y := Dummy1.RotationAngle.Y + LDiff.X;
  end;

  FLastPos := PointF(X, Y);

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.GorillaViewport1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  FMove := false;
end;

procedure TForm1.GorillaViewport1MouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; var Handled: Boolean);
begin
  if WheelDelta < 0 then
    GorillaCamera1.Position.Z := GorillaCamera1.Position.Z - 0.1
  else
    GorillaCamera1.Position.Z := GorillaCamera1.Position.Z + 0.1;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.NumberBox10Change(Sender: TObject);
begin
  /// change ambient occlussion bias for PBR shading model
  (GorillaVolumetricMesh1.MaterialSource as TGorillaVolumetricMeshMaterialSource).AOBias := NumberBox10.Value;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.NumberBox11Change(Sender: TObject);
begin
  /// change shading intensity
  (GorillaVolumetricMesh1.MaterialSource as TGorillaVolumetricMeshMaterialSource).ShadingIntensity := NumberBox11.Value;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.NumberBox12Change(Sender: TObject);
begin
  /// change brightness
  (GorillaVolumetricMesh1.MaterialSource as TGorillaVolumetricMeshMaterialSource).Brightness := NumberBox12.Value;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.NumberBox14Change(Sender: TObject);
begin
  /// changing raytracing stop limit
  GorillaVolumetricMesh1.RayStop := NumberBox14.Value;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.NumberBox15Change(Sender: TObject);
begin
  /// changing raytracing limits
  GorillaVolumetricMesh1.RayLimits := Point(Round(NumberBox15.Value), Round(NumberBox16.Value));

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.NumberBox16Change(Sender: TObject);
begin
  /// changing raytracing limits
  GorillaVolumetricMesh1.RayLimits := Point(Round(NumberBox15.Value), Round(NumberBox16.Value));

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.NumberBox4Change(Sender: TObject);
begin
  /// change gamut factor
  GorillaVolumetricMesh1.GamutFactor := NumberBox4.Value;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.NumberBox5Change(Sender: TObject);
begin
  /// change iso surface detection limit
  GorillaVolumetricMesh1.IsoSurfaceLimit := NumberBox5.Value;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.NumberBox6Change(Sender: TObject);
begin
  /// change gamut intensity
  GorillaVolumetricMesh1.GamutIntensity := NumberBox6.Value;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.NumberBox7Change(Sender: TObject);
begin
  /// change gamut alpha intensity
  GorillaVolumetricMesh1.GamutAlphaIntensity := NumberBox7.Value;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.NumberBox8Change(Sender: TObject);
begin
  /// change metallic bias for PBR shading model
  (GorillaVolumetricMesh1.MaterialSource as TGorillaVolumetricMeshMaterialSource).MetallicBias := NumberBox8.Value;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.NumberBox9Change(Sender: TObject);
begin
  /// change roughness bias for PBR shading model
  (GorillaVolumetricMesh1.MaterialSource as TGorillaVolumetricMeshMaterialSource).RoughnessBias := NumberBox9.Value;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;


procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  // control clipping
  case ComboBox2.ItemIndex of
    /// Back
    1 : ActivateClipping(1);
    /// Top
    2 : ActivateClipping(2);
    /// Bottom
    3 : ActivateClipping(3);
    /// Left
    4 : ActivateClipping(4);
    /// Right
    5 : ActivateClipping(5);

    /// Front
    else ActivateClipping(0);
  end;
end;

procedure TForm1.TrackBar2Change(Sender: TObject);
begin
  GorillaVolumetricMesh1.Position.X := TrackBar2.Value;
end;

procedure TForm1.TrackBar3Change(Sender: TObject);
begin
  GorillaVolumetricMesh1.Position.Y := TrackBar3.Value;
end;

procedure TForm1.TrackBar4Change(Sender: TObject);
begin
  GorillaVolumetricMesh1.Position.Z := TrackBar4.Value;
end;

procedure TForm1.TrackBar5Change(Sender: TObject);
begin
  GorillaVolumetricMesh1.RotationAngle.X := TrackBar5.Value;
end;

procedure TForm1.TrackBar6Change(Sender: TObject);
begin
  GorillaVolumetricMesh1.RotationAngle.Y := TrackBar6.Value;
end;

procedure TForm1.TrackBar7Change(Sender: TObject);
begin
  GorillaVolumetricMesh1.RotationAngle.Z := TrackBar7.Value;
end;

procedure TForm1.TrackBar8Change(Sender: TObject);
begin
  GorillaVolumetricMesh1.Width := TrackBar8.Value;
end;

procedure TForm1.TrackBar9Change(Sender: TObject);
begin
  GorillaVolumetricMesh1.Height := TrackBar9.Value;
end;

procedure TForm1.TrackBar10Change(Sender: TObject);
begin
  GorillaVolumetricMesh1.Depth := TrackBar10.Value;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  /// load some volumetric data by a loading dialog
  if Form2.Execute() then
  begin
    /// reset rotation so we have a fresh look after loading 3D data
    GorillaVolumetricMesh1.RotationAngle.Point := TPoint3D.Zero;
    GorillaVolumetricMesh1.ResetRotationAngle();

    /// setup volumetric mesh setting detected by loading dialog
    GorillaVolumetricMesh1.BeginUpdate();
    try
      GorillaVolumetricMesh1.CustomSize := Form2.Header.Sizes;
      GorillaVolumetricMesh1.Sizes := TGorillaVolumetricMeshSize.vmsCustom;
      GorillaVolumetricMesh1.DataType := Form2.Header._Type;
      GorillaVolumetricMesh1.Details := Form2.Details;
    finally
      GorillaVolumetricMesh1.EndUpdate();
    end;

    /// if NRRD file then use LoadFromNRRDFile method
    if Form2.IsNRRD then
      GorillaVolumetricMesh1.LoadFromNRRDFile(Form2.FileName)
    /// otherwise try to load from raw data
    else
      GorillaVolumetricMesh1.LoadFromRawFile(Form2.FileName,
        Form2.Header.Sizes.Width, Form2.Header.Sizes.Height,
        Form2.Header.Sizes.Depth,
        Form2.Header._Type);
  end;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.CheckBox1Change(Sender: TObject);
begin
  /// activate / deactivate linear filtering
  GorillaVolumetricMesh1.LinearFiltering := CheckBox1.IsChecked;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.CheckBox2Change(Sender: TObject);
begin
  /// activate / deactivate mip mapping
  GorillaVolumetricMesh1.MipMaps := CheckBox2.IsChecked;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.CheckBox3Change(Sender: TObject);
begin
  GorillaVolumetricMesh1.UseLighting := CheckBox3.IsChecked;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.CmbBx_ShapeChange(Sender: TObject);
begin
  /// changing the volumetric shape
  GorillaVolumetricMesh1.Shape := TGorillaVolumetricMeshShape(CmbBx_Shape.ItemIndex);

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.ComboBox2Change(Sender: TObject);
begin
  /// changing the clipping plane on the volumetric mesh
  AdjustCamera(ComboBox2.ItemIndex);
  ActivateClipping(ComboBox2.ItemIndex);

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.ComboBox5Change(Sender: TObject);
begin
  /// change shading model used for rendering
  case ComboBox5.ItemIndex of
    1 : GorillaVolumetricMesh1.ShadingModel := TGorillaShadingModel.smPhong;
    2 : GorillaVolumetricMesh1.ShadingModel := TGorillaShadingModel.smBlinnPhong;
    3 : GorillaVolumetricMesh1.ShadingModel := TGorillaShadingModel.smPBR;
    else GorillaVolumetricMesh1.ShadingModel := TGorillaShadingModel.smLambert;
  end;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.ComboBox6Change(Sender: TObject);
begin
  /// change gamut rendering mode
  case ComboBox6.ItemIndex of
    1 : GorillaVolumetricMesh1.GamutMode := TGorillaVolumetricGamutMode.GamutByValue;
    2 : GorillaVolumetricMesh1.GamutMode := TGorillaVolumetricGamutMode.GamutByValueMultiply;
    3 : GorillaVolumetricMesh1.GamutMode := TGorillaVolumetricGamutMode.GamutBySum;
    4 : GorillaVolumetricMesh1.GamutMode := TGorillaVolumetricGamutMode.GamutBySumMultiply;
    else GorillaVolumetricMesh1.GamutMode := TGorillaVolumetricGamutMode.GamutNone;
  end;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.Edt_DetailsChange(Sender: TObject);
begin
  /// changing raytracing detail
  GorillaVolumetricMesh1.Details := Edt_Details.Value;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.AdjustCamera(AView : Integer);
var LPt : TPoint3D;
begin
  /// we adjust the camera to look into the right direction
  case AView of
    1 : begin
          /// back position
          LPt := Point3D(0, 0, 10);
        end;

    2 : begin
          /// top position
          LPt := Point3D(0.00001, -10, 0);
        end;

    3 : begin
          /// bottom position
          LPt := Point3D(0.00001, 10, 0);
        end;

    4 : begin
          /// left position
          LPt := Point3D(-10, 0, 0);
        end;

    5 : begin
          /// right position
          LPt := Point3D(10, 0, 0);
        end;

    else
      begin
          /// front position
          LPt := Point3D(0, 0, -10);
      end;
  end;

  GorillaCamera1.Position.Point := LPt;
  GorillaCamera1.Target := GorillaVolumetricMesh1;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

procedure TForm1.ActivateClipping(AView : Integer);
var LPlane : TPlaneF;
begin
  /// we compute the clipping plane distance by using the current trackbar value
  /// divided by the halfsize of the volumetric mesh
  /// this is of course just some demo code and can be modified by you
  case AView of
    1 : begin
          /// back clipping plane
          LPlane := TPlaneF.Create(0, 0, -1, (TrackBar1.Value / ((GorillaVolumetricMesh1.Width / 2) * 100)));
        end;

    2 : begin
          /// top clipping plane
          LPlane := TPlaneF.Create(0, 1, 0, (TrackBar1.Value / ((GorillaVolumetricMesh1.Width / 2) * 100)));
        end;

    3 : begin
          /// bottom clipping plane
          LPlane := TPlaneF.Create(0, -1, 0, (TrackBar1.Value / ((GorillaVolumetricMesh1.Width / 2) * 100)));
        end;

    4 : begin
          /// left clipping plane
          LPlane := TPlaneF.Create(1, 0, 0, (TrackBar1.Value / ((GorillaVolumetricMesh1.Width / 2) * 100)));
        end;

    5 : begin
          /// right clipping plane
          LPlane := TPlaneF.Create(-1, 0, 0, (TrackBar1.Value / ((GorillaVolumetricMesh1.Width / 2) * 100)));
        end;

    else
      begin
        /// front clipping plane
        LPlane := TPlaneF.Create(0, 0, 1, (TrackBar1.Value / ((GorillaVolumetricMesh1.Width / 2) * 100)));
      end;
  end;

  /// set the clipping plane
  GorillaVolumetricMesh1.ClippingPlane := LPlane;

  /// refresh viewport after changes made, because we do not use fixed frame rate rendering
  GorillaViewport1.Invalidate();
end;

end.
